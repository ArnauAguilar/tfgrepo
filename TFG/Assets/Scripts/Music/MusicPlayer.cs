using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    private PlayerTrigger _playerTrigger;

    private MusicManager _musicManager;
    
    [SerializeField] private AudioClip musicTrack;
    [SerializeField] private bool destroyAfter;

    private void Start()
    {
        _playerTrigger = GetComponent<PlayerTrigger>();
        
        //_playerTrigger.onPlayerEnter.Subscribe(OnPlayerEnter);
        OnPlayerEnter();
    }

    private void OnPlayerEnter()
    {
        _musicManager = FindObjectOfType<MusicManager>();

        _musicManager.PlayNewMusicTrack(musicTrack);
        if(destroyAfter) Destroy(gameObject);
    }
}
