using System.Collections;
using UnityEngine;


public class PlayerSound : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;
    
    [SerializeField] private AudioClip[] robotSounds;
    [SerializeField] private Vector2 minMaxVolume;
    [SerializeField] private Vector2 minMaxPitch;
    [SerializeField] private Vector2 minMaxDelay;

    private float _timeToBeep;
    
    private void Start()
    {
        _timeToBeep = Random.Range(minMaxDelay.x, minMaxDelay.y);
        StartCoroutine(WaitUntilNextBeep());
    }

    private IEnumerator WaitUntilNextBeep()
    {
        yield return new WaitForSeconds(_timeToBeep);
        
        PlayBeep();
    }

    private void PlayBeep()
    {
        audioSource.clip = robotSounds[Random.Range(0, robotSounds.Length)];
        audioSource.volume = Random.Range(minMaxVolume.x, minMaxVolume.y);
        audioSource.pitch = Random.Range(minMaxPitch.x, minMaxPitch.y);
        audioSource.Play();
        
        _timeToBeep = Random.Range(minMaxDelay.x, minMaxDelay.y);
        StartCoroutine(WaitUntilNextBeep());
    }
}
