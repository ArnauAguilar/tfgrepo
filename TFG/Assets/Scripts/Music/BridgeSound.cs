using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeSound : MonoBehaviour
{
    [SerializeField] private AudioSource lightsOffSource;
    [SerializeField] private AudioSource bridgeSource;

    public void PlayLightsOffSound()
    {
        //lightsOffSource.Play();
    }

    public void PlayBridgeSound()
    {
        bridgeSource.Play();
    }
}
