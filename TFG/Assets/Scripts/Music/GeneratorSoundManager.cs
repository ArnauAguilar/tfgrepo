using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratorSoundManager : MonoBehaviour
{
    [SerializeField] private GravityGenerator gravityGenerator;

    [SerializeField] private AudioSource stateAudioSource;
    [SerializeField] private AudioSource flowAudioSource;

    [SerializeField] private AudioClip startGenerator;
    [SerializeField] private AudioClip stopGenerator;
    
    private void Awake()
    {
        gravityGenerator.OnGeneratorSwitch.Subscribe(OnGeneratorSwitch);
    }

    private void OnGeneratorSwitch()
    {
        if (gravityGenerator.GeneratorIsActive)
        {
            StartCoroutine(FadeAudioSource.StartFade(flowAudioSource, 4, 0, 1));

            stateAudioSource.clip = startGenerator;
            stateAudioSource.Play();
        }
        else
        {
            StartCoroutine(FadeAudioSource.StartFade(flowAudioSource, 1, 1, 0));

            stateAudioSource.clip = stopGenerator;
            stateAudioSource.Play();
        }
    }
}
