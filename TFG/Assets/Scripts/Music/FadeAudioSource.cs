using System.Collections;
using UnityEngine;

public static class FadeAudioSource 
{
    public static IEnumerator StartFade(AudioSource audioSource, float duration, float initialVolume, 
        float targetVolume)
    {
        if(targetVolume > 0) audioSource.Play();
        
        float currentTime = 0;
        if (audioSource.isPlaying) initialVolume = audioSource.volume;
        float start = audioSource.volume = initialVolume;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
            yield return null;
        }

        if (targetVolume == 0)
        {
            audioSource.Stop();
        }
    }
}