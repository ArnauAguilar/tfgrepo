using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [SerializeField] private AudioSource musicAudioSource;
    [SerializeField] private AudioSource secondaryAudioSource;

    [SerializeField] private float maximumVolume;

    private void Start()
    {
        StartCoroutine(FadeAudioSource.StartFade(musicAudioSource, 5, 0, maximumVolume));
    }

    public void PlayNewMusicTrack(AudioClip music)
    {
        StartCoroutine(FadeAudioSource.StartFade(musicAudioSource, 5, 0, maximumVolume));
        return;
        
        if (musicAudioSource.clip == music || secondaryAudioSource.clip == music) return;

        if (!musicAudioSource.isPlaying)
        {
            musicAudioSource.clip = music;
            StartCoroutine(FadeAudioSource.StartFade(musicAudioSource, 5, 0, maximumVolume));
            StartCoroutine(FadeAudioSource.StartFade(secondaryAudioSource, 5, maximumVolume, 0));
            return;
        }
        
        secondaryAudioSource.clip = music;
        StartCoroutine(FadeAudioSource.StartFade(musicAudioSource, 5, maximumVolume, 0));
        StartCoroutine(FadeAudioSource.StartFade(secondaryAudioSource, 5, 0, maximumVolume));
    }
}
