using UnityEngine;

public class MenuMusicPlayer : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;

    public void PlayMusic()
    {
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
            StartCoroutine(FadeAudioSource.StartFade(audioSource, 3, 0, 1));
        }
    }

    public void PauseMusic()
    {
        if (audioSource.isPlaying)
        {
            StartCoroutine(FadeAudioSource.StartFade(audioSource, 2, 1, 0));
        }
    }
}
