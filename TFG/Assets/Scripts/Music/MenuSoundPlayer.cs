using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSoundPlayer : MonoBehaviour
{
    [SerializeField] private AudioClip[] UIClips;
    [SerializeField] private float volume;

    public void PlayRandomUISound(AudioSource audioSource)
    {
        audioSource.clip = UIClips[Random.Range(0, UIClips.Length)];
        audioSource.volume = volume;
        audioSource.Play();
    }
}
