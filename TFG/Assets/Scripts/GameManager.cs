using System;
using UnityEngine;
using Alone.SaveAndLoad;
using Sirenix.OdinInspector;
using UnityEngine.Audio;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public InputController inputController;
    public SerializationManager serializationManager;
    public MenuMusicPlayer menuMusicPlayer;

    public AudioMixer generalAudioMixer;
    public AloneSettings settings;

    public DeviceType currentDeviceType;

    public bool isOnSwitch;
    public string currentSaveSlotPath;
    public bool isNewSlot;
    
    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);

            isOnSwitch = CheckForSwitch();
            
            if (!serializationManager.LoadSettings())
            {
                settings = new AloneSettings();
                ResetAllSettings();
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        ApplyMixerVolumeValue(settings.masterVolume, "Master");
        ApplyMixerVolumeValue(settings.musicVolume, "Music");
        ApplyMixerVolumeValue(settings.effectsVolume, "Effects");
    }

    [Button]
    public void ResetVideoSettings()
    {
        settings.ResetVideoSettings();
        serializationManager.SaveSettings(settings);
        
        //Screen.fullScreen = settings.presentationMode;
        Resolution newResolution = AloneSettings.GetResolutions()[settings.resolutionIndex];
        if (newResolution.width != Screen.currentResolution.width &&
            newResolution.height != Screen.currentResolution.height)
        {
            Screen.SetResolution(newResolution.width, newResolution.height, Screen.fullScreen);
        }
        QualitySettings.vSyncCount = settings.vSync ? 1 : 0;
    }
    
    [Button]
    public void ResetKeyboardAndMouseSettings()
    {
        settings.ResetKeyboardAndMouseSettings();
        serializationManager.SaveSettings(settings);
        
        OrbitCamera orbitCamera = FindObjectOfType<OrbitCamera>();
        if (orbitCamera)
        {
            orbitCamera.KeyboardRotationSpeed = settings.keyboardRotationSpeed;
            orbitCamera.InvertedYAxisMouse = settings.invertMouse;
            orbitCamera.UseMouseAcceleration = settings.mouseAcceleration;
        }
    }
    
    [Button]
    public void ResetGamepadSettings()
    {
        settings.ResetGamepadSettings();
        serializationManager.SaveSettings(settings);
        
        OrbitCamera orbitCamera = FindObjectOfType<OrbitCamera>();
        if (orbitCamera)
        {
            orbitCamera.UseRightHandedScheme = settings.useRightHandedStickMoveLook;
            orbitCamera.GamepadRotationSpeed = settings.gamepadRotationSpeed;
            orbitCamera.InvertedYAxisGamepad = settings.invertLook;
        }

        MovingSphere movingSphere = FindObjectOfType<MovingSphere>();
        if (movingSphere)
        {
            movingSphere.UseRightHandedScheme = settings.useRightHandedStickMoveLook;
        }
    }
    
    [Button]
    public void ResetAllSettings()
    {
        settings.ResetVideoSettings();
        settings.ResetAudioSettings();
        settings.ResetKeyboardAndMouseSettings();
        settings.ResetGamepadSettings();
        serializationManager.SaveSettings(settings);
    }

    private static bool CheckForSwitch()
    {
#if UNITY_SWITCH
            return true;
#endif
        return false;
    }
    
    public void ApplyMixerVolumeValue(float value, string mixer)
    {
        float actualValue = Mathf.Clamp(value, 0.01f, 100) / 100f;
        generalAudioMixer.SetFloat(mixer, Mathf.Log10(actualValue) * 20);
    }
}
