using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public class SceneLoaderTrigger : MonoBehaviour
{
    private PlayerTrigger _playerTrigger;

    private enum TriggerAction
    {
        Load,
        Unload
    }

    [System.Serializable]
    private struct SceneToLoadOrUnload
    {
        [EnumToggleButtons] public TriggerAction triggerAction;
        public SceneReference scene;
    }

    [SerializeField] private List<SceneToLoadOrUnload> scenes;
    
    private void Awake()
    {
        _playerTrigger = GetComponent<PlayerTrigger>();
        
        _playerTrigger.onPlayerEnter.Subscribe(OnPlayerEnter);
    }

    private void OnPlayerEnter()
    {
        if (scenes.Count == 0) return;

        foreach (var scene in scenes.Where(scene => scene.scene != null))
        {
            switch (scene.triggerAction)
            {
                case TriggerAction.Load:
                    SceneLoader.LoadAdditiveSceneForced(scene.scene);
                    break;
                
                case TriggerAction.Unload:
                    SceneLoader.UnloadSceneForced(scene.scene);
                    break;
            }
        }
    }
}
