using System.Collections;
using System.Collections.Generic;
using Mono.CSharp;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif
using UnityEngine.SceneManagement;
using UnityEngine;

public static class SceneLoader
{
    private static List<AsyncOperation> scenesToLoad = new List<AsyncOperation>();
    public static int ScenesToLoadCount => scenesToLoad.Count;


    private static SceneReference nextSceneToLoad;
    public static SceneReference NextSceneToLoad => nextSceneToLoad;

    public static bool AllLoaded()
    {
        foreach (AsyncOperation asyncOperation in scenesToLoad)
        {
            if (!asyncOperation.isDone) return false;
        }

        return true;
    }

    public static bool SetActiveScene(SceneReference scene)
    {
        if (!AllLoaded()) return false;

        SceneManager.SetActiveScene(SceneManager.GetSceneByPath(scene));
        
        return true;
    }

    public static SceneReference GetCurrentActiveScene()
    {
        return new SceneReference {ScenePath = SceneManager.GetActiveScene().path};
    }

    public static bool LoadLoadingScreenScene(SceneReference loadingScreenScene, SceneReference _nextSceneToLoad, 
        List<SceneReference> _nextSceneToLoadAdditive)
    {
        if (!AllLoaded() || SceneManager.GetSceneByPath(loadingScreenScene).isLoaded) return false;

        nextSceneToLoad = _nextSceneToLoad;
        
        scenesToLoad.Clear();
        scenesToLoad.Add(SceneManager.LoadSceneAsync(loadingScreenScene, LoadSceneMode.Single));
        scenesToLoad.Add(SceneManager.LoadSceneAsync(_nextSceneToLoad, LoadSceneMode.Additive));
        foreach (var scene in _nextSceneToLoadAdditive)
        {
            scenesToLoad.Add(SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive));
        }
        return true;
    }

    public static bool LoadMainScene(SceneReference scene)
    {
        if (!AllLoaded() || SceneManager.GetSceneByPath(scene).isLoaded) return false;
        scenesToLoad.Clear();
        scenesToLoad.Add(SceneManager.LoadSceneAsync(scene, LoadSceneMode.Single));
        return true;
    }

    public static bool LoadAdditiveScene(SceneReference scene)
    {
        if (!AllLoaded() || SceneManager.GetSceneByPath(scene).isLoaded) return false;
        scenesToLoad.Clear();
        scenesToLoad.Add(SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive));
        return true;
    }

    public static void LoadAdditiveSceneForced(SceneReference scene)
    {
        if (!SceneManager.GetSceneByPath(scene).isLoaded) SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
    }

    public static bool LoadMultipleScenes(SceneReference mainScene, List<SceneReference> additiveScenes)
    {
        if (!AllLoaded()) return false;
        scenesToLoad.Clear();
        scenesToLoad.Add(SceneManager.LoadSceneAsync(mainScene, LoadSceneMode.Single));
        foreach (SceneReference scene in additiveScenes)
        {
            scenesToLoad.Add(SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive));
        }
        return true;
    }
#if UNITY_EDITOR
    public static void LoadMultipleScenesEditor(SceneReference mainScene, List<SceneReference> additiveScenes)
    {
        EditorSceneManager.OpenScene(mainScene, OpenSceneMode.Single);
        foreach (SceneReference scene in additiveScenes)
        {
            EditorSceneManager.OpenScene(scene, OpenSceneMode.Additive);
        }
    }
#endif
    

    public static bool UnloadScene(SceneReference scene)
    {
        if (!AllLoaded()) return false;
        if (SceneManager.GetSceneByPath(scene).isLoaded)
        {
            SceneManager.UnloadSceneAsync(scene);
            return true;
        }
        return false;
    }

    public static void UnloadSceneForced(SceneReference scene)
    {
        if (SceneManager.GetSceneByPath(scene).isLoaded) SceneManager.UnloadSceneAsync(scene);
    }
    
    public static bool UnloadMultipleScenes(List<SceneReference> scenes)
    {
        if (!AllLoaded()) return false;
        scenesToLoad.Clear();
        foreach (SceneReference scene in scenes)
        {
            if (SceneManager.GetSceneByPath(scene).isLoaded) SceneManager.UnloadSceneAsync(scene);
        }
        return true;
    }

    public static float GetLoadingProgress()
    {
        float progress = 0;
        foreach (AsyncOperation asyncOperation in scenesToLoad)
        {
            if (asyncOperation.isDone) progress += 1;
            else progress += asyncOperation.progress;
        }

        return progress/scenesToLoad.Count;
    }
    
}
