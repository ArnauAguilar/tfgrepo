using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UIElements;

[CreateAssetMenu(fileName = "MultiScene", menuName = "SceneManagment/MultiScene", order = 1)]
[System.Serializable]
public class MultiScene_SO : ScriptableObject
{
    public List<SceneReference> aditiveScenes;
    public SceneReference mainScene;
    
    [Button(ButtonSizes.Large)]
    public void LoadScenes()
    {
#if UNITY_EDITOR
        SceneLoader.LoadMultipleScenesEditor(mainScene, aditiveScenes);
#endif

    }
}
