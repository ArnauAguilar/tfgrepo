
using System.Collections;
using Alone.Menu;
using UnityEngine;

public class StartAnimation : MonoBehaviour
{
    [SerializeField] private Fader fader;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private MovingSphere movingSphere;
    [SerializeField] private OrbitCamera orbitCamera;

    private void Start()
    {
        if (!GameManager.Instance.isNewSlot)
        {
            Destroy(gameObject);
            return;
        }

        StartCoroutine(PlayStartAnimation());
    }

    private IEnumerator PlayStartAnimation()
    {
        PauseMenu.CanPause = false;
        movingSphere.ManualInput = false;
        orbitCamera.CameraAnimation = ScriptedAnimations.ForceCameraUpdate;
        
        fader.FadeIn(0);
        
        yield return new WaitForSeconds(0.5f);
        audioSource.Play();
        
        yield return new WaitForSeconds(5);
        fader.FadeOut(2f);
        yield return new WaitForSeconds(1);
        
        PauseMenu.CanPause = true;
        movingSphere.ManualInput = true;
        orbitCamera.CameraAnimation = ScriptedAnimations.Stop;
    }
}
