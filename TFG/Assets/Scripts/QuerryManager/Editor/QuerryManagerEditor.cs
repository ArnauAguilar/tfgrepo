﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Graphs;
using UnityEngine;

[CustomEditor(typeof(QuerryManager))]
[CanEditMultipleObjects]
public class QuerryManagerEditor : Editor
{
    private QuerryManager querryManager;
    private void OnEnable()
    {
        querryManager = (target as QuerryManager);
        redStyle = new GUIStyle(EditorStyles.foldout);
        redStyle.normal.textColor = Color.red;
        redStyle.active.textColor = Color.red;
        greenStyle = new GUIStyle(EditorStyles.foldout);
        greenStyle.normal.textColor = Color.green;
        greenStyle.active.textColor = Color.green;

    }

    private GUIStyle redStyle, greenStyle; 
    private bool showPosition = false;
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Bake Geometry Positions"))
        {
            querryManager.BakeGeometryAreas();
        }
        if (GUILayout.Button("Read From Bake"))
            querryManager.ReadBakedData();
        
        if (GUILayout.Button("Clear Baked Data"))
            querryManager.ClearBakedData();
        if (GUILayout.Button("Forget Baked Data"))
            querryManager.ForgetBakedData();
        
        if(querryManager.bakedData.Count > 0)
            showPosition = EditorGUILayout.Foldout(showPosition, "List of readed data", greenStyle);
        else
            showPosition = EditorGUILayout.Foldout(showPosition, "List of readed data", redStyle);

        if (showPosition)
            foreach (KeyValuePair<Vector3, Collider[]> data in querryManager.bakedData)
            {
                GUILayout.Label($"{data.Key} Box");
            }
        //serializedObject
    }
}
