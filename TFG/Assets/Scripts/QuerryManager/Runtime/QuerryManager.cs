﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using QFSW.QC;
using UnityEditor;
using UnityEngine;

[CommandPrefix("QuerryManager.")]
public class QuerryManager : MonoBehaviour
{
    public Transform startPosition, endPosition;
    [Command("BoxSize", "Set the box size")]
    public int boxSize = 10;
    [Range(0,1)]
    public float boxOverlap;
    public Dictionary<Vector3, Collider[]> bakedData = new Dictionary<Vector3, Collider[]>();
    public LayerMask layerMask;

    public enum DebugType
    {
        NEVER,
        ALWAYS,
        WHEN_SELECTED
    }

    public DebugType debugState;
    public bool drawWireframe;
    private void Start()
    {
        ReadBakedData();
    }
    
    public void ReadBakedData()
    {
        bakedData = new Dictionary<Vector3, Collider[]>();
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<QuerryManagerCache>() is QuerryManagerCache cache)
            {
                List<Collider> cols = new List<Collider>();
                foreach (GuidReference guidReference in cache.references)
                {
                    foreach (Collider collider in guidReference.gameObject.GetComponents<Collider>())
                    {
                        if(!collider.isTrigger)
                            cols.Add(collider);
                    }
                    
                }
                bakedData.Add(transform.GetChild(i).position, cols.ToArray());
            }
        }        
#if UNITY_EDITOR
        Debug.Log("Readed from baked data!");
#endif
    }

    public void ForgetBakedData()
    {
        bakedData.Clear();
    }

    public void ClearBakedData()
    {
        while(transform.childCount>0)
            DestroyImmediate(transform.GetChild(0).gameObject);
        
        bakedData = new Dictionary<Vector3, Collider[]>();
    }

    


    [Command("Bake", "Bake geometry of scene")]
    public void BakeGeometryAreas()
    {
        if (startPosition == null || endPosition == null)
        {
            Debug.Log("BAKE ERROR \n Start or end positions are null!");
            return;
        }
#if UNITY_EDITOR
        EditorUtility.DisplayProgressBar("Baking geometry data", "Preparing bake", 0);
#endif
        ClearBakedData();

        Vector3 startPos = new Vector3((int)startPosition.position.x,(int)startPosition.position.y,(int)startPosition.position.z), endPos = endPosition.position;
        Vector3 separation = endPos - startPos;
        Vector3 xDir = Vector3.Project(separation, Vector3.right).normalized, 
                yDir = Vector3.Project(separation, Vector3.up).normalized, 
                zDir = Vector3.Project(separation, Vector3.forward).normalized;
        
        Vector3 currentBox = startPos + xDir * boxSize/2.0f + yDir * boxSize/2.0f + zDir * boxSize/2.0f;
        Vector3 startBox = currentBox;
        
        Vector3Int iterations = new Vector3Int(
            Mathf.CeilToInt(Mathf.Abs(startPos.x - endPos.x) / boxSize),
            Mathf.CeilToInt(Mathf.Abs(startPos.y - endPos.y) / boxSize),
            Mathf.CeilToInt(Mathf.Abs(startPos.z - endPos.z) / boxSize)
        );

        int progress = 0;
        for (int x = 0; x < iterations.x; x++)
        {
            for (int y = 0; y < iterations.y; y++)
            {
                for (int z = 0; z < iterations.z; z++)
                {
#if UNITY_EDITOR
                    EditorUtility.DisplayProgressBar("Baking geometry data", $"Baking box {currentBox}", (float)progress / (float)(iterations.z * iterations.y * iterations.x));
#endif
                    
                    progress++;
                    Collider[] hitColliders = Physics.OverlapBox(currentBox, new Vector3(boxSize * (1+boxOverlap),boxSize * (1+boxOverlap),boxSize * (1+boxOverlap)) / 2.0f, Quaternion.identity, layerMask);

                    if (hitColliders.Length == 0)
                    {
                        currentBox.z += boxSize * Mathf.Sign(separation.z);
                        continue;
                    }

                    List<GuidReference> refs = new List<GuidReference>();
                    List<Collider> cols = new List<Collider>();
                    foreach (Collider c in hitColliders)
                    {
                        if (!c.isTrigger)
                        {
                            cols.Add(c);
                            GuidComponent guidComponent = c.gameObject.GetComponent<GuidComponent>();
                            if (guidComponent == null)
                            {
                                c.gameObject.AddComponent(typeof(GuidComponent));
                                guidComponent = c.gameObject.GetComponent<GuidComponent>();
                                
                            }
                            GuidReference reference = new GuidReference(guidComponent);
                            if(!refs.Contains(reference))
                                refs.Add(reference);
                        }

                    }

                    GameObject cacheObject = new GameObject($"{currentBox} cache");
                    cacheObject.transform.position = currentBox;
                    cacheObject.transform.parent = transform;
                    
                    QuerryManagerCache qm = cacheObject.AddComponent<QuerryManagerCache>();
                    qm.references = refs;

                    bakedData.Add(currentBox, cols.ToArray());

                    currentBox.z += boxSize * Mathf.Sign(separation.z);

                }
                currentBox.y += boxSize * Mathf.Sign(separation.y);
                currentBox.z = startBox.z;
            }

            currentBox.x += boxSize * Mathf.Sign(separation.x);
            currentBox.y = startBox.y;
        }
#if UNITY_EDITOR
        EditorUtility.DisplayProgressBar("Baking geometry data", "Parsing data", 1);
#endif
        ReadBakedData();
#if UNITY_EDITOR
        EditorUtility.ClearProgressBar();
#endif
        Debug.Log("Baked");
    }
#if UNITY_EDITOR

    private void OnDrawGizmosSelected()
    {
        if (debugState == DebugType.NEVER) return;
        if (bakedData.Count == 0 || debugState!= DebugType.WHEN_SELECTED) return;
        DrawAllBoxes();
    }

    private void OnDrawGizmos()
    {
        if (debugState == DebugType.NEVER) return;
        if (bakedData.Count == 0 || debugState!= DebugType.ALWAYS) return;
        DrawAllBoxes();
    }

    private void DrawAllBoxes()
    {
        Gizmos.color = new Color(1, 1, 1, 0.5f);
        foreach (KeyValuePair<Vector3, Collider[]> box in bakedData)
        {
            if(drawWireframe)
                Gizmos.DrawWireCube(box.Key, new Vector3(boxSize, boxSize, boxSize));
            else
                Gizmos.DrawCube(box.Key, new Vector3(boxSize, boxSize, boxSize));
        }
    }
#endif

    public bool GetClosestSurface(Vector3 origin, out Vector3 surfacePoint)
    {
        Vector3 first = bakedData.First().Key- Vector3.one * boxSize/2.0f;
        Vector3 offset = (origin - first )/boxSize;
        Vector3Int intOffset = new Vector3Int((int) offset.x, (int) offset.y, (int) offset.z);
        
        Vector3 quadrant = (first + intOffset * boxSize) + Vector3.one * boxSize / 2.0f;
        
        if (bakedData.ContainsKey(quadrant))
        {

            float minDist = Mathf.Infinity, currentDist = 0;
            Vector3 closestPoint= quadrant, currentPoint = Vector3.zero;
            for (int i = 0; i < bakedData[quadrant].Length; i++)
            {
                if (bakedData[quadrant][i] is MeshCollider meshCollider)
                {
                    if (meshCollider.convex)
                    {
                        currentPoint = meshCollider.ClosestPoint( meshCollider.ClosestPointOnBounds(origin));
                        currentDist = Vector3.Distance(origin, currentPoint);
                        if (currentDist < minDist)
                        {
                            closestPoint = currentPoint;
                            minDist = currentDist;
                        }
                    }
                }
                else
                {
                    currentPoint = bakedData[quadrant][i].ClosestPoint(bakedData[quadrant][i].ClosestPointOnBounds(origin));
                    currentDist = Vector3.Distance(origin, currentPoint);
                    if (currentDist < minDist)
                    {
                        closestPoint = currentPoint;
                        minDist = currentDist;
                    }
                }
                
            }
            surfacePoint = closestPoint;
            return true;
        }
        else
        {
            surfacePoint = origin;
            return false;
        }
        
    }
    
    public bool GetClosestSurfaceDev(Vector3 origin, out Vector3 surfacePoint)
    {
        Vector3 first = bakedData.First().Key- Vector3.one * boxSize/2.0f;
        Vector3 offset = (origin - first )/boxSize;
        Vector3Int intOffset = new Vector3Int((int) offset.x, (int) offset.y, (int) offset.z);
        
        Vector3 quadrant = (first + intOffset * boxSize) + Vector3.one * boxSize / 2.0f;
        
        if (bakedData.ContainsKey(quadrant))
        {

            float minDist = Mathf.Infinity, currentDist = 0;
            Vector3 closestPoint= quadrant, currentPoint = Vector3.zero;
            for (int i = 0; i < bakedData[quadrant].Length; i++)
            {
                if (bakedData[quadrant][i] is MeshCollider meshCollider)
                {
                    if (meshCollider.convex)
                    {
                        
                        Bounds b = meshCollider.bounds;
                        if (b.Contains(origin))
                        {
                            Vector3 minDir = Vector3.one;
                            float min = Mathf.Infinity;
                            if (b.IntersectRay(new Ray(origin, Vector3.up), out float distY))
                            {
                                if (distY < min)
                                {
                                    minDir = Vector3.up;
                                    min = distY;
                                }
                            }

                            if (b.IntersectRay(new Ray(origin, Vector3.right), out float distX))
                            {
                                if (distX < min)
                                {
                                    minDir = Vector3.right;
                                    min = distX;
                                }
                            }

                            if (b.IntersectRay(new Ray(origin, Vector3.forward), out float distZ))
                            {
                                if (distZ < min)
                                {
                                    minDir = Vector3.forward;
                                    min = distZ;
                                }
                            }

                            if (b.IntersectRay(new Ray(origin, -Vector3.up), out float dist_Y))
                            {
                                if (dist_Y < min)
                                {
                                    minDir = -Vector3.up;
                                    min = dist_Y;
                                }
                            }

                            if (b.IntersectRay(new Ray(origin, -Vector3.right), out float dist_X))
                            {
                                if (dist_X < min)
                                {
                                    minDir = -Vector3.right;
                                    min = dist_X;
                                }
                            }

                            if (b.IntersectRay(new Ray(origin, -Vector3.forward), out float dist_Z))
                            {
                                if (dist_Z < min)
                                {
                                    minDir = -Vector3.forward;
                                    min = dist_Z;
                                }
                            }

                            Vector3 scaledDir = minDir * min;
                            surfacePoint = origin + scaledDir;
                            return true;
                        }

                        currentPoint = meshCollider.ClosestPointOnBounds( origin);
                        currentDist = Vector3.Distance(origin, currentPoint);
                        if (currentDist < minDist)
                        {
                            closestPoint = currentPoint;
                            minDist = currentDist;
                        }
                    }
                }
                else
                {
                    Bounds b = bakedData[quadrant][i].bounds;
                    if (b.Contains(origin))
                    {
                        Vector3 minDir = Vector3.one;
                        float min = Mathf.Infinity;
                        if (b.IntersectRay(new Ray(origin, Vector3.up), out float distY))
                        {
                            if (distY < min)
                            {
                                minDir = Vector3.up;
                                min = distY;
                            }
                        }

                        if (b.IntersectRay(new Ray(origin, Vector3.right), out float distX))
                        {
                            if (distX < min)
                            {
                                minDir = Vector3.right;
                                min = distX;
                            }
                        }

                        if (b.IntersectRay(new Ray(origin, Vector3.forward), out float distZ))
                        {
                            if (distZ < min)
                            {
                                minDir = Vector3.forward;
                                min = distZ;
                            }
                        }

                        if (b.IntersectRay(new Ray(origin, -Vector3.up), out float dist_Y))
                        {
                            if (dist_Y < min)
                            {
                                minDir = -Vector3.up;
                                min = dist_Y;
                            }
                        }

                        if (b.IntersectRay(new Ray(origin, -Vector3.right), out float dist_X))
                        {
                            if (dist_X < min)
                            {
                                minDir = -Vector3.right;
                                min = dist_X;
                            }
                        }

                        if (b.IntersectRay(new Ray(origin, -Vector3.forward), out float dist_Z))
                        {
                            if (dist_Z < min)
                            {
                                minDir = -Vector3.forward;
                                min = dist_Z;
                            }
                        }

                        Vector3 scaledDir = minDir * min;
                        surfacePoint = origin + scaledDir;
                        return true;
                    }

                    currentPoint = bakedData[quadrant][i].ClosestPointOnBounds(origin);
                    currentDist = Vector3.Distance(origin, currentPoint);
                    if (currentDist < minDist)
                    {
                        closestPoint = currentPoint;
                        minDist = currentDist;
                    }
                }
                
            }
            surfacePoint = closestPoint;
            return true;
        }
        else
        {
            surfacePoint = origin;
            return false;
        }
        
    }
}
