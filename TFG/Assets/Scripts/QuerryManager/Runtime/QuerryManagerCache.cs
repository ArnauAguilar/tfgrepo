﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuerryManagerCache : MonoBehaviour
{
    public List<GuidReference> references = new List<GuidReference>();
    private QuerryManager qm;
    private void OnDrawGizmosSelected()
    {
        if (qm == null)
        {
            qm = transform.GetComponentInParent<QuerryManager>();
            return;
        }
        if (qm.debugState == QuerryManager.DebugType.NEVER) return;
        if (qm.bakedData.Count == 0) return;
        Gizmos.color = new Color(1, 1, 1, 0.5f);
        Gizmos.DrawWireCube(transform.position, new Vector3(qm.boxSize, qm.boxSize, qm.boxSize));
    }

    //private void OnTriggerStay(Collider other)
    //{
    //    if (colliders.Contains(other)) return;
    //    colliders.Add(other);
    //}
}
