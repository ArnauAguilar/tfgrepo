using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeGravityCameraHelper : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Camera.main.GetComponent<OrbitCamera>().LookCameraPosition();
            Debug.Log("LOCKED");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Camera.main.GetComponent<OrbitCamera>().UnlockCameraPosition();
            Debug.Log("UNLOCKED");
        }
    }
}
