﻿using System;
using System.Collections;
using System.Collections.Generic;
using Alone;
using UnityEngine;
using UnityEngine.Serialization;

public class EmissionManager : MonoBehaviour
{
    public enum TurnState
    {
        TURNING_OFF,
        TURNING_ON,
        ON,
        OFF
    };

    public List<Renderer> toLightUp = new List<Renderer>();
    public List<float> turnOnDuration = new List<float>();
    public bool allSameTime;
    public float timeForAll;
    
    public List<Renderer> endLightUp = new List<Renderer>();
    public float timeToLitEnd = 0;
    
    private List<Material> _materials = new List<Material>();
    private List<Material> _endMaterials = new List<Material>();
    private List<float>  endLitTimes = new List<float>();
    private float maxTime = 0;

    public float totalTimeToLit => maxTime + timeToLitEnd;

    public Signal<bool> emissionAnimationDone;
    void OnEnable()
    {
        if(allSameTime) turnOnDuration = new List<float>();
        int i = 0;
        foreach (Renderer r in toLightUp)
        {
            if(allSameTime)
                turnOnDuration.Add(timeForAll);
           _materials.Add(r.material);
           r.material.SetFloat("_EmissionTime", state == TurnState.ON ? 0 : 1);
           maxTime += turnOnDuration[i];
           endLitTimes.Add(maxTime);
           
           i++;
        }
        
        foreach (Renderer r in endLightUp)
        {
            _endMaterials.Add(r.material);
            r.material.SetFloat("_EmissionTime", state == TurnState.ON ? 0 : 1);
        }
    }

    private TurnState previousState;
    private void OnValidate()
    {
        if (state != previousState)
        {
            switch (state)
            {
                case TurnState.TURNING_OFF:
                    TurnOff();
                    break;
                case TurnState.TURNING_ON:
                    TurnOn();
                    break;
                case TurnState.ON:
                    break;
                case TurnState.OFF:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    public TurnState state = TurnState.OFF;
    private float time = 0;
    void Update()
    {
        switch (state)
        {
            case TurnState.TURNING_OFF:
                TurnOffProcess();
                break;
            case TurnState.TURNING_ON:
                TurnOnProcess();
                break;
            case TurnState.ON:
                break;
            case TurnState.OFF:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        previousState = state;
    }

    void TurnOnProcess()
    {
        if (time > maxTime || maxTime == 0)
        {
            if (time > maxTime + timeToLitEnd)
            {
                _materials[_materials.Count -1].SetFloat("_EmissionTime", 0);
                state = TurnState.ON;
                emissionAnimationDone.Dispatch(true);
            }
            else
            {
                float t = maxTime + timeToLitEnd - time;
                for (int i = 0; i < _endMaterials.Count; i++)
                {
                    t.Remap(timeToLitEnd, 0, 1, 0);
                    _endMaterials[i].SetFloat("_EmissionTime", t);
                }
                time += Time.deltaTime;
            }
            return;
        }

        for (int i = 0; i < toLightUp.Count; i++)
        {
            if (endLitTimes[i] > time)
            {
                if (i > 0)
                    _materials[i-1].SetFloat("_EmissionTime", 0);
                
                _materials[i].SetFloat("_EmissionTime", time.Remap(i - 1 < 0 ? 0 : endLitTimes[i - 1], endLitTimes[i], 1, 0));
                time += Time.deltaTime;
                return;
            }
                
        }
    }
    
    void TurnOffProcess()
    {
        if (time >= maxTime)
        {
            float t = time - maxTime;
            for (int i = 0; i < _endMaterials.Count; i++)
            {
                _endMaterials[i].SetFloat("_EmissionTime", (t - timeToLitEnd)*-1);
            }
            time -= Time.deltaTime;
            return;
        }

        if (_materials.Count == 0)
        {
            if(_endMaterials.Count > 0)
                for (int i = 0; i < _endMaterials.Count; i++)
                {
                    //t.Remap(timeToLitEnd, 0, 0, 1);
                    _endMaterials[i].SetFloat("_EmissionTime", 1);
                }
            state = TurnState.OFF;
            emissionAnimationDone.Dispatch(false);
            return;
        }
        if (time <= 0)
        {
            _materials[0].SetFloat("_EmissionTime", 1);
            state = TurnState.OFF;
            emissionAnimationDone.Dispatch(false);
            return;
        }

        for (int i = 0; i < _endMaterials.Count; i++)
        {
            _endMaterials[i].SetFloat("_EmissionTime", 1);
        }

        for (int i = 0; i < toLightUp.Count; i++)
        {
            if (endLitTimes[i] >= time)
            {
                if (i < toLightUp.Count - 1)
                    _materials[i+1].SetFloat("_EmissionTime", 1);
                
                _materials[i].SetFloat("_EmissionTime", time.Remap(endLitTimes[i], i - 1 < 0 ? 0 : endLitTimes[i - 1], 0, 1));
                time -= Time.deltaTime;
                return;
            }
                
        }
    }

    public void TurnOn()
    {
        state = TurnState.TURNING_ON;
        time = 0;
    }

    public void TurnOff()
    {
        state = TurnState.TURNING_OFF;
        time = maxTime + timeToLitEnd;
    }
}
