using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class GUIDTesting : MonoBehaviour
{
    public Collider col;
    public GuidReference reference;
    public GuidComponent component;
    [Button]
    public void FindGUID()
    {
       col = GameObject.FindObjectOfType<Collider>();
       component = col.GetComponent<GuidComponent>();
       reference = new GuidReference(component);
    }
}
