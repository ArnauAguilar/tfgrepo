﻿using UnityEngine;
using Alone.SaveAndLoad;
using Sirenix.OdinInspector;
using UnityEngine.Serialization;


namespace Alone
{
    [RequireComponent(typeof(SerializationManager), typeof(SavingSlotInformation))]
    public class GameplayManager : MonoBehaviour
    {
        public static GameplayManager Instance { get; private set; }
        
        // Serialization Manager
        [ReadOnly, SerializeField] private SerializationManager _serializationManager;
        public SerializationManager SerializationManager => _serializationManager;

        // Saving Slot Information
        [ReadOnly, SerializeField] private SavingSlotInformation _savingSlotInformation;
        public SavingSlotInformation SavingSlotInformation => _savingSlotInformation;


        // Coroutine Runner
        [ReadOnly, SerializeField] private CoroutineRunner _coroutineRunner;
        public CoroutineRunner CoroutineRunner => _coroutineRunner;
        
        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }

            _serializationManager = GetComponent<SerializationManager>();
            _savingSlotInformation = GetComponent<SavingSlotInformation>();
            _coroutineRunner = GetComponent<CoroutineRunner>();
        }
    }   
}
