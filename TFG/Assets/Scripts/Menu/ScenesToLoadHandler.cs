using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class ScenesToLoadHandler : MonoBehaviour
{
    [System.Serializable]
    public struct ProgressInfo
    {
        public string name;
        public Sprite progressSprite;
        public float progress;
        public MultiScene_SO multiSceneToLoad;
    }


    [SerializeField] private List<ProgressInfo> progressInfo = new List<ProgressInfo>();
    public List<ProgressInfo> ProgressInfoList => progressInfo;
}
