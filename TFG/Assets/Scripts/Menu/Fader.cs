using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    [SerializeField] private Image background;

    public void FadeIn(float duration)
    {
        StopAllCoroutines();
        StartCoroutine(FadeInAnimation(duration));
    }

    public void FadeOut(float duration)
    {
        StopAllCoroutines();
        StartCoroutine(FadeOutAnimation(duration));
    }

    private IEnumerator FadeInAnimation(float duration)
    {
        Color backgroundColor = Color.black;
        
        float currentTime = 0f;
        while (currentTime < duration)
        {
            backgroundColor.a = Mathf.Lerp(0, 1, currentTime / duration);
            background.color = backgroundColor;
            currentTime += Time.unscaledDeltaTime;
            yield return null;
        }
        
        background.color = Color.black;
    }
    
    private IEnumerator FadeOutAnimation(float duration)
    {
        Color backgroundColor = Color.black;
        
        float currentTime = 0f;
        while (currentTime < duration)
        {
            backgroundColor.a = Mathf.Lerp(1, 0, currentTime / duration);
            background.color = backgroundColor;
            currentTime += Time.unscaledDeltaTime;
            yield return null;
        }
        
        background.color = Color.black.Semitransparent(0);
    }
}
