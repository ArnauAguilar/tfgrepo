﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Alone.SaveAndLoad;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine.EventSystems;

namespace Alone.Menu
{
    public class MainMenu : Menu
    {
        private SerializationManager _serializationManager;
        private ScenesToLoadHandler _scenesToLoadHandler;
        private SavingSlotManager _savingSlotManager;
        [SerializeField] private NewSettingsMenu settingsMenu;
        private ConfirmationMessageManager _confirmationManager;
        private MenuInputHelper _menuInputHelper;

        [SerializeField] private Button continueButton;
        [SerializeField] private Button newGameButton;
        [SerializeField] private Button settingsButton;
        [SerializeField] private Button quitButton;

        [ReadOnly, SerializeField] private string defaultSaveFile;
        private List<ProgressSlots> _currentSlots = new List<ProgressSlots>();

        [SerializeField] private SceneReference loadingScreenScene;
        [SerializeField] private MultiScene_SO firstMultiScene;
        [SerializeField] private int tutorialProgressIndex = 0;

        [SerializeField] private GameObject mainMenu;
        [SerializeField] private GameObject topArea;
        [SerializeField] private TextMeshProUGUI menuTitleText;

        private void Start()
        {
            GameManager.Instance.menuMusicPlayer.PlayMusic();
            
            focused = true;

            _menuInputController = FindObjectOfType<MenuInputController>();
            _menuInputController.Initialize(continueButton.gameObject);
            _menuInputController.onCancelPressed.Subscribe(Quit);
            
            // Get variables
            _serializationManager = GameManager.Instance.serializationManager;
            _scenesToLoadHandler = GetComponent<ScenesToLoadHandler>();
            _savingSlotManager = GetComponent<SavingSlotManager>();
            _confirmationManager = GetComponent<ConfirmationMessageManager>();

            PauseMenu.CanPause = false;
            
            mainMenu.SetActive(true);
            InitializeButtons();

            _savingSlotManager.onCreateNewSaveSlot.Subscribe(OnCreateNewSaveSlot);
            _savingSlotManager.onDeleteSaveSlot.Subscribe(OnDestroySaveSlot);
            _savingSlotManager.onLoadSaveSlot.Subscribe(OnLoadSaveSlot);
            _savingSlotManager.onCloseSlotsPanel.Subscribe(OnCloseSlotsPanel);
            
            settingsMenu.onSettingsMenuClose.Subscribe(OnCloseSlotsPanel);
            
            _confirmationManager.onConfirm.Subscribe(ForceQuit);
            _confirmationManager.onCancel.Subscribe(OnCloseSlotsPanel);

            menuTitleText.text = "Main Menu";
            
            ToggleButton(newGameButton.gameObject, true);

            if(GameManager.Instance.isOnSwitch)
            {
                ToggleButton(quitButton.gameObject, false);

                _menuInputHelper = FindObjectOfType<MenuInputHelper>();
                _menuInputHelper.ToggleCancelBackImages(false);
                
                // All load logic of switch (maybe)
                // Si al fer el load no hi ha cap partida guardada el button de continue no hauria de ser visible
                // Amb la funcio de ToggleButton tal i com esta abaix es desactiva

                // En cas de que el continue es quedi amagat descomentar les següents linies perque el menu funcioni
                // _lastButtonSelected = _defaultButtonSelected = newGameButton.gameObject;
                // EventSystem.current.SetSelectedGameObject(_lastButtonSelected);
                
                return;
            }

            _menuInputController.ShowAndUnlockCursor();
            
            _serializationManager.CheckForDeprecatedSaveFiles();
            
            if (SaveFileExists()) 
            {
                ToggleButton(newGameButton.gameObject, false);
                SetVerticalButtonNavigation(continueButton, null, settingsButton);
                SetVerticalButtonNavigation(settingsButton, continueButton, null);
                return;
            }

            _menuInputController.Initialize(newGameButton.gameObject);
            
            ToggleButton(continueButton.gameObject, false);
        }

        private void OnDestroy()
        {
            _menuInputController.onCancelPressed.UnSubscribe(Quit);
        }

        // Buttons methods
        private void InitializeButtons()
        {
            continueButton.onClick.AddListener(Load);
            newGameButton.onClick.AddListener(NewGame);
            settingsButton.onClick.AddListener(Settings);
            quitButton.onClick.AddListener(Quit);
        }
        
        private bool SaveFileExists()
        {
            string[] currentSaveFiles = _serializationManager.GetSaveFiles();
            //return currentSaveFiles.Length > 0;

            _currentSlots = new List<ProgressSlots>();
            
            
            foreach (var t in currentSaveFiles)
            {
                int progress = _serializationManager.GetProgressIndex(t);
                ScenesToLoadHandler.ProgressInfo progressInfo = _scenesToLoadHandler.ProgressInfoList[progress];

                ProgressSlots currentSlot = new ProgressSlots
                {
                    path = t,
                    progressName = progressInfo.name,
                    progressSprite = progressInfo.progressSprite,
                    progressPercentage = progressInfo.progress
                };
                
                _currentSlots.Add(currentSlot);
            }

            return _currentSlots.Count > 0;
        }
        
        private void Continue()
        {
            GameManager.Instance.menuMusicPlayer.PauseMusic();
            
            // Esta amb un valor enorme perque si no el troba (no s'ha fet el load correctament) doni el error
            int currentProgressIndex = 1000;
            
            if (GameManager.Instance.isOnSwitch)
            {
                Debug.Log("continue switch");
                // Load the progress 
                //currentProgressIndex = The current progress loaded from the save file
                _serializationManager.Load();
            }
            else
            {
                defaultSaveFile = PlayerPrefs.GetString("DefaultSaveFile");
                _serializationManager.Load(defaultSaveFile);
                currentProgressIndex = SaveData.Current.progress;
            }

            if (_scenesToLoadHandler.ProgressInfoList.Count > currentProgressIndex &&
                _scenesToLoadHandler.ProgressInfoList[currentProgressIndex].multiSceneToLoad != null)
            {
                firstMultiScene = _scenesToLoadHandler.ProgressInfoList[currentProgressIndex].multiSceneToLoad;
            }
            else
            {
                firstMultiScene = _scenesToLoadHandler.ProgressInfoList[0].multiSceneToLoad;
                Debug.LogError($"Progress Not found: {currentProgressIndex}");
            }

            GameManager.Instance.isNewSlot = currentProgressIndex == 0;
            GameManager.Instance.currentSaveSlotPath = defaultSaveFile;
            
            SceneLoader.LoadLoadingScreenScene(loadingScreenScene, 
                firstMultiScene.mainScene, firstMultiScene.aditiveScenes);
        }

        private void NewGame()
        {
            if (GameManager.Instance.isOnSwitch)
            {
                // Save empty file with progressIndex = 0
                SaveData.Current.progress = 0;
                _serializationManager.InitializeSwitchSave();
                // -----------------------------------------------------------------------------------------------------

                SceneLoader.LoadLoadingScreenScene(loadingScreenScene, _scenesToLoadHandler.ProgressInfoList[0].multiSceneToLoad.mainScene, _scenesToLoadHandler.ProgressInfoList[0].multiSceneToLoad.aditiveScenes);
                
                return;
            }
            
            OnCreateNewSaveSlot("Slot 1");
        }

        private void OnCreateNewSaveSlot(string saveName)
        {
            defaultSaveFile = saveName;
            
            PlayerPrefs.SetString("DefaultSaveFile", saveName);

            var strArray = saveName.Split(' ');
            SaveData.Current.slotIndex = int.Parse(strArray[1]);
            SaveData.Current.progress = tutorialProgressIndex;
            
            _serializationManager.Save(saveName);

            Continue();
        }

        private void OnLoadSaveSlot(string saveName)
        {
            defaultSaveFile = saveName;
            
            PlayerPrefs.SetString("DefaultSaveFile", defaultSaveFile);
            
            var strArray = saveName.Split(' ');
            SaveData.Current.slotIndex = int.Parse(strArray[1]);

            Continue();
        }
        
        private void OnDestroySaveSlot(string path)
        {
            SerializationManager.DestroyFile(path);
            foreach (var slot in _currentSlots)
            {
                if (slot.path == path)
                {
                    _currentSlots.Remove(slot);
                    break;
                }
            }

            if (_currentSlots.Count == 0)
            {
                ToggleButton(newGameButton.gameObject);
                ToggleButton(continueButton.gameObject, false);
                
                SetVerticalButtonNavigation(newGameButton, null, settingsButton);
                SetVerticalButtonNavigation(settingsButton, newGameButton, null);

                _menuInputController.ObjectToSelectOnBack = newGameButton.gameObject;
            }
        }

        private void OnCloseSlotsPanel()
        {          
            topArea.SetActive(true);
            mainMenu.SetActive(true);
            _menuInputController.SelectObjectOnBack();
            menuTitleText.text = "Main Menu";
            StartCoroutine(WaitForSetFocused());

            if (GameManager.Instance.isOnSwitch)
            {
                _menuInputHelper.ToggleCancelBackImages(false);
            }
        }

        private IEnumerator WaitForSetFocused()
        {
            yield return new WaitForSeconds(0.05f);
            focused = true;
        }
        
        private void Load()
        {
            if (GameManager.Instance.isOnSwitch)
            {
                Continue();
                return;
            }
            
            focused = false;
            _menuInputController.ObjectToSelectOnBack = continueButton.gameObject;
            _savingSlotManager.OpenSlotsPanel(_currentSlots.ToArray());
            menuTitleText.text = "Continue";
            mainMenu.SetActive(false);
        }
        
        private void Settings()
        {
            focused = false;
            _menuInputController.ObjectToSelectOnBack = settingsButton.gameObject;
            settingsMenu.OpenSettingsMenu();
            topArea.SetActive(false);
            mainMenu.SetActive(false);
        }

        private void Quit()
        {
            if (!focused || GameManager.Instance.isOnSwitch) return;

            focused = false;

            GameObject currentSelectedButton = EventSystem.current.currentSelectedGameObject;
            
            _menuInputController.ObjectToSelectOnBack = currentSelectedButton ? 
                currentSelectedButton : quitButton.gameObject;
            _confirmationManager.OpenExitConfirmationMessage();
        }

        private static void ForceQuit()
        {
            CloseApplication();
        }
    }   
}

[System.Serializable]
public struct ProgressSlots
{
    public string path;
    public string progressName;
    public float progressPercentage;
    public Sprite progressSprite;
}
