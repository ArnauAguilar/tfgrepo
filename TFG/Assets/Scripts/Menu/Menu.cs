﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Alone.Menu
{
    public class Menu : MonoBehaviour
    {
        protected MenuInputController _menuInputController;
        
        private Canvas _canvas;

        public Signal onMenuOpen;
        public Signal onMenuClose;

        protected bool focused;
        public bool Focused { get => focused; set => focused = value; }

        protected virtual void Awake()
        {
            _menuInputController = FindObjectOfType<MenuInputController>();
            _canvas = GetComponent<Canvas>();
        }

        public void OpenMenu()
        {
            _canvas.enabled = true;
            onMenuOpen.Dispatch();
            focused = true;
        }

        public void CloseMenu()
        {
            _canvas.enabled = false;
            onMenuClose.Dispatch();
            focused = false;
        }

        protected static void CloseApplication()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
        
        protected static void ToggleButton(GameObject button, bool status = true)
        {
            button.SetActive(status);
            button.GetComponent<Button>().enabled = status;
            button.GetComponentInChildren<TextMeshProUGUI>().enabled = status;
        }

        protected static void SetVerticalButtonNavigation(Button button, Selectable upButton, Selectable downButton)
        {
            Navigation navigation = button.navigation;
            navigation.selectOnUp = upButton ? upButton : navigation.selectOnUp;
            navigation.selectOnDown = downButton ? downButton : navigation.selectOnDown;
            button.navigation = navigation;
        }
    }
}
