
using System;
using Rewired;
using TMPro;
using UnityEngine;

public class EngagementScreen : MonoBehaviour
{
    private MenuInputController _menuInputController;

    [SerializeField] private SceneReference sceneToLoad;

    [SerializeField] private GameObject keyboardInput;
    
    
    private void Start()
    {  
        _menuInputController = FindObjectOfType<MenuInputController>();
        
        _menuInputController.onInputActionsChanged.Subscribe(OnInputActionsChanged);
        InputController.SubscribeToInput(LoadMainMenuScene, "UISubmit");
        
        GameManager.Instance.menuMusicPlayer.PlayMusic();
    }

    private void OnDestroy()
    {
        _menuInputController.onInputActionsChanged.UnSubscribe(OnInputActionsChanged);
        InputController.UnSubscribeToInput(LoadMainMenuScene, "UISubmit");
    }

    private void OnInputActionsChanged(InputActionForPlatform platform)
    {
        keyboardInput.SetActive(platform.deviceType == DeviceType.Keyboard);
    }

    private void LoadMainMenuScene(InputActionEventData data)
    {
        GameManager.Instance.currentDeviceType = _menuInputController.CurrentDeviceType;
        SceneLoader.LoadMainScene(sceneToLoad);
    }
}
