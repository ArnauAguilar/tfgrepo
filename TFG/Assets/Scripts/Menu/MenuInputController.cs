
using Rewired;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuInputController : MonoBehaviour
{
    private DeviceType _lastDeviceType = DeviceType.Keyboard;
    public DeviceType CurrentDeviceType => _lastDeviceType;

    public Signal onCancelPressed;
    public Signal onKeyboardSelected;
    public Signal onGamepadSelected;

    private GameObject _defaultObjectSelected;
    private GameObject _lastObjectSelected;
    private GameObject _objectToSelectOnBack;
    public GameObject ObjectToSelectOnBack { set => _objectToSelectOnBack = value; }

    [Header("Input actions")]
    public Signal<InputActionForPlatform> onInputActionsChanged;

    [Header("Platforms")] 
    [SerializeField] private InputActionForPlatform keyboardInputActions;
    [SerializeField] private InputActionForPlatform xBoxInputActions;
    [SerializeField] private InputActionForPlatform playstationInputActions;
    [SerializeField] private InputActionForPlatform switchInputActions;

    private void Start()
    {
        GameManager.Instance.inputController.OnControlsChange.Subscribe(CheckForNewCurrentDevice);
        InputController.SubscribeToInput(DispatchCancel, GameManager.Instance.isOnSwitch ? "UISubmit" : "UICancel");

        _lastDeviceType = GameManager.Instance.currentDeviceType;

        if (GameManager.Instance.isOnSwitch)
        {
            _lastDeviceType = DeviceType.Switch;
        }
        
        ChangeInputActions(_lastDeviceType);
    }

    private void CheckForNewCurrentDevice()
    {
        DeviceType deviceType = InputController.DeviceType;
        if (deviceType != _lastDeviceType)
        {
            ChangeInputActions(deviceType);
            
            if (deviceType == DeviceType.Keyboard)
            {
                OnKeyboardSelected();
            }
            else
            {
                if (!(deviceType == DeviceType.PlayStation && _lastDeviceType == DeviceType.XBox ||
                      deviceType == DeviceType.XBox && _lastDeviceType == DeviceType.PlayStation))
                {
                    OnGamepadSelected();
                }
            }
        }

        _lastDeviceType = deviceType;
    }

    private void DispatchCancel(InputActionEventData data)
    {
        onCancelPressed.Dispatch();
    }

    public void Initialize(GameObject defaultObject, bool forceSelect = true)
    {
        _defaultObjectSelected = _lastObjectSelected = defaultObject;

        if (!forceSelect && _lastDeviceType == DeviceType.Keyboard) return;
        
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(_defaultObjectSelected);
    }

    public void SelectGameObject(GameObject gameObjectToSelect)
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(gameObjectToSelect);
    }
    
    private void OnKeyboardSelected()
    {
        _lastObjectSelected = EventSystem.current.currentSelectedGameObject;

        ShowAndUnlockCursor();
        
        EventSystem.current.SetSelectedGameObject(null);
        
        onKeyboardSelected.Dispatch();
    }

    private void OnGamepadSelected()
    {
        HideAndLockCursor();

        SelectLastGameObject();
        
        onGamepadSelected.Dispatch();
    }

    public void ShowAndUnlockCursor()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void HideAndLockCursor()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void SelectLastGameObject()
    {
        EventSystem.current.SetSelectedGameObject(null);
        if (!_lastObjectSelected) _lastObjectSelected = _defaultObjectSelected;
        EventSystem.current.SetSelectedGameObject(_lastObjectSelected);
    }

    public void SelectObjectOnBack()
    {
        if (!_objectToSelectOnBack) return;
        _lastObjectSelected = _defaultObjectSelected = _objectToSelectOnBack;
        
        if (_lastDeviceType == DeviceType.Keyboard) return;

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(_defaultObjectSelected);
    }

    private void ChangeInputActions(DeviceType currentDevice)
    {
        InputActionForPlatform currentPlatform = GameManager.Instance.isOnSwitch 
            ? switchInputActions : keyboardInputActions;
        
        if (currentDevice == DeviceType.XBox) currentPlatform = xBoxInputActions;
        else if (currentDevice == DeviceType.PlayStation) currentPlatform = playstationInputActions;

        onInputActionsChanged.Dispatch(currentPlatform);
    }
}
