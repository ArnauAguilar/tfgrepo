using Alone;
using Alone.Menu;
using Alone.SaveAndLoad;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SettingsMenu : Menu
{
    private MenuInputController _menuInputController;

    [SerializeField] private GameObject settingsPanel;

    [SerializeField] private Button audioSettingsButton;
    [SerializeField] private Button videoSettingsButton;
    [SerializeField] private VideoSettingsMenu videoSettingsMenu;
    [SerializeField] private Button keyboardSettingsButton;
    [SerializeField] private KeyboardSettingsMenu keyboardSettingsMenu;
    [SerializeField] private Button gamepadSettingsButton;
    [SerializeField] private GamepadSettingsMenu gamepadSettingsMenu;
    [SerializeField] private Button backButton;


    public Signal onClose;
    
    private GameObject _defaultButtonSelected;
    private GameObject _lastButtonSelected;

    protected override void Awake()
    {
        base.Awake();
        
        _menuInputController = FindObjectOfType<MenuInputController>();

        InitializeButtons();
        
        settingsPanel.SetActive(false);
        focused = false;
    }

    public void OnGamepadSelected()
    {
        if (!focused) return;
        
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        if (!_lastButtonSelected) _lastButtonSelected = _defaultButtonSelected;
        EventSystem.current.SetSelectedGameObject(_lastButtonSelected);
    }

    private void InitializeButtons()
    {
        audioSettingsButton.onClick.AddListener(OpenAudioSettings);
        
        videoSettingsButton.onClick.AddListener(OpenVideoSettings);
        videoSettingsMenu.onClose.Subscribe(CloseVideoSettings);
        
        keyboardSettingsButton.onClick.AddListener(OpenKeyboardSettings);
        keyboardSettingsMenu.onClose.Subscribe(CloseKeyboardSettings);
        
        gamepadSettingsButton.onClick.AddListener(OpenGamepadSettings);
        gamepadSettingsMenu.onClose.Subscribe(CloseGamepadSettings);
        
        backButton.onClick.AddListener(CloseSettingsMenu);
    }

    public void OpenSettingsMenu()
    {
        focused = true;
        
        settingsPanel.SetActive(true);
        
        _menuInputController.onCancelPressed.Subscribe(CloseSettingsMenu);

        _defaultButtonSelected = _lastButtonSelected = videoSettingsButton.gameObject;

        if (InputController.DeviceType == DeviceType.Keyboard)
        {
            //OnKeyboardSelected();
        }
        else
        {
            OnGamepadSelected();
        }
    }
    
    private void CloseSettingsMenu()
    {
        if (!focused) return;
        
        _menuInputController.onCancelPressed.UnSubscribe(CloseSettingsMenu);
        
        settingsPanel.SetActive(false);
        
        onClose.Dispatch();
        
        SaveSettings();
    }

    private void OpenVideoSettings()
    {
        focused = false;
        EventSystem.current.SetSelectedGameObject(null);
        videoSettingsMenu.OpenVideoSettingsMenu();
    }

    private void CloseVideoSettings()
    {
        focused = true;
        
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(videoSettingsButton.gameObject);
    }
    
    private void OpenKeyboardSettings()
    {
        focused = false;
        EventSystem.current.SetSelectedGameObject(null);
        keyboardSettingsMenu.OpenKeyboardSettingsMenu();
    }

    private void CloseKeyboardSettings()
    {
        focused = true;
        
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(keyboardSettingsButton.gameObject);
    }

    private void OpenGamepadSettings()
    {
        focused = false;
        EventSystem.current.SetSelectedGameObject(null);
        gamepadSettingsMenu.OpenGamepadSettingsMenu();
    }
    
    private void CloseGamepadSettings()
    {
        focused = true;
        
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(gamepadSettingsButton.gameObject);
    }

    private void OpenAudioSettings()
    {
        Debug.Log("Open Audio");
    }

    private static void SaveSettings()
    {
        SerializationManager serializationManager = FindObjectOfType<SerializationManager>();
        serializationManager.SaveSettings(GameManager.Instance.settings);
    }
}
