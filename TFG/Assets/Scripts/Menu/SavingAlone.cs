using System.Collections;
using UnityEngine;

public class SavingAlone : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvasGroup;

    private void Start()
    {
        HideCanvasGroup();
    }
    
    private void HideCanvasGroup()
    {
        canvasGroup.gameObject.SetActive(false);
    }

    public void AnimateSaving(float fadeInDuration = 0.3f, float animationDuration = 2f, float fadeOutDuration = 0.3f,
        float initialValue = 0, float finalValue = 1)
    {
        StartCoroutine(Animate(fadeInDuration, animationDuration, fadeOutDuration, initialValue, finalValue));
    }

    private IEnumerator Animate(float fadeInDuration, float animationDuration, float fadeOutDuration,
        float initialValue, float finalValue)
    {
        canvasGroup.gameObject.SetActive(true);
        
        canvasGroup.alpha = initialValue;
        
        float currentDuration = 0;
        while (currentDuration < fadeInDuration)
        {
            currentDuration += Time.unscaledDeltaTime;
            canvasGroup.alpha = Mathf.Lerp(initialValue, finalValue, currentDuration / fadeInDuration);
            yield return null;
        }

        canvasGroup.alpha = finalValue;
        
        yield return new WaitForSecondsRealtime(animationDuration);

        currentDuration = 0;
        while (currentDuration < fadeOutDuration)
        {
            currentDuration += Time.unscaledDeltaTime;
            canvasGroup.alpha = Mathf.Lerp(finalValue, initialValue, currentDuration / fadeInDuration);
            yield return null;
        }
        
        canvasGroup.alpha = initialValue;

        if (initialValue == 0) HideCanvasGroup();
    }
}
