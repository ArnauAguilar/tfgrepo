
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InteractCanvasManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI interactMessage;
    [SerializeField] private Image interactImage;

    [SerializeField] private Sprite interactButtonKeyboard;
    [SerializeField] private Sprite interactButtonXbox;
    [SerializeField] private Sprite interactButtonPS4;
    [SerializeField] private Sprite interactButtonSwitch;
    
    private void Start()
    {
        OnControlsChanged();
        GameManager.Instance.inputController.OnControlsChange.Subscribe(OnControlsChanged);
        
        HideInteractMessage();
    }

    private void OnDestroy()
    {        
        GameManager.Instance.inputController.OnControlsChange.UnSubscribe(OnControlsChanged);
    }

    private void OnControlsChanged()
    {
        interactImage.sprite = InputController.DeviceType switch
        {
            DeviceType.Keyboard => interactButtonKeyboard,
            DeviceType.XBox => interactButtonXbox,
            DeviceType.PlayStation => interactButtonPS4,
            _ => interactButtonSwitch
        };
    }
    
    public void ShowInteractMessage()
    {
        interactMessage.gameObject.SetActive(true);
        interactImage.gameObject.SetActive(true);
    }

    public void HideInteractMessage()
    {
        interactMessage.gameObject.SetActive(false);
        interactImage.gameObject.SetActive(false);
    }
}
