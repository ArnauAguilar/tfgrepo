﻿using System;
using Alone.SaveAndLoad;
using UnityEngine;
using UnityEngine.UI;

namespace Alone.Menu
{
    public class SavingSlotManager : MonoBehaviour
    {
        private MenuInputController _menuInputController;
        private MenuInputHelper _menuInputHelper;
        
        [SerializeField] private GameObject slotsPanel;
        [SerializeField] private Button backButton;

        [SerializeField] private SaveSlot[] slots;

        public Signal<string> onCreateNewSaveSlot;
        public Signal<string> onDeleteSaveSlot;
        public Signal<string> onLoadSaveSlot;

        public Signal onCloseSlotsPanel;

        private void Awake()
        {
            _menuInputController = FindObjectOfType<MenuInputController>();
            _menuInputHelper = FindObjectOfType<MenuInputHelper>();
            
            CloseSlotsPanel();
            
            backButton.onClick.AddListener(OnCloseSlotsPanel);
        }

        private void Start()
        {
            InputController.SubscribeToInput(OnDeleteSlotButtonDown, "UIDelete");
            InputController.SubscribeToInputUp(OnDeleteSlotButtonUp, "UIDelete");
        }

        private void OnDestroy()
        {
            InputController.UnSubscribeToInput(OnDeleteSlotButtonDown, "UIDelete");
            InputController.UnSubscribeToInputUp(OnDeleteSlotButtonUp, "UIDelete");
            UnsubscribeFromSlots();
        }


        private void InitializeSlots(ProgressSlots[] progressSlots)
        {
            foreach (var progressSlot in progressSlots)
            {
                foreach (var slot in slots)
                {
                    if (!progressSlot.path.Contains(slot.SaveSlotName)) continue;
                    
                    slot.InitializeSaveSlot(progressSlot.path, progressSlot.progressName, 
                        progressSlot.progressPercentage, progressSlot.progressSprite);
                    break;
                }
            }

            foreach (var slot in slots)
            {
                slot.onSaveSlotPressed.Subscribe(OnSaveSlotPressedLoad);
                slot.onSaveSlotDeleted.Subscribe(OnSaveSlotDeleted);
            }

            _menuInputController.onCancelPressed.Subscribe(OnCloseSlotsPanel);
            _menuInputController.Initialize(slots[0].SlotButton.gameObject);
        }
        
        public void OpenSlotsPanel(ProgressSlots[] progressSlots)
        {
            slotsPanel.SetActive(true);
            InitializeSlots(progressSlots);
            _menuInputHelper.ToggleDeleteImages(true);
        }

        private void OnCloseSlotsPanel()
        {
            CloseSlotsPanel();
            UnsubscribeFromSlots();
            onCloseSlotsPanel.Dispatch();
            _menuInputHelper.ToggleDeleteImages(false);
        }
        
        private void CloseSlotsPanel()
        {
            slotsPanel.SetActive(false);
        }

        private void UnsubscribeFromSlots()
        {
            _menuInputController.onCancelPressed.UnSubscribe(OnCloseSlotsPanel);

            foreach (var slot in slots)
            {
                slot.onSaveSlotPressed.UnSubscribe(OnSaveSlotPressedLoad);
                slot.onSaveSlotDeleted.UnSubscribe(OnSaveSlotDeleted);
            }
        }

        private void OnDeleteSlotButtonDown(Rewired.InputActionEventData data)
        {
            foreach (var saveSlot in slots) saveSlot.StartDeletingSaveSlot();
        }

        private void OnDeleteSlotButtonUp(Rewired.InputActionEventData data)
        {
            foreach (var saveSlot in slots) saveSlot.StopDeletingSaveSlot();
        }
        
        private void OnSaveSlotPressedLoad(SaveSlot saveSlot)
        {
            if (string.IsNullOrEmpty(saveSlot.FilePath))
            {
                onCreateNewSaveSlot.Dispatch(saveSlot.SaveSlotName);
            }
            else
            {
                onLoadSaveSlot.Dispatch(saveSlot.SaveSlotName);
            }
        }

        private void OnSaveSlotDeleted(SaveSlot saveSlot)
        {
            onDeleteSaveSlot.Dispatch(saveSlot.FilePath);
        }
    }   
}
