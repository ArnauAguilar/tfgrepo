using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
 
public class EventSensitiveScrollRect : MonoBehaviour
{
    [SerializeField] private ExtendedDropdown dropDown;
    [SerializeField] private Scrollbar scrollbar;

    private void Awake()
    {
        scrollbar.value = 1 - dropDown.value / (float)dropDown.options.Count;
    }
}