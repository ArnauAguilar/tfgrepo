
using UnityEngine;
using UnityEngine.UI;

public class CustomOptionButton : MonoBehaviour
{
    [SerializeField] private Button button;
    public Button Button => button;

    public Signal<int> onOptionSelected;

    [SerializeField] private int optionNumber;

    private void Awake()
    {
        button.onClick.AddListener(OnOptionSelected);
    }

    private void OnDestroy()
    {
        button.onClick.RemoveListener(OnOptionSelected);
    }

    private void OnOptionSelected()
    {
        onOptionSelected.Dispatch(optionNumber);
    }
}
