using System.Collections;
using System.Collections.Generic;
using Rewired.Integration.UnityUI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ExtendedSlider : Slider
{
    private RewiredStandaloneInputModule _rewiredStandaloneInputModule;
    
    public Graphic[] objects;

    protected override void Start()
    {
        _rewiredStandaloneInputModule = FindObjectOfType<RewiredStandaloneInputModule>();
        
        base.Start();
    }

    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);

        SelectSlider();
    }
    
    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        
        SelectSlider();
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        base.OnDeselect(eventData);

        DeselectSlider();
    }
    
    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        
        DeselectSlider();
    }

    private void SelectSlider()
    {
        foreach (var obj in objects)
        {
            obj.color = colors.highlightedColor;
        }

        if (!_rewiredStandaloneInputModule)
            _rewiredStandaloneInputModule = FindObjectOfType<RewiredStandaloneInputModule>();
        
        _rewiredStandaloneInputModule.inputActionsPerSecond = 30;
        StartCoroutine(WaitForSetNewInputActionsPerSeconds());
    }

    private IEnumerator WaitForSetNewInputActionsPerSeconds()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        _rewiredStandaloneInputModule.MoveOneElementPerAxisPress = false;
    }
    
    private void DeselectSlider()
    {
        foreach (var obj in objects)
        {
            obj.color = colors.normalColor;
        }
        
        if (!_rewiredStandaloneInputModule)
            _rewiredStandaloneInputModule = FindObjectOfType<RewiredStandaloneInputModule>();
        _rewiredStandaloneInputModule.MoveOneElementPerAxisPress = true;
        _rewiredStandaloneInputModule.inputActionsPerSecond = 10;
        StopAllCoroutines();
    }
}
