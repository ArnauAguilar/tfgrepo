using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class OptionSelector : MonoBehaviour
{
    [System.Serializable]
    private struct Option
    {
        public CustomOptionButton optionButton;
        public Graphic[] graphics;
    }
    
    [SerializeField] private Option[] options;
    [SerializeField] private Color selectedColor;
    [SerializeField] private Color unselectedColor;

    private int optionIndex;
    public int OptionIndex => optionIndex;

    private void Awake()
    {
        foreach (var option in options)
        {
            option.optionButton.onOptionSelected.Subscribe(SelectOption);
        }
    }

    private void OnDestroy()
    {
        foreach (var option in options)
        {
            option.optionButton.onOptionSelected.UnSubscribe(SelectOption);
        }
    }

    public void SelectOption(int optionNumber)
    {
        optionIndex = optionNumber;
        for (int i = 0; i < options.Length; i++)
        {
            foreach (var graphic in options[i].graphics)
            {
                graphic.color = i == optionIndex ? selectedColor : unselectedColor;
            }
        }
    }

    public void AddCallback(UnityAction callback)
    {
        foreach (var option in options)
        {
            option.optionButton.Button.onClick.AddListener(callback);
        }
    }
}
