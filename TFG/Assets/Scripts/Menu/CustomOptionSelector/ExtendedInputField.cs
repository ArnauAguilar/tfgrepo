using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;

public class ExtendedInputField : MonoBehaviour
{
    private TMP_InputField _inputField;
    private MenuInputController _menuInputController;

    [SerializeField] private ExtendedSlider extendedSlider;

    private void Awake()
    {
        _inputField = GetComponent<TMP_InputField>();
        _menuInputController = FindObjectOfType<MenuInputController>();
    }

    private void Start()
    {
        _menuInputController.onKeyboardSelected.Subscribe(OnKeyboardSelected);
        _menuInputController.onGamepadSelected.Subscribe(OnGamepadSelected);
        
        _inputField.onEndEdit.AddListener(OnEndEdit);
        extendedSlider.onValueChanged.AddListener(SetValueFromSlider);
    }

    private void OnDestroy()
    {
        _menuInputController.onKeyboardSelected.UnSubscribe(OnKeyboardSelected);
        _menuInputController.onGamepadSelected.UnSubscribe(OnGamepadSelected);
        
        _inputField.onEndEdit.RemoveListener(OnEndEdit);
        extendedSlider.onValueChanged.RemoveListener(SetValueFromSlider);
    }

    private void OnKeyboardSelected()
    {
        _inputField.readOnly = false;
    }

    private void OnGamepadSelected()
    {
        _inputField.readOnly = true;
    }

    private void SetValueFromSlider(float value)
    {
        _inputField.text = ((int)value).ToString();
    }
    
    private void OnEndEdit(string value)
    {
        extendedSlider.value = int.Parse(value);
    }
}
