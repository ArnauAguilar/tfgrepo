
using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CustomOptionSelector : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private Button leftButton;
    public Button LeftButton => leftButton;
    [SerializeField] private Button rightButton;
    [SerializeField] private TextMeshProUGUI label;
    
    [Header("Options")]
    [SerializeField] private string[] options;

    [SerializeField] private int initialOption;
    public int InitialOption { set => initialOption = value; }

    private int _currentOption;
    public int CurrentOption { get => _currentOption; }

    private void Awake()
    {
        UpdateLabel();
    }

    public void SetOptions(string[] newOptions)
    {
        options = newOptions;
    }
    
    public void UpdateInitialOption(int option)
    {
        initialOption = _currentOption = option;
        UpdateLabel();
    }

    public void UpdateInitialOption(bool binary)
    {
        UpdateInitialOption(Convert.ToInt32(binary));
    }
    
    private void UpdateLabel()
    {
        label.text = options[_currentOption];
    }
    
    public void PreviousOption()
    {
        _currentOption = (_currentOption - 1 + options.Length) % options.Length;
        UpdateLabel();
    }

    public void NextOption()
    {
        _currentOption = (_currentOption + 1) % options.Length;
        UpdateLabel();
    }

    public void Reset()
    {
        _currentOption = initialOption;
        UpdateLabel();
    }
}