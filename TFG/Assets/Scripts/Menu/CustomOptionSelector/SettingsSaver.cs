using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsSaver : MonoBehaviour
{
    private AloneSettings _aloneSettings;
    
    [Header("DISPLAY SETTINGS")]
    [SerializeField] private OptionSelector displayModeSelector;
    [SerializeField] private Dropdown displayResolution;
    [SerializeField] private Toggle displayVSync;

    [Header("CONTROLS SETTINGS")]
    [SerializeField] private Slider controlsMouseSensitivity;
    [SerializeField] private Toggle controlsInvertVerticalMouse;
    [Space]
    [SerializeField] private Slider controlsGamepadSensitivity;
    [SerializeField] private OptionSelector controlsStickMoveLook;
    [SerializeField] private Toggle controlInvertVerticalStick;
    [SerializeField] private Toggle controlVibrationStrength;

    private void Awake()
    {
        
    }
}
