using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ExtendedToggle : Toggle
{
    public Graphic[] objects;
    
    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);

        foreach (var obj in objects)
        {
            obj.color = colors.highlightedColor;
        }
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        
        foreach (var obj in objects)
        {
            obj.color = colors.highlightedColor;
        }
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        base.OnDeselect(eventData);
        
        foreach (var obj in objects)
        {
            obj.color = colors.normalColor;
        }
    }
    
    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        
        foreach (var obj in objects)
        {
            obj.color = colors.normalColor;
        }
    }
}
