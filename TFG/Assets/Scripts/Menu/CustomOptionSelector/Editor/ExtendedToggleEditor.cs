
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ExtendedToggle))]
public class ExtendedToggleEditor : UnityEditor.UI.ToggleEditor
{
    private SerializedObject extendedToggle;
    private SerializedProperty objects;

    protected override void OnEnable()
    {
        base.OnEnable();
        
        extendedToggle = new SerializedObject(target);
    }

    public override void OnInspectorGUI()
    {
        objects = extendedToggle.FindProperty("objects");
        EditorGUILayout.PropertyField(objects, new GUIContent("Objects"), true);
        
        base.OnInspectorGUI();
        extendedToggle.ApplyModifiedProperties();
    }
}
