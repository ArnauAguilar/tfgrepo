using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ExtendedButton))]
public class ExtendedButtonEditor : UnityEditor.UI.ButtonEditor
{
    private SerializedObject extendedButton;
    private SerializedProperty objects;

    protected override void OnEnable()
    {
        base.OnEnable();
        
        extendedButton = new SerializedObject(target);
    }

    public override void OnInspectorGUI()
    {
        objects = extendedButton.FindProperty("objects");
        EditorGUILayout.PropertyField(objects, new GUIContent("Objects"), true);
        
        base.OnInspectorGUI();
        extendedButton.ApplyModifiedProperties();
    }
}
