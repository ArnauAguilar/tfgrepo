
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ExtendedDropdown))]
public class ExtendedDropdownEditor : UnityEditor.UI.DropdownEditor
{
    private SerializedObject extendedDropdown;
    private SerializedProperty objects;
    private SerializedProperty color;

    protected override void OnEnable()
    {
        base.OnEnable();
        
        extendedDropdown = new SerializedObject(target);
    }

    public override void OnInspectorGUI()
    {
        objects = extendedDropdown.FindProperty("objects");
        color = extendedDropdown.FindProperty("color");
        EditorGUILayout.PropertyField(objects, new GUIContent("Objects"), true);
        EditorGUILayout.PropertyField(color, new GUIContent("Color"));
        
        base.OnInspectorGUI();
        extendedDropdown.ApplyModifiedProperties();
    }
    
}
