
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ExtendedSlider))]
public class ExtendedSliderEditor : UnityEditor.UI.SliderEditor
{
    private SerializedObject extendedSlider;
    private SerializedProperty objects;

    protected override void OnEnable()
    {
        base.OnEnable();
        
        extendedSlider = new SerializedObject(target);
    }
    
    public override void OnInspectorGUI()
    {
        objects = extendedSlider.FindProperty("objects");
        
        EditorGUILayout.PropertyField(objects, new GUIContent("Objects"), true);

        base.OnInspectorGUI();
        extendedSlider.ApplyModifiedProperties();
    }
}
