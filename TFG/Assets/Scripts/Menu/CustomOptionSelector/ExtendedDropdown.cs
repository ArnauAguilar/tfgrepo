using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ExtendedDropdown : TMP_Dropdown
{
    public Graphic[] objects;
    public Color color;
    public Signal onSubmit;

    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);

        foreach (var obj in objects)
        {
            obj.color = color;
        }
    }

    public override void OnSubmit(BaseEventData eventData)
    {
        base.OnSubmit(eventData);
        
        foreach (var obj in objects)
        {
            obj.color = color;
        }

        onSubmit.Dispatch();
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        
        foreach (var obj in objects)
        {
            obj.color = color;
        }
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        base.OnDeselect(eventData);
        
        foreach (var obj in objects)
        {
            obj.color = colors.normalColor;
        }
    }
    
    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        
        foreach (var obj in objects)
        {
            obj.color = colors.normalColor;
        }
    }
}
