﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Alone.Menu
{
    public class PauseMenu : Menu
    {
        [SerializeField] private NewSettingsMenu settingsMenu;
        
        [Header("Buttons")]
        [SerializeField] private Button resumeButton;
        [SerializeField] private Button settingsButton;
        [SerializeField] private Button mainMenuButton;
        [SerializeField] private ConfirmationMessageManager mainMenuConfirmationMessageManager;
        [SerializeField] private Button quitButton;
        [SerializeField] private ConfirmationMessageManager quitConfirmationMessageManager;

        [Header("Other Settings")] 
        [SerializeField] private int mainMenuBuildIndex;

        [Header("Pause Menu Areas")] 
        [SerializeField] private GameObject pauseMenu;
        [SerializeField] private GameObject topArea;

        public static bool CanPause { get; set; }

        protected override void Awake()
        {
            base.Awake();

            CanPause = true;
        }
        
        private void Start()
        {
            focused = true;
            (IsPaused() ? (Action)Pause : Resume)();
            InitializeButtons();

            if (GameManager.Instance.isOnSwitch)
            {
                ToggleButton(quitButton.gameObject, false);
            }
            
            settingsMenu.onSettingsMenuClose.Subscribe(OnFocusPauseMenu);
            
            mainMenuConfirmationMessageManager.onConfirm.Subscribe(ForceMainMenu);
            mainMenuConfirmationMessageManager.onCancel.Subscribe(OnFocusPauseMenu);
            
            quitConfirmationMessageManager.onConfirm.Subscribe(ForceQuit);
            quitConfirmationMessageManager.onCancel.Subscribe(OnFocusPauseMenu);
            
            InputController.SubscribeToInput(OnPauseButtonPressed, "Pause");
        }

        private void OnDestroy()
        {
            InputController.UnSubscribeToInput(OnPauseButtonPressed, "Pause");
        }

        private void InitializeButtons()
        {
            resumeButton.onClick.AddListener(Resume);
            settingsButton.onClick.AddListener(Settings);
            mainMenuButton.onClick.AddListener(OpenMainMenuConfirmationMessage);
            quitButton.onClick.AddListener(OpenQuitConfirmationMessage);
        }

        public static bool IsPaused()
        {
            return Time.timeScale == 0;
        }

        private void OnPauseButtonPressed(Rewired.InputActionEventData data)
        {
            if (CanPause)
            {
                (IsPaused() ? (Action)Resume : Pause)();
            }
        }
        
        private void Pause()
        {
            focused = true;
            Time.timeScale = 0f;
            OpenMenu();
            
            _menuInputController.Initialize(resumeButton.gameObject);
            _menuInputController.ShowAndUnlockCursor();
        }
        
        private void Resume()
        {
            if (!focused) return;

            CloseMenu();
            _menuInputController.HideAndLockCursor();
            _menuInputController.SelectGameObject(null);
            
            StartCoroutine(WaitToResume());
        }

        private static IEnumerator WaitToResume()
        {
            yield return new WaitForSecondsRealtime(0.05f);
            Time.timeScale = 1f;
        }

        private void Settings()
        {
            focused = false;

            topArea.SetActive(false);
            pauseMenu.SetActive(false);
            _menuInputController.ObjectToSelectOnBack = settingsButton.gameObject;
            settingsMenu.OpenSettingsMenu();
        }

        private void OnFocusPauseMenu()
        {
            focused = true;
            topArea.SetActive(true);
            pauseMenu.SetActive(true);
            _menuInputController.SelectObjectOnBack();
        }

        private void OpenMainMenuConfirmationMessage()
        {
            if (!focused) return;

            focused = false;

            _menuInputController.ObjectToSelectOnBack = mainMenuButton.gameObject;
            mainMenuConfirmationMessageManager.OpenExitConfirmationMessage();
        }
        
        private void ForceMainMenu()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(mainMenuBuildIndex);
        }

        private void OpenQuitConfirmationMessage()
        {
            if (!focused) return;

            focused = false;

            _menuInputController.ObjectToSelectOnBack = quitButton.gameObject;
            quitConfirmationMessageManager.OpenExitConfirmationMessage();
        }
        
        private void ForceQuit()
        {
            CloseApplication();
        }
    }
}
