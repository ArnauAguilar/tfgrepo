using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AloneSettings
{
    // Video Settings
    public int resolutionIndex;
    public int presentationMode;
    public bool vSync;
    
    // Audio Settings
    public float masterVolume;
    public float musicVolume;
    public float effectsVolume;
    
    // Keyboard and mouse Settings
    public float keyboardRotationSpeed;
    public bool invertMouse;
    public bool mouseAcceleration;
    
    // Gamepad Settings
    public bool useRightHandedStickMoveLook;
    public float gamepadRotationSpeed;
    public bool invertLook;
    public int vibrationStrength;

    public static Resolution[] GetResolutions()
    {
        return Screen.resolutions;
    }
    
    public static List<string> GetResolutionsFormatted(out int currentResolutionIndex)
    {
        Resolution[] resolutions = Screen.resolutions;
        List<string> resolutionsOptions = new List<string>();

        Resolution currentResolution = Screen.currentResolution;
        currentResolutionIndex = -1;
        
        for(int i = 0; i < resolutions.Length; i++)
        {
            resolutionsOptions.Add(resolutions[i].ToString());

            if (resolutions[i].width == currentResolution.width && resolutions[i].height == currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        return resolutionsOptions;
    }

    public void ResetVideoSettings()
    {
        resolutionIndex = -1;
        presentationMode = 0;
        vSync = true;
    }

    public void ResetAudioSettings()
    {
        masterVolume = 50;
        musicVolume = 50;
        effectsVolume = 50;
        
        GameManager.Instance.ApplyMixerVolumeValue(masterVolume, "Master");
        GameManager.Instance.ApplyMixerVolumeValue(musicVolume, "Music");
        GameManager.Instance.ApplyMixerVolumeValue(effectsVolume, "Effects");
    }
    
    public void ResetKeyboardAndMouseSettings()
    {
        keyboardRotationSpeed = 25;
        invertMouse = false;
        mouseAcceleration = true;
    }

    public void ResetGamepadSettings()
    {
        useRightHandedStickMoveLook = true;
        gamepadRotationSpeed = 50;
        invertLook = false;
        vibrationStrength = 50;
    }
}
