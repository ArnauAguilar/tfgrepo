using System;
using Alone.Menu;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GamepadSettingsMenu : Menu
{
    private MenuInputController _menuInputController;
    private AloneSettings _aloneSettings;
    
    [SerializeField] private GameObject gamepadSettingsPanel;

    [SerializeField] private CustomOptionSelector rightLeftHandedSticks;
    [SerializeField] private Slider gamepadSensitivity;
    [SerializeField] private CustomOptionSelector invertLook;

    [SerializeField] private Button backButton;

    public Signal onClose;
    
    private GameObject _defaultSettingSelected;
    private GameObject _lastSettingSelected;
    
    protected override void Awake()
    {
        base.Awake();

        _menuInputController = FindObjectOfType<MenuInputController>();

        gamepadSettingsPanel.SetActive(false);
        focused = false;

        gamepadSensitivity.minValue = 1;
        gamepadSensitivity.maxValue = 200;
    }

    private void Start()
    {
        InitializeButtons();
    }

    public void OnGamepadSelected()
    {
        if (!focused) return;
        
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        if (!_lastSettingSelected) _lastSettingSelected = _defaultSettingSelected;
        EventSystem.current.SetSelectedGameObject(_lastSettingSelected);
    }
    
    private void InitializeButtons()
    {
        backButton.onClick.AddListener(CloseGamepadSettingsMenu);
    }

    public void OpenGamepadSettingsMenu()
    {
        focused = true;

        _aloneSettings = GameManager.Instance.settings;
        
        gamepadSettingsPanel.SetActive(true);

        _menuInputController.onCancelPressed.Subscribe(CloseGamepadSettingsMenu);

        _defaultSettingSelected = _lastSettingSelected = rightLeftHandedSticks.LeftButton.gameObject;
        
        if (InputController.DeviceType == DeviceType.Keyboard)
        {
            //OnKeyboardSelected();
        }
        else
        {
            OnGamepadSelected();
        }

        rightLeftHandedSticks.UpdateInitialOption(_aloneSettings.useRightHandedStickMoveLook);
        gamepadSensitivity.value = _aloneSettings.gamepadRotationSpeed;
        invertLook.UpdateInitialOption(_aloneSettings.invertLook);
    }

    private void CloseGamepadSettingsMenu()
    {
        if (!focused) return;
        
        _menuInputController.onCancelPressed.UnSubscribe(CloseGamepadSettingsMenu);
        
        gamepadSettingsPanel.SetActive(false);

        _aloneSettings.useRightHandedStickMoveLook = Convert.ToBoolean(rightLeftHandedSticks.CurrentOption);
        _aloneSettings.gamepadRotationSpeed = gamepadSensitivity.value;
        _aloneSettings.invertLook = Convert.ToBoolean(invertLook.CurrentOption);
        
        OrbitCamera orbitCamera = FindObjectOfType<OrbitCamera>();
        if (orbitCamera)
        {
            orbitCamera.UseRightHandedScheme = _aloneSettings.useRightHandedStickMoveLook;
            orbitCamera.GamepadRotationSpeed = _aloneSettings.gamepadRotationSpeed;
            orbitCamera.InvertedYAxisGamepad = _aloneSettings.invertLook;
        }

        MovingSphere movingSphere = FindObjectOfType<MovingSphere>();
        if (movingSphere)
        {
            movingSphere.UseRightHandedScheme = _aloneSettings.useRightHandedStickMoveLook;
        }
        
        onClose.Dispatch();
    }
}
