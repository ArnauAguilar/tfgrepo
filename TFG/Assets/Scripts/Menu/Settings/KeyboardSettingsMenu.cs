
using System;
using Alone;
using Alone.Menu;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class KeyboardSettingsMenu : Menu
{
    private MenuInputController _menuInputController;
    private AloneSettings _aloneSettings;
    
    [SerializeField] private GameObject keyboardSettingsPanel;

    [SerializeField] private Slider keyboardSensitivity;
    [SerializeField] private CustomOptionSelector invertMouse;
    [SerializeField] private CustomOptionSelector mouseAcceleration;
    [SerializeField] private Button backButton;

    public Signal onClose;
    
    private GameObject _defaultSettingSelected;
    private GameObject _lastSettingSelected;
    
    protected override void Awake()
    {
        base.Awake();

        _menuInputController = FindObjectOfType<MenuInputController>();

        keyboardSettingsPanel.SetActive(false);
        focused = false;

        keyboardSensitivity.minValue = 1;
        keyboardSensitivity.maxValue = 50;
    }

    private void Start()
    {
        InitializeButtons();
    }

    public void OnGamepadSelected()
    {
        if (!focused) return;
        
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        if (!_lastSettingSelected) _lastSettingSelected = _defaultSettingSelected;
        EventSystem.current.SetSelectedGameObject(_lastSettingSelected);
    }
    
    private void InitializeButtons()
    {
        backButton.onClick.AddListener(CloseKeyboardSettingsMenu);
    }

    public void OpenKeyboardSettingsMenu()
    {
        focused = true;

        _aloneSettings = GameManager.Instance.settings;
            
        keyboardSettingsPanel.SetActive(true);

        _menuInputController.onCancelPressed.Subscribe(CloseKeyboardSettingsMenu);

        _defaultSettingSelected = _lastSettingSelected = keyboardSensitivity.gameObject;
        
        if (InputController.DeviceType == DeviceType.Keyboard)
        {
            //OnKeyboardSelected();
        }
        else
        {
            OnGamepadSelected();
        }

        keyboardSensitivity.value = _aloneSettings.keyboardRotationSpeed;
        invertMouse.UpdateInitialOption(_aloneSettings.invertMouse);
        mouseAcceleration.UpdateInitialOption(_aloneSettings.mouseAcceleration);
    }

    private void CloseKeyboardSettingsMenu()
    {
        if (!focused) return;
        
        _menuInputController.onCancelPressed.UnSubscribe(CloseKeyboardSettingsMenu);
        
        keyboardSettingsPanel.SetActive(false);

        _aloneSettings.keyboardRotationSpeed = keyboardSensitivity.value;
        _aloneSettings.invertMouse = Convert.ToBoolean(invertMouse.CurrentOption);
        _aloneSettings.mouseAcceleration = Convert.ToBoolean(mouseAcceleration.CurrentOption);

        OrbitCamera orbitCamera = FindObjectOfType<OrbitCamera>();
        if (orbitCamera)
        {
            orbitCamera.KeyboardRotationSpeed = _aloneSettings.keyboardRotationSpeed;
            orbitCamera.InvertedYAxisMouse = _aloneSettings.invertMouse;
            orbitCamera.UseMouseAcceleration = _aloneSettings.mouseAcceleration;
        }
        
        onClose.Dispatch();
    }
}
