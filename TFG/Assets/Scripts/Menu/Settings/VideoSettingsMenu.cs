using System;
using Alone.Menu;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VideoSettingsMenu : Menu
{
    private MenuInputController _menuInputController;
    private AloneSettings _aloneSettings;
    
    [SerializeField] private GameObject videoSettingsMenu;
    
    [SerializeField] private CustomOptionSelector resolution;
    [SerializeField] private CustomOptionSelector presentationMode;
    [SerializeField] private CustomOptionSelector vsync;
    [SerializeField] private CustomOptionSelector cameraShake;
    
    [SerializeField] private Button backButton;

    public Signal onClose;
    
    private GameObject _defaultSettingSelected;
    private GameObject _lastSettingSelected;

    protected override void Awake()
    {
        base.Awake();

        _menuInputController = FindObjectOfType<MenuInputController>();

        videoSettingsMenu.SetActive(false);
        focused = false;
    }
    
    private void Start()
    {
        InitializeButtons();
    }

    public void OnKeyboardSelected()
    {
        _lastSettingSelected = EventSystem.current.currentSelectedGameObject;

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
            
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void OnGamepadSelected()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        if (!_lastSettingSelected) _lastSettingSelected = _defaultSettingSelected;
        EventSystem.current.SetSelectedGameObject(_lastSettingSelected);
    }
    
    private void InitializeButtons()
    {
        backButton.onClick.AddListener(CloseVideoSettingsMenu);
    }
    
    public void OpenVideoSettingsMenu()
    {
        focused = true;

        _aloneSettings = GameManager.Instance.settings;
        
        videoSettingsMenu.SetActive(true);

        _menuInputController.onCancelPressed.Subscribe(CloseVideoSettingsMenu);

        _defaultSettingSelected = _lastSettingSelected = resolution.LeftButton.gameObject;
        
        if (InputController.DeviceType == DeviceType.Keyboard)
        {
            OnKeyboardSelected();
        }
        else
        {
            OnGamepadSelected();
        }
        
        resolution.SetOptions(AloneSettings.GetResolutionsFormatted(out int currentResolutionIndex).ToArray());
        resolution.UpdateInitialOption(_aloneSettings.resolutionIndex == -1 ? 
            currentResolutionIndex : _aloneSettings.resolutionIndex);
        presentationMode.UpdateInitialOption(_aloneSettings.presentationMode);
        vsync.UpdateInitialOption(_aloneSettings.vSync);
    }
    
    private void CloseVideoSettingsMenu()
    {
        if (!focused) return;
        
        _menuInputController.onCancelPressed.UnSubscribe(CloseVideoSettingsMenu);
        
        videoSettingsMenu.SetActive(false);

        _aloneSettings.resolutionIndex = resolution.CurrentOption;
        _aloneSettings.presentationMode = presentationMode.CurrentOption;
        _aloneSettings.vSync = Convert.ToBoolean(vsync.CurrentOption);

        //Screen.fullScreen = _aloneSettings.presentationMode;
        Resolution newResolution = AloneSettings.GetResolutions()[_aloneSettings.resolutionIndex];
        if (newResolution.width != Screen.currentResolution.width &&
            newResolution.height != Screen.currentResolution.height)
        {
            //Screen.SetResolution(newResolution.width, newResolution.height, _aloneSettings.presentationMode);
        }
        QualitySettings.vSyncCount = _aloneSettings.vSync ? 1 : 0;
        
        onClose.Dispatch();
    }
}
