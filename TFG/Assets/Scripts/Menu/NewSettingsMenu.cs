using System;
using System.Collections;
using System.Collections.Generic;
using Alone.Menu;
using Alone.SaveAndLoad;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NewSettingsMenu : Menu
{
    private SerializationManager _serializationManager;
    private MenuInputHelper _menuInputHelper;
    
    [SerializeField] private GameObject leftTabInputImage;
    [Space]
    [SerializeField] private Button displaySettingsButton;
    [SerializeField] private GameObject displayAudioSeparator;
    [SerializeField] private Button audioSettingsButton;
    [SerializeField] private Button controlsSettingsButton;
    [Space]
    [SerializeField] private GameObject rightTabInputImage;
    [Space] 
    [SerializeField] private Button[] backButtons;
    [Space] 
    [SerializeField] private GameObject displaySettings;
    [SerializeField] private GameObject defaultSelectableDisplaySettings;
    [SerializeField] private GameObject audioSettings;
    [SerializeField] private GameObject defaultSelectableAudioSettings;
    [SerializeField] private GameObject controlsSettings;
    [SerializeField] private GameObject keyboardSettings;
    [SerializeField] private GameObject defaultSelectableKeyboardSettings;
    [SerializeField] private GameObject defaultSelectableGamepadSettings;
    [Space(30)]
    
    private AloneSettings _aloneSettings;
    
    [Header("DISPLAY SETTINGS")]
    [SerializeField] private OptionSelector displayModeSelector;
    [SerializeField] private TMP_Dropdown displayResolution;
    [SerializeField] private Toggle displayVSync;
    [SerializeField] private ExtendedButton resetDefaultDisplaySettingsButton;

    [Header("DISPLAY SETTINGS")]
    [SerializeField] private Slider masterVolume;
    [SerializeField] private Slider musicVolume;
    [SerializeField] private Slider effectsVolume;
    [SerializeField] private ExtendedButton resetDefaultAudioSettingsButton;
    
    [Header("CONTROLS SETTINGS")]
    [SerializeField] private Slider controlsMouseSensitivity;
    [SerializeField] private Toggle controlsInvertVerticalMouse;
    [Space]
    [SerializeField] private Slider controlsGamepadSensitivity;
    [SerializeField] private OptionSelector controlsStickMoveLook;
    [SerializeField] private Toggle controlsInvertVerticalStick;
    [SerializeField] private Slider controlsVibrationStrength;
    [SerializeField] private ExtendedButton resetDefaultControlsSettingsButton;
    
    private int _currentTab;

    public Signal onSettingsMenuClose;
    
    protected override void Awake()
    {
        base.Awake();

        _menuInputHelper = FindObjectOfType<MenuInputHelper>();
        _menuInputHelper.ToggleResetToDefaultsImages(false);
        
        CloseMenu();
    }

    private void Start()
    {
        _aloneSettings = _aloneSettings = GameManager.Instance.settings;
        InitializeButtons();

        _serializationManager = GameManager.Instance.serializationManager;
        
        InitializeDisplaySettings();
        InitializeAudioSettings();
        InitializeControlsSettings();
    }

    private void InitializeButtons()
    {
        displaySettingsButton.onClick.AddListener(OpenDisplaySettings);
        audioSettingsButton.onClick.AddListener(OpenAudioSettings);
        controlsSettingsButton.onClick.AddListener(OpenControlsSettings);

        foreach (var backButton in backButtons)
        {
            backButton.onClick.AddListener(CloseSettingsMenu);
        }
        
        resetDefaultDisplaySettingsButton.onClick.AddListener(ResetDisplaySettings);
        resetDefaultAudioSettingsButton.onClick.AddListener(ResetAudioSettings);
        resetDefaultControlsSettingsButton.onClick.AddListener(ResetControlSettings);
        
        displayModeSelector.AddCallback(ApplyAndSaveDisplaySettings);
        displayResolution.onValueChanged.AddListener(ApplyAndSaveResolution);
        displayVSync.onValueChanged.AddListener(ApplyAndSaveVSync);
        resetDefaultDisplaySettingsButton.onClick.AddListener(ApplyAndSaveDisplaySettings);
        
        masterVolume.onValueChanged.AddListener(ApplyMasterVolume);
        musicVolume.onValueChanged.AddListener(ApplyMusicVolume);
        effectsVolume.onValueChanged.AddListener(ApplyEffectsVolume);
    }
    
    public void OpenSettingsMenu()
    {
        _menuInputHelper.ToggleResetToDefaultsImages(true);
        
        OpenMenu();

        _currentTab = -1;
        
        if(!GameManager.Instance.isOnSwitch) OpenDisplaySettings();
        else
        {
            OpenAudioSettings();
            displaySettingsButton.gameObject.SetActive(false);
            displayAudioSeparator.SetActive(false);
        }
        
        SetTabs(!GameManager.Instance.isOnSwitch, true);
        
        InputController.SubscribeToInput(NextTab, "UINextTab");
        InputController.SubscribeToInput(PreviousTab, "UIPreviousTab");
        _menuInputController.onCancelPressed.Subscribe(CloseSettingsMenu);
        InputController.SubscribeToInput(ResetSettings, "UIResetDefault");
    }

    private void CloseSettingsMenu()
    {
        _menuInputHelper.ToggleResetToDefaultsImages(false);
        
        CloseMenu();
        
        InputController.UnSubscribeToInput(NextTab, "UINextTab");
        InputController.UnSubscribeToInput(PreviousTab, "UIPreviousTab");
        _menuInputController.onCancelPressed.UnSubscribe(CloseSettingsMenu);
        InputController.UnSubscribeToInput(ResetSettings, "UIResetDefault");

        ApplyAndSaveSettings();
        
        onSettingsMenuClose.Dispatch();
    }
    
    private void OpenDisplaySettings()
    {
        if (_currentTab == 0) return;
        
        _currentTab = 0;
        SetTabs(false, true);
        _menuInputController.Initialize(defaultSelectableDisplaySettings);
        _menuInputController.SelectGameObject(defaultSelectableDisplaySettings);
        ToggleSettings(displaySettings, displaySettingsButton, true);
        ToggleSettings(audioSettings, audioSettingsButton, false);
        ToggleSettings(controlsSettings, controlsSettingsButton, false);
        displaySettingsButton.targetGraphic.color = displaySettingsButton.colors.selectedColor;
        
        InitializeDisplaySettings();
        ApplyAndSaveSettings();
    }

    private void OpenAudioSettings()
    {
        if (_currentTab == 1) return;
        
        _currentTab = 1;
        SetTabs(!GameManager.Instance.isOnSwitch, true);
        ToggleSettings(displaySettings, displaySettingsButton, false);
        ToggleSettings(audioSettings, audioSettingsButton, true);
        ToggleSettings(controlsSettings, controlsSettingsButton, false);

        audioSettingsButton.targetGraphic.color = audioSettingsButton.colors.selectedColor;
        
        _menuInputController.Initialize(defaultSelectableAudioSettings);
        
        InitializeAudioSettings();
        ApplyAndSaveSettings();
    }

    private void OpenControlsSettings()
    {
        if (_currentTab == 2) return;
        
        _currentTab = 2;
        SetTabs(true, false);
        ToggleSettings(displaySettings, displaySettingsButton, false);
        ToggleSettings(audioSettings, audioSettingsButton, false);
        ToggleSettings(controlsSettings, controlsSettingsButton, true);

        controlsSettingsButton.targetGraphic.color = controlsSettingsButton.colors.selectedColor;

        if (GameManager.Instance.isOnSwitch)
        {
            ToggleSettings(keyboardSettings, null, false);
            _menuInputController.Initialize(defaultSelectableGamepadSettings);
            return;
        }
        
        ToggleSettings(keyboardSettings, null, true);
        _menuInputController.Initialize(defaultSelectableKeyboardSettings);
        
        InitializeControlsSettings();
        ApplyAndSaveSettings();
    }

    private void SetTabs(bool leftTabActive, bool rightTabActive)
    {
        leftTabInputImage.SetActive(leftTabActive);
        rightTabInputImage.SetActive(rightTabActive);
    }

    private void NextTab(Rewired.InputActionEventData data)
    {
        switch (_currentTab)
        {
            case 0:
                OpenAudioSettings();
                break;
            case 1:
                OpenControlsSettings();
                break;
            case 2:
                return;
        }
    }

    private void PreviousTab(Rewired.InputActionEventData data)
    {
        switch (_currentTab)
        {
            case 0:
                return;
            case 1:
                if (!GameManager.Instance.isOnSwitch) OpenDisplaySettings();
                break;
            case 2:
                OpenAudioSettings();
                break;
        }
    }

    private static void ToggleSettings(GameObject settings, Button settingsTabButton, bool active)
    {
        settings.SetActive(active);

        if (settingsTabButton)
        {
            settingsTabButton.targetGraphic.color =
                active ? settingsTabButton.colors.selectedColor : settingsTabButton.colors.normalColor;   
        }
    }

    private void InitializeDisplaySettings()
    {
        displayModeSelector.SelectOption(_aloneSettings.presentationMode);
        displayResolution.options = new List<TMP_Dropdown.OptionData>();
        displayResolution.AddOptions(AloneSettings.GetResolutionsFormatted(out int currentResolutionIndex));
        displayResolution.value = _aloneSettings.resolutionIndex == -1
            ? currentResolutionIndex
            : _aloneSettings.resolutionIndex;
        displayVSync.isOn = _aloneSettings.vSync;
    }

    private void ResetDisplaySettings()
    {
        _aloneSettings.ResetVideoSettings();
        InitializeDisplaySettings();
    }

    private void InitializeAudioSettings()
    {
        masterVolume.value = _aloneSettings.masterVolume;
        masterVolume.onValueChanged.Invoke(masterVolume.value);
        musicVolume.value = _aloneSettings.musicVolume;
        musicVolume.onValueChanged.Invoke(musicVolume.value);
        effectsVolume.value = _aloneSettings.effectsVolume;
        effectsVolume.onValueChanged.Invoke(effectsVolume.value);
    }

    private void ResetAudioSettings()
    {
        _aloneSettings.ResetAudioSettings();
        InitializeAudioSettings();
    }

    private void InitializeControlsSettings()
    {
        controlsMouseSensitivity.value = _aloneSettings.keyboardRotationSpeed;
        controlsMouseSensitivity.onValueChanged.Invoke(controlsMouseSensitivity.value);
        controlsInvertVerticalMouse.isOn = _aloneSettings.invertMouse;

        controlsGamepadSensitivity.value = _aloneSettings.gamepadRotationSpeed;
        controlsGamepadSensitivity.onValueChanged.Invoke(controlsGamepadSensitivity.value);
        controlsStickMoveLook.SelectOption(Convert.ToInt32(_aloneSettings.useRightHandedStickMoveLook));
        controlsInvertVerticalStick.isOn = _aloneSettings.invertLook;
        controlsVibrationStrength.value = _aloneSettings.vibrationStrength;
        controlsVibrationStrength.onValueChanged.Invoke(controlsVibrationStrength.value);
    }

    private void ResetControlSettings()
    {
        _aloneSettings.ResetGamepadSettings();
        _aloneSettings.ResetKeyboardAndMouseSettings();
        InitializeControlsSettings();
    }

    private void ResetSettings(Rewired.InputActionEventData data)
    {
        switch (_currentTab)
        {
            case 0:
                ResetDisplaySettings();
                break;
            case 1:
                ResetAudioSettings();
                break;
            case 2:
                ResetControlSettings();
                break;
        }
    }
    
    private void ApplyAndSaveSettings()
    {
        ApplyAndSaveDisplaySettings();

        _aloneSettings.keyboardRotationSpeed = controlsMouseSensitivity.value;
        _aloneSettings.invertMouse = controlsInvertVerticalMouse.isOn;

        _aloneSettings.masterVolume = Mathf.Clamp(masterVolume.value, 0.0001f, 100);
        _aloneSettings.musicVolume = Mathf.Clamp(musicVolume.value, 0.0001f, 100);
        _aloneSettings.effectsVolume = Mathf.Clamp(effectsVolume.value, 0.0001f, 100);
        
        _aloneSettings.gamepadRotationSpeed = controlsGamepadSensitivity.value;
        _aloneSettings.useRightHandedStickMoveLook = Convert.ToBoolean(controlsStickMoveLook.OptionIndex);
        _aloneSettings.invertLook = controlsInvertVerticalStick.isOn;
        _aloneSettings.vibrationStrength = (int)controlsVibrationStrength.value;

        OrbitCamera orbitCamera = FindObjectOfType<OrbitCamera>();
        if (orbitCamera)
        {
            orbitCamera.KeyboardRotationSpeed = _aloneSettings.keyboardRotationSpeed;
            orbitCamera.InvertedYAxisMouse = _aloneSettings.invertMouse;
            orbitCamera.UseMouseAcceleration = _aloneSettings.mouseAcceleration;
            
            orbitCamera.UseRightHandedScheme = _aloneSettings.useRightHandedStickMoveLook;
            orbitCamera.GamepadRotationSpeed = _aloneSettings.gamepadRotationSpeed;
            orbitCamera.InvertedYAxisGamepad = _aloneSettings.invertLook;
        }

        MovingSphere movingSphere = FindObjectOfType<MovingSphere>();
        if (movingSphere)
        {
            movingSphere.UseRightHandedScheme = _aloneSettings.useRightHandedStickMoveLook;
        }
        
        _serializationManager.SaveSettings(_aloneSettings);
    }

    private void ApplyAndSaveDisplaySettings()
    {
        StartCoroutine(ApplyDisplaySettingsTimeDown());
    }

    private void ApplyAndSaveResolution(int resolution)
    {
        ApplyAndSaveDisplaySettings();
    }

    private void ApplyAndSaveVSync(bool active)
    {
        ApplyAndSaveDisplaySettings();
    }

    private IEnumerator ApplyDisplaySettingsTimeDown()
    {
        yield return new WaitForSecondsRealtime(0.1f);
        
        _aloneSettings.resolutionIndex = displayResolution.value;
        _aloneSettings.presentationMode = displayModeSelector.OptionIndex;
        _aloneSettings.vSync = displayVSync.isOn;
        
        Screen.fullScreen = _aloneSettings.presentationMode == 0;
        Resolution newResolution = AloneSettings.GetResolutions()[_aloneSettings.resolutionIndex];
        Screen.SetResolution(newResolution.width, newResolution.height, _aloneSettings.presentationMode == 0);
        
        QualitySettings.vSyncCount = _aloneSettings.vSync ? 1 : 0;
    }

    private void ApplyMasterVolume(float value)
    {
        GameManager.Instance.ApplyMixerVolumeValue(value, "Master");
    }
    
    private void ApplyMusicVolume(float value)
    {
        GameManager.Instance.ApplyMixerVolumeValue(value, "Music");
    }
    
    private void ApplyEffectsVolume(float value)
    {
        GameManager.Instance.ApplyMixerVolumeValue(value, "Effects");
    }
}
