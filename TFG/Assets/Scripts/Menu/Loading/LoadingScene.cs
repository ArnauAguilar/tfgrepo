
using System;
using System.Collections;
using System.Security.Cryptography;
using Alone.Menu;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScene : MonoBehaviour
{
    [SerializeField] private Slider progressionSlider;
    [SerializeField] private TextMeshProUGUI loadingText;
    
    private void Start()
    {
        progressionSlider.maxValue = 1;

        StartCoroutine(AnimateLoading());
    }

    private void Update()
    {        
        progressionSlider.value = SceneLoader.GetLoadingProgress();

        if (progressionSlider.value >= progressionSlider.maxValue)
        {
            OnLoadFinished();
        }
    }

    private void OnLoadFinished()
    {
        StopAllCoroutines();
        SceneReference loadingScene = SceneLoader.GetCurrentActiveScene();
        SceneLoader.SetActiveScene(SceneLoader.NextSceneToLoad);
        SceneLoader.UnloadSceneForced(loadingScene);
    }

    private IEnumerator AnimateLoading()
    {
        while (true)
        {
            loadingText.text = "Loading";
            yield return new WaitForSecondsRealtime(0.3f);
            loadingText.text += ".";
            yield return new WaitForSecondsRealtime(0.3f);
            loadingText.text += ".";
            yield return new WaitForSecondsRealtime(0.3f);
            loadingText.text += ".";
            yield return new WaitForSecondsRealtime(0.3f);
        }
    }
}
