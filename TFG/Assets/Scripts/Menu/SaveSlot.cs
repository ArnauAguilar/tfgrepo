﻿using System.Collections;
using Alone.SaveAndLoad;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

namespace Alone.Menu
{
    public class SaveSlot : MonoBehaviour, ISelectHandler, IDeselectHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public string SaveSlotName => gameObject.name;

        [Header("Slot UI Settings")] 
        [SerializeField] private Sprite emptySlotSprite;
        [SerializeField] private Image slotProgressImage;

        [SerializeField] private Button slotButton;
        public Button SlotButton => slotButton;

        [Space] 
        
        [SerializeField] private TextMeshProUGUI saveSlotActionText;
        [SerializeField] private TextMeshProUGUI completionPercentageText;
        [SerializeField] private TextMeshProUGUI progressNameText;

        [SerializeField] private GameObject slotHasSaveFile;
        [SerializeField] private GameObject slotIsEmpty;

        [Space] 
        
        private bool _currentlySelected;
        [SerializeField] private Image deleteSlotProgressImage;
        [SerializeField] private float timeToDeleteSlot = 2;
        public Signal<SaveSlot> onSaveSlotDeleted;

        // Save Slot File Path
        private string _filePath;
        public string FilePath => _filePath;

        // Signals for selection, deselection and pressed
        public Signal<SaveSlot> onSaveSlotPressed;

        private void Awake()
        {
            slotButton.onClick.AddListener(LoadSaveState);

            SetEmptySlot();
        }

        public void InitializeSaveSlot(string filePath, string progressName, float progressPercentage, Sprite progressSprite)
        {
            _filePath = filePath;

            if (string.IsNullOrEmpty(_filePath))
            {
                slotProgressImage.sprite = emptySlotSprite;
                slotProgressImage.rectTransform.offsetMax = new Vector2(0, 0);
                

                saveSlotActionText.text = "New Game";
                
                slotHasSaveFile.SetActive(false);
                slotIsEmpty.SetActive(true);
            }
            else
            {
                slotProgressImage.sprite = progressSprite;
                slotProgressImage.rectTransform.offsetMax = new Vector2(0, -60);

                saveSlotActionText.text = "Load";
                
                slotHasSaveFile.SetActive(true);
                completionPercentageText.text = progressPercentage + " %";
                progressNameText.text = progressName;
                
                slotIsEmpty.SetActive(false);
            }
        }

        private void SetEmptySlot()
        {
            slotProgressImage.sprite = emptySlotSprite;
            
            slotHasSaveFile.SetActive(false);
        }
        
        
        private void LoadSaveState()
        {
            onSaveSlotPressed.Dispatch(this);
        }

        public void OnSelect(BaseEventData eventData)
        {
            _currentlySelected = true;
        }
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            _currentlySelected = true;
        }

        public void OnDeselect(BaseEventData eventData)
        {
            _currentlySelected = false;
            
            ForceStopDeletingSaveSlot();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _currentlySelected = false;

            ForceStopDeletingSaveSlot();
        }

        public void StartDeletingSaveSlot()
        {
            if (!_currentlySelected || string.IsNullOrEmpty(_filePath)) return;

            StartCoroutine(DeleteSlotAnimation());
        }

        public void StopDeletingSaveSlot()
        {
            if (!_currentlySelected || string.IsNullOrEmpty(_filePath)) return;
            
            ForceStopDeletingSaveSlot();
        }

        private void ForceStopDeletingSaveSlot()
        {
            StopAllCoroutines();
            deleteSlotProgressImage.fillAmount = 0;
        }

        private IEnumerator DeleteSlotAnimation()
        {
            float _counter = 0;
            
            deleteSlotProgressImage.fillAmount = 0;
            
            while(deleteSlotProgressImage.fillAmount < 1)
            {
                deleteSlotProgressImage.fillAmount = Mathf.Lerp(0, 1, _counter / timeToDeleteSlot);
                _counter += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            
            DeleteSlot();
            deleteSlotProgressImage.fillAmount = 0;
        }

        private void DeleteSlot()
        {
            onSaveSlotDeleted.Dispatch(this);
            InitializeSaveSlot(null, null, 0, null);
        }
    }
}
