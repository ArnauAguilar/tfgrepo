
using UnityEngine;
using UnityEngine.UI;

public class MenuInputHelper : MonoBehaviour
{
    private MenuInputController _menuInputController;

    [Space]
    
    [SerializeField] private Image[] navigateImages;
    [SerializeField] private Image[] navigateSecondaryImages;

    [Space]
    
    [SerializeField] private Image[] selectImages;
    [SerializeField] private Image[] selectSecondaryImages;

    [Space]
    
    [SerializeField] private Image[] cancelBackImages;

    [Space]
    
    [SerializeField] private Image[] deleteImages;
    
    [Space]
    
    [SerializeField] private Image[] previousTabImages;
    [SerializeField] private Image[] nextTabImages;

    [Space] 
    
    [SerializeField] private Image[] resetToDefaults;

    private void Awake()
    {
        _menuInputController = FindObjectOfType<MenuInputController>();
        
        _menuInputController.onInputActionsChanged.Subscribe(OnChangeInputActions);
        
        ToggleDeleteImages(false);
    }

    private void OnChangeInputActions(InputActionForPlatform currentPlatform)
    {
        foreach (var navigateImage in navigateImages) navigateImage.sprite = currentPlatform.navigate;
        if (currentPlatform.navigateSecondary)
        {
            foreach (var navigateSecondaryImage in navigateSecondaryImages)
            {
                navigateSecondaryImage.gameObject.SetActive(true);
                navigateSecondaryImage.sprite = currentPlatform.navigateSecondary;
            }
        }
        else foreach (var navigateSecondaryImage in navigateSecondaryImages) navigateSecondaryImage.gameObject.SetActive(false);
        
        foreach (var selectImage in selectImages) selectImage.sprite = currentPlatform.select;
        if (currentPlatform.selectSecondary)
        {
            foreach (var selectSecondaryImage in selectSecondaryImages)
            {
                selectSecondaryImage.gameObject.SetActive(true);
                selectSecondaryImage.sprite = currentPlatform.selectSecondary;
            }
        }
        else foreach (var selectSecondaryImage in selectSecondaryImages) selectSecondaryImage.gameObject.SetActive(false);
        
        foreach (var cancelBackImage in cancelBackImages) cancelBackImage.sprite = currentPlatform.cancelBack;
        
        foreach (var deleteImage in deleteImages) deleteImage.sprite = currentPlatform.delete;
        
        foreach (var previousTabImage in previousTabImages) previousTabImage.sprite = currentPlatform.previousTab;
        foreach (var nextTabImage in nextTabImages) nextTabImage.sprite = currentPlatform.nextTab;
        
        foreach (var resetImage in resetToDefaults) resetImage.sprite = currentPlatform.resetToDefault;
    }

    private void OnDestroy()
    {
        _menuInputController.onInputActionsChanged.UnSubscribe(OnChangeInputActions);
    }

    public void ToggleCancelBackImages(bool active)
    {
        foreach (var cancelBackImage in cancelBackImages) cancelBackImage.transform.parent.gameObject.SetActive(active);
    }
    
    public void ToggleDeleteImages(bool active)
    {
        foreach (var deleteImage in deleteImages) deleteImage.transform.parent.gameObject.SetActive(active);
    }

    public void ToggleResetToDefaultsImages(bool active)
    {
        foreach (var resetToDefault in resetToDefaults) resetToDefault.transform.parent.gameObject.SetActive(active);
    }
}
