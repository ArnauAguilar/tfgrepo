using Alone;
using UnityEngine;
using UnityEngine.EventSystems;

public class SaveSlotButton : MonoBehaviour, ISelectHandler, IDeselectHandler
{
    public Signal onButtonSelected;
    public Signal onButtonDeselected;

    public void OnSelect(BaseEventData eventData)
    {
        onButtonSelected.Dispatch();
    }

    public void OnDeselect(BaseEventData eventData)
    {
        onButtonDeselected.Dispatch();
    }
}
