
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ConfirmationMessageManager : MonoBehaviour
{
    private MenuInputController _menuInputController;
    
    [SerializeField] private GameObject confirmationMessagePanel;
    [SerializeField] private Button confirmationButton;
    [SerializeField] private Button backgroundCloseButton;
    [SerializeField] private Button xCloseButton;

    public Signal onConfirm;
    public Signal onCancel;

    private void Awake()
    {
        _menuInputController = FindObjectOfType<MenuInputController>();
        
        CloseExitConfirmationMessage();
        
        confirmationButton.onClick.AddListener(ConfirmExit);
        
        backgroundCloseButton.onClick.AddListener(CloseExitConfirmationMessage);
        xCloseButton.onClick.AddListener(CloseExitConfirmationMessage);
    }

    public void OpenExitConfirmationMessage()
    {
        confirmationMessagePanel.SetActive(true);
        
        _menuInputController.onCancelPressed.Subscribe(CloseExitConfirmationMessage);
        _menuInputController.Initialize(confirmationButton.gameObject);
        
        _menuInputController.onKeyboardSelected.Subscribe(OnKeyBoardSelected);
        _menuInputController.onGamepadSelected.Subscribe(OnGamepadSelected);

        if (_menuInputController.CurrentDeviceType == DeviceType.Keyboard) OnKeyBoardSelected();
        else OnGamepadSelected();
    }

    private void ConfirmExit()
    {
        _menuInputController.onCancelPressed.UnSubscribe(CloseExitConfirmationMessage);
        
        onConfirm.Dispatch();
    }
    
    private void CloseExitConfirmationMessage()
    {
        confirmationMessagePanel.SetActive(false);
        onCancel.Dispatch();
        
        _menuInputController.onCancelPressed.UnSubscribe(CloseExitConfirmationMessage);
        _menuInputController.onKeyboardSelected.UnSubscribe(OnKeyBoardSelected);
        _menuInputController.onGamepadSelected.UnSubscribe(OnGamepadSelected);
    }

    private void OnKeyBoardSelected()
    {
        xCloseButton.gameObject.SetActive(true);
    }

    private void OnGamepadSelected()
    {
        xCloseButton.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        _menuInputController.onKeyboardSelected.UnSubscribe(OnKeyBoardSelected);
        _menuInputController.onGamepadSelected.UnSubscribe(OnGamepadSelected);
    }
}
