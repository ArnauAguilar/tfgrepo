using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;

public class SwitchSaveSystem 
{
    #if UNITY_SWITCH
    private enum SaveState
    {
        Waiting,
        Required,
        Saved,
    }
    private nn.account.Uid userId;
    private const string mountName = "MySave";
    private const string fileName = "MySaveData";
    private static readonly string filePath = string.Format("{0}:/{1}", mountName, fileName);
    private nn.fs.FileHandle fileHandle = new nn.fs.FileHandle();
    private const int largeSaveDataSize = 32 * 1024 * 1024;
    private byte[] largeSaveData = new byte[largeSaveDataSize];
    private Thread thread = null;
    private bool isRunning = false;
    private SaveState saveState = SaveState.Waiting;

    public void Initialize()
    {
        nn.account.Account.Initialize();
        nn.account.UserHandle userHandle = new nn.account.UserHandle();

        if (!nn.account.Account.TryOpenPreselectedUser(ref userHandle))
        {
            nn.Nn.Abort("Failed to open preselected user.");
        }
        nn.Result result = nn.account.Account.GetUserId(ref userId, userHandle);
        result.abortUnlessSuccess();
        result = nn.fs.SaveData.Mount(mountName, userId);
        result.abortUnlessSuccess();

        //Load();

        StartThread();
    }

    private void StartThread()
    {
        Debug.Assert(thread == null);
        thread = new Thread(ThreadLoop);
        thread.Priority = System.Threading.ThreadPriority.Lowest;
        isRunning = true;
        thread.Start();
    }

    private void ThreadLoop()
    {
        while (isRunning)
        {
            if (saveState == SaveState.Required)
            {
                SaveCurrentData();
                saveState = SaveState.Saved;
            }
            else
            {
                Thread.Sleep(1);
            }
        }
    }

    public void Save(int progress)
    {
        using (BinaryWriter writer = new BinaryWriter(new MemoryStream(sizeof(int))))
        {
            writer.Write(progress);

            writer.BaseStream.Close();
            (writer.BaseStream as MemoryStream).GetBuffer().CopyTo(largeSaveData, 0);
        }

#if UNITY_SWITCH
        // Nintendo Switch Guideline 0080
        UnityEngine.Switch.Notification.EnterExitRequestHandlingSection();
#endif
        saveState = SaveState.Required;
    }

    public int Load()
    {
        nn.fs.EntryType entryType = 0;
        nn.Result result = nn.fs.FileSystem.GetEntryType(ref entryType, filePath);
        if (nn.fs.FileSystem.ResultPathNotFound.Includes(result))
        {
            return -1;
        }
        result.abortUnlessSuccess();

        result = nn.fs.File.Open(ref fileHandle, filePath, nn.fs.OpenFileMode.Read);
        result.abortUnlessSuccess();

        long fileSize = 0;
        result = nn.fs.File.GetSize(ref fileSize, fileHandle);
        result.abortUnlessSuccess();
        Debug.Assert(fileSize == largeSaveData.LongLength);

        result = nn.fs.File.Read(fileHandle, 0, largeSaveData, largeSaveData.LongLength);
        result.abortUnlessSuccess();

        nn.fs.File.Close(fileHandle);

        using (BinaryReader reader = new BinaryReader(new MemoryStream(largeSaveData)))
        {
            return reader.ReadInt32();
        }
    }

    private void SaveCurrentData()
    {
        nn.Result result = nn.fs.File.Open(ref fileHandle, filePath, nn.fs.OpenFileMode.Write);
        result.abortUnlessSuccess();

        const int offset = 0;
        result = nn.fs.File.Write(fileHandle, offset, largeSaveData, largeSaveData.LongLength, nn.fs.WriteOption.Flush);
        result.abortUnlessSuccess();

        nn.fs.File.Close(fileHandle);
        result = nn.fs.FileSystem.Commit(mountName);
        result.abortUnlessSuccess();
    }


    public void InitializeSaveData()
    {
        nn.fs.EntryType entryType = 0;
        nn.Result result = nn.fs.FileSystem.GetEntryType(ref entryType, filePath);
        if (result.IsSuccess())
        {
            return;
        }
        if (!nn.fs.FileSystem.ResultPathNotFound.Includes(result))
        {
            result.abortUnlessSuccess();
        }

        using (BinaryWriter writer = new BinaryWriter(new MemoryStream(sizeof(int))))
        {
            //writer.Write(0);//slotIndex
            writer.Write(0);//progress

            writer.BaseStream.Close();
            (writer.BaseStream as MemoryStream).GetBuffer().CopyTo(largeSaveData, 0);
        }

#if UNITY_SWITCH
        // Nintendo Switch Guideline 0080
        UnityEngine.Switch.Notification.EnterExitRequestHandlingSection();
#endif

        result = nn.fs.File.Create(filePath, largeSaveDataSize);
        result.abortUnlessSuccess();

        result = nn.fs.File.Open(ref fileHandle, filePath, nn.fs.OpenFileMode.Write);
        result.abortUnlessSuccess();

        const int offset = 0;
        result = nn.fs.File.Write(fileHandle, offset, largeSaveData, largeSaveData.LongLength, nn.fs.WriteOption.Flush);
        result.abortUnlessSuccess();

        nn.fs.File.Close(fileHandle);
        result = nn.fs.FileSystem.Commit(mountName);
        result.abortUnlessSuccess();

#if UNITY_SWITCH
        // Nintendo Switch Guideline 0080
        UnityEngine.Switch.Notification.LeaveExitRequestHandlingSection();
#endif
    }

    public void Update()
    {

        if(saveState == SaveState.Saved)
        {
            ///"Saved!"
            ///
            Debug.Log("Saved Data");

            saveState = SaveState.Waiting;
#if UNITY_SWITCH
            // Nintendo Switch Guideline 0080
            UnityEngine.Switch.Notification.LeaveExitRequestHandlingSection();
#endif
        }
    }
#endif
}
