using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class VentsAnimation : MonoBehaviour
{
    [SerializeField] private Transform ventsDoor;

    private Vector3 _initialDoorEulerAngles;
    
    [Title("Animation Settings")] 
    [SerializeField] private float animationDuration;
    public float AnimationDuration => animationDuration;
    [SerializeField] private float desiredOpenDegrees;

    private void Start()
    {
        _initialDoorEulerAngles = ventsDoor.transform.localEulerAngles;
    }

    [Button]
    public void OpenVentilationDoor()
    {
        StartCoroutine(AnimateVentilationDoor(0, desiredOpenDegrees));
    }

    [Button]
    public void CloseVentilationDoor()
    {
        StartCoroutine(AnimateVentilationDoor(desiredOpenDegrees, 0));
    }
    
    private IEnumerator AnimateVentilationDoor(float initialRotation, float finalRotation)
    {
        Vector3 initialEulerAngles = new Vector3(initialRotation, _initialDoorEulerAngles.y, _initialDoorEulerAngles.z);
        Vector3 finalEulerAngles = new Vector3(finalRotation, _initialDoorEulerAngles.y, _initialDoorEulerAngles.z);

        ventsDoor.transform.localEulerAngles = initialEulerAngles;
        
        float currentTime = 0f;
        while (currentTime < animationDuration)
        {
            ventsDoor.transform.localEulerAngles =
                Vector3.Lerp(initialEulerAngles, finalEulerAngles, currentTime / animationDuration);
            currentTime += Time.unscaledDeltaTime;
            yield return null;
        }
        
        ventsDoor.transform.localEulerAngles = finalEulerAngles;
    }
}
