
using System.Collections;
using Alone.Menu;
using Sirenix.OdinInspector;
using UnityEngine;

public class Vents : MonoBehaviour
{
    private InteractCanvasManager _interactCanvasManager;
    private OrbitCamera _orbitCamera;

    private bool _canInteract;
    [SerializeField] private PlayerTrigger trigger;

    [SerializeField] private Transform originTransform;
    [SerializeField] private Transform destinationTransform;
    [SerializeField] private GameObject ventCollider;

    [Title("Animation Settings")] 
    [SerializeField] private bool playAnimation;

    [Header("Camera Animation")]
    [SerializeField] private float timeToCompleteCameraAnimation;
    [SerializeField] private Vector2 cameraOrbitAnglesOrigin;
    [SerializeField] private Vector2 cameraOrbitAnglesDestination;
    [SerializeField] private GravitySource desiredGravitySource;

    [Header("Vents animation")]
    [SerializeField] private VentsAnimation originVentAnimation;
    [SerializeField] private Transform originVentInsideTransform;
    [SerializeField] private float minimumDistanceToPlay = 0.1f;
    
    [Header("Screen animation")]
    [SerializeField] private Fader fader;

    [Header("Sound")] 
    [SerializeField] private AudioSource openAudioSource;
    [SerializeField] private AudioSource travelAudioSource;

    private void Start()
    {
        if (!trigger)
        {
            trigger = transform.GetComponentInChildren<PlayerTrigger>();
        }
        
        trigger.onPlayerEnter.Subscribe(OnVentsTriggerEnter);
        trigger.onPlayerExit.Subscribe(OnVentsTriggerExit);

        InputController.SubscribeToInput(OnInteract, "Interact");
    }

    private void Init()
    {
        _interactCanvasManager = FindObjectOfType<InteractCanvasManager>();
        _orbitCamera = FindObjectOfType<OrbitCamera>();
        fader = FindObjectOfType<Fader>();
    }

    private void OnDestroy()
    {
        InputController.UnSubscribeToInput(OnInteract, "Interact");
    }

    private void OnVentsTriggerEnter()
    {
        if (!_interactCanvasManager)
        {
            Init();
        }
        _canInteract = true;
        CheckForInteractCanvasManager();
        _interactCanvasManager.ShowInteractMessage();
    }

    private void OnVentsTriggerExit()
    {
        _canInteract = false;
        CheckForInteractCanvasManager();
        _interactCanvasManager.HideInteractMessage();
    }

    private void CheckForInteractCanvasManager()
    {
        if (!_interactCanvasManager) _interactCanvasManager = FindObjectOfType<InteractCanvasManager>();
    }
    
    private void OnInteract(Rewired.InputActionEventData data)
    {
        if (!_canInteract) return;

        if (playAnimation)
        {
            StartCoroutine(PlayVentsAnimation());
        }
        else
        {
            trigger.Player.transform.position = destinationTransform.position;
        }
    }

    private IEnumerator PlayVentsAnimation()
    {
        PauseMenu.CanPause = false;
        
        OnVentsTriggerExit();

        StartCoroutine(PlayVentsAnimationCamera());
        
        MovingSphere playerMovingSphere = trigger.Player.GetComponent<MovingSphere>();
        playerMovingSphere.ManualInput = false;
        
        // Move to the center in front of the origin vent
        while (Vector3.Distance(originTransform.position, playerMovingSphere.transform.position) 
               > minimumDistanceToPlay)
        {
            playerMovingSphere.DesiredVelocity =
                (originTransform.position - playerMovingSphere.transform.position).normalized *
                playerMovingSphere.animationSpeed;
            yield return null;
        }

        playerMovingSphere.DesiredVelocity = Vector3.zero;
        _orbitCamera.CameraAnimation = ScriptedAnimations.Playing;

        // Play the origin vents open anim
        ventCollider.SetActive(false);
        originVentAnimation.OpenVentilationDoor();
        openAudioSource.Play();
        yield return new WaitForSeconds(originVentAnimation.AnimationDuration);
        
        // Get inside the origin vent
        while (Vector3.Distance(originVentInsideTransform.position, playerMovingSphere.transform.position) 
               > minimumDistanceToPlay * 5)
        {
            playerMovingSphere.DesiredVelocity =
                (originVentInsideTransform.position - playerMovingSphere.transform.position).normalized *
                playerMovingSphere.animationSpeed;
            yield return null;
        }

        playerMovingSphere.DesiredVelocity = Vector3.zero;

        //Play the origin vents close anim
        originVentAnimation.CloseVentilationDoor();
        fader.FadeIn(0.5f);
        travelAudioSource.Play();
        yield return new WaitForSeconds(originVentAnimation.AnimationDuration);

        _orbitCamera.CameraAnimation = ScriptedAnimations.ForceCameraUpdate;
        
        // Tp to the other vent
        _orbitCamera.DesiredGravitySource = null;
        playerMovingSphere.transform.position = destinationTransform.position;
        _orbitCamera.DesiredOrbitAngles = cameraOrbitAnglesDestination;

        ventCollider.SetActive(true);

        yield return new WaitForSeconds(originVentAnimation.AnimationDuration);

        fader.FadeOut(1);

        yield return new WaitForSeconds(0.5f);
        
        playerMovingSphere.ManualInput = true;
        _orbitCamera.CameraAnimation = ScriptedAnimations.Stop;
        
        PauseMenu.CanPause = true;
    }

    private IEnumerator PlayVentsAnimationCamera()
    {
        _orbitCamera.DesiredGravitySource = desiredGravitySource;
        _orbitCamera.CameraAnimation = ScriptedAnimations.ForceCameraUpdate;

        Vector2 initialOrbitAngles = _orbitCamera.OrbitAngles;
        if (Mathf.Abs(initialOrbitAngles.y - cameraOrbitAnglesOrigin.y) > 180)
        {
            initialOrbitAngles.y += -360;
        }
        
        float timeElapsed = 0f;
        while (timeElapsed < timeToCompleteCameraAnimation)
        {
            _orbitCamera.DesiredOrbitAngles = Vector2.Lerp(initialOrbitAngles, cameraOrbitAnglesOrigin,
                timeElapsed / timeToCompleteCameraAnimation);
            timeElapsed += Time.unscaledDeltaTime;
            yield return null;
        }
        
        _orbitCamera.DesiredOrbitAngles = cameraOrbitAnglesOrigin;
    }
}
