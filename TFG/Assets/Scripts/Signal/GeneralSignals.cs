using System.Collections;
using System.Collections.Generic;
using Alone;
using UnityEngine;

public static class GeneralSignals
{
    public static class GravitySignals
    {
        public static Signal<GravityGenerator> onGeneratorTurnedOn;
        public static Signal<GravityGenerator> onGeneratorTurnedOff;
    }
}
