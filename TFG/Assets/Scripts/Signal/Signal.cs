﻿using System;

public struct Signal
{
    private event Action CurrentSignal;
    private event Action SingleUseSignal;

    public void Subscribe(params Action[] actions)
    {
        foreach (Action a in actions)
        {
            CurrentSignal += a;
        }
    }

    public void SubscribeOnce(params Action[] actions)
    {
        foreach (Action a in actions)
        {
            SingleUseSignal += a;
        }
    }

    public void UnSubscribe(params Action[] actions)
    {
        foreach (Action a in actions)
        {
            CurrentSignal -= a;
        }
    }

    public void UnSubscribeOnce(params Action[] actions)
    {
        foreach (Action a in actions)
        {
            SingleUseSignal -= a;
        }
    }

    public void Dispatch()
    {
        CurrentSignal?.Invoke();
        if (SingleUseSignal == null) return;

        Action SingleUse = SingleUseSignal;
        ClearOnce();
        SingleUse.Invoke();
    }

    public void Clear()
    {
        CurrentSignal = null;
    }

    public void ClearOnce()
    {
        SingleUseSignal = null;
    }

    public int GetCount()
    {
        return CurrentSignal?.GetInvocationList().Length ?? 0;
    }
}

public struct Signal<T>
{
    private event Action<T> CurrentSignal;
    private event Action<T> SingleUseSignal;

    public void Subscribe(params Action<T>[] actions)
    {
        foreach (Action<T> a in actions)
        {
            CurrentSignal += a;
        }
    }

    public void SubscribeOnce(params Action<T>[] actions)
    {
        foreach (Action<T> a in actions)
        {
            SingleUseSignal += a;
        }
    }

    public void UnSubscribe(params Action<T>[] actions)
    {
        foreach (Action<T> a in actions)
        {
            CurrentSignal -= a;
        }
    }

    public void UnSubscribeOnce(params Action<T>[] actions)
    {
        foreach (Action<T> a in actions)
        {
            SingleUseSignal -= a;
        }
    }

    public void Dispatch(T param)
    {
        CurrentSignal?.Invoke(param);
        if (SingleUseSignal == null) return;

        Action<T> SingleUse = SingleUseSignal;
        ClearOnce();
        SingleUse.Invoke(param);
    }

    public void Clear()
    {
        CurrentSignal = null;
    }

    public void ClearOnce()
    {
        SingleUseSignal = null;
    }
}  