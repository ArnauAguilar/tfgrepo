﻿using System.Collections;
using UnityEngine;

namespace Alone
{
    public class CoroutineRunner : MonoBehaviour
    {
        public static CoroutineRunner Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
        }

        public void Run(IEnumerator coroutine)
        {
            StartCoroutine(coroutine);
        }

        public void Stop(IEnumerator coroutine)
        {
            StopCoroutine(coroutine);
        }

        public void StopCoroutines()
        {
            StopAllCoroutines();
        }
    }   
}
