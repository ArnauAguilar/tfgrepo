using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.Rendering;
using LightType = UnityEngine.LightType;

public class LightExplorerHelper : EditorWindow
{
    List<Light> _lights = new List<Light>();
    List<Light> _lightsAffecting = new List<Light>();
    Vector2 _scrollPos1 = Vector2.zero;
    Vector2 _scrollPos2 = Vector2.zero;
    // Add menu item named "My Window" to the Window menu
    [MenuItem("Tools/Lighting/Light Explorer Helper")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(LightExplorerHelper));
        
    }

    private Vector3 cameraPos = Vector3.zero;
    private int shadowUnitsAtUse = 0;
    private float shadowDistance;
    
    void OnGUI()
    {
        GUILayout.Label($"Position: {cameraPos.ToString()}");
        GUILayout.Label($"Shadow units at use: {shadowUnitsAtUse.ToString()}");
        
        EditorGUILayout.Space();
        
        GUILayout.Label("List of lights affecting");
        
        shadowDistance = EditorGUILayout.FloatField("Current Shadow Distance: ",shadowDistance);

        _scrollPos2 = EditorGUILayout.BeginScrollView(_scrollPos2);
        foreach (Light light in _lightsAffecting)
        {
            if (GUILayout.Button(light.name + " " + GetShadowCost(light)))
            {
                Selection.activeGameObject = light.gameObject;
            }
        }
        EditorGUILayout.EndScrollView();
        EditorGUILayout.Space();
        
        if (GUILayout.Button("Select all affect lights"))
        {
            Selection.objects = _lightsAffecting.ToArray();
        }
        
        EditorGUILayout.Space();
        GUILayout.Label("List of all lights");
        _scrollPos1 = EditorGUILayout.BeginScrollView(_scrollPos1);
        foreach (Light light in _lights)
        {
            if (GUILayout.Button(light.name))
            {
                Selection.activeGameObject = light.gameObject;
                SceneView.lastActiveSceneView.LookAt(light.transform.position);
            }
        }
        
        EditorGUILayout.EndScrollView();
        if(GUILayout.Button("Refresh"))
        {
            GetAllLights();
            Debug.Log("Processing Lights");
        }
    }

    private void Update()
    {
        cameraPos = SceneView.lastActiveSceneView.camera.transform.position;
        shadowUnitsAtUse = 0;
        _lightsAffecting = new List<Light>();
        foreach (Light light in _lights)
        {
            if(!light.enabled || !light.gameObject.activeSelf) continue;
            
            

            if (Vector3.Distance(light.transform.position, cameraPos) <= light.range + shadowDistance * 0.5f && light.shadows != LightShadows.None && light.lightmapBakeType != LightmapBakeType.Baked)
            {
                _lightsAffecting.Add(light);
                shadowUnitsAtUse += GetShadowCost(light);

            }
        }
        
        Repaint();
    }

    int GetShadowCost(Light l)
    {
        switch (l.type)
        {
            case LightType.Spot:
                return 1;
            case LightType.Point:
                 return 6;
        }

        return 0;
    }

    void  GetAllLights()
    {
       _lights = new List<Light>( GameObject.FindObjectsOfType<Light>());
    }
    
}
