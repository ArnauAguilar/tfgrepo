using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EditorFunctions 
{

    [MenuItem("Tools/Materials/SetBaseColorToWhite")]
    static void  SetMaterialColorsToWhite()
    {
        Renderer[] renderers = (Renderer[])Object.FindObjectsOfType(typeof(Renderer));
        List<Material> mats = new List<Material>();
        foreach (Renderer renderer in renderers)
        {
            renderer.GetSharedMaterials(mats);

            foreach (Material material in mats)
            {
                material.SetColor("_BaseColor", Color.white);
                material.SetColor("_BaseColorTop", Color.white);
                material.SetColor("_BaseColorLeft", Color.white);
                material.SetColor("_BaseColorFront", Color.white);
            }
            
            mats.Clear();
        }
    }
    
    [MenuItem("Tools/Materials/SetBaseColorToGrey")]
    static void  SetMaterialColorsToGrey()
    {
        Renderer[] renderers = (Renderer[])Object.FindObjectsOfType(typeof(Renderer));
        List<Material> mats = new List<Material>();
        foreach (Renderer renderer in renderers)
        {
            renderer.GetSharedMaterials(mats);

            foreach (Material material in mats)
            {
                material.SetColor("_BaseColor", Color.grey);
                material.SetColor("_BaseColorTop", Color.grey);
                material.SetColor("_BaseColorLeft", Color.grey);
                material.SetColor("_BaseColorFront", Color.grey);
            }
            
            mats.Clear();
        }
    }
    
}
