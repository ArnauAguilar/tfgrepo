
using System;

[Serializable]
public class SaveData
{
    private static SaveData _current;
    public static SaveData Current
    {
        get => _current ??= new SaveData();
        set => _current = value;
    }
    
    public int slotIndex;
    public int progress;
}
