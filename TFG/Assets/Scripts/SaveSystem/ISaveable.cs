﻿
namespace Alone.SaveAndLoad
{
    public interface ISaveable
    {
        object CaptureState();
        void RestoreState(object state);
    }
}
