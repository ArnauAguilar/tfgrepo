
using UnityEngine;

public class ProgressSaver : MonoBehaviour
{
    private PlayerTrigger _playerTrigger;

    [SerializeField] private int progressIndex;
    public int ProgressIndex => progressIndex;
    
    [System.Serializable]
    private struct GeneratorStatus
    {
        public GuidReference generatorGUIDReference;
        public bool generatorIsActive;
    }
    [SerializeField] private GeneratorStatus[] generatorStatuses;
    
    private void Awake()
    {
        _playerTrigger = GetComponent<PlayerTrigger>();
        Destroy(GetComponent<MeshRenderer>());
        Destroy(GetComponent<MeshFilter>());
        
        _playerTrigger.onPlayerEnter.Subscribe(SaveProgress);
    }

    private void SaveProgress()
    {
        SaveData.Current.progress = progressIndex;
        GameManager.Instance.serializationManager.Save(GameManager.Instance.currentSaveSlotPath);
        
        FindObjectOfType<SavingAlone>().AnimateSaving();
        
        Destroy(gameObject);
    }

    public void LoadGeneratorStatus()
    {
        foreach (var generator in generatorStatuses)
        {
            GravityGenerator gravityGenerator =
                generator.generatorGUIDReference.gameObject.GetComponent<GravityGenerator>();
            gravityGenerator.ToggleGenerator(generator.generatorIsActive);
        }
    }
}
