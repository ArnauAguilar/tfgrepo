
using UnityEngine;

public class LevelShortCuts : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private Transform[] progressions;

    private void Update()
    {
        for (int i = 0; i < progressions.Length; i++)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1 + i))
            {
                player.transform.position = progressions[i].position;
            }
        }
    }
}
