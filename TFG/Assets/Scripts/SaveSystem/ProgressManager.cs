using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressManager : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private ProgressSaver[] progressSavers;
    
    private void Awake()
    {
        if (SaveData.Current.progress == 0) return;

        List<ProgressSaver> newProgressSavers = new List<ProgressSaver>();
        foreach (var progressSaver in progressSavers)
        {
            if (progressSaver.ProgressIndex >= SaveData.Current.progress)
            {
                newProgressSavers.Add(progressSaver);
            }
        }

        player.transform.position = newProgressSavers[0].transform.position;
        newProgressSavers[0].LoadGeneratorStatus();
        newProgressSavers.RemoveAt(0);

        foreach (var progressSaver in progressSavers)
        {
            if (progressSaver.ProgressIndex <= SaveData.Current.progress)
            {
                Destroy(progressSaver.gameObject);
            }
        }
        
        FindObjectOfType<SavingAlone>().AnimateSaving();
    }
}
