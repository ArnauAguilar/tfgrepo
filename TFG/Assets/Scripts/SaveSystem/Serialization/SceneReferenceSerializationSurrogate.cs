using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

namespace Alone.SaveAndLoad
{
    public class SceneReferenceSerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            SceneReference sceneReference = (SceneReference) obj;

            var byteArray = System.Text.Encoding.UTF8.GetBytes(sceneReference.ScenePath);
            var byteArrayToString = BitConverter.ToString(byteArray);
            info.AddValue("(", byteArrayToString);
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            SceneReference sceneReference = (SceneReference) obj;

            var byteArrayToString = (string) info.GetValue("(", typeof(string));
             
            String[] arr = byteArrayToString.Split('-');
            byte[] array = new byte[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                array[i] = Convert.ToByte(arr[i],16);
            }
            
            sceneReference.ScenePath = System.Text.Encoding.UTF8.GetString(array);
            Debug.Log(sceneReference.ScenePath);

            obj = sceneReference;
            return obj;
        }
    }
}
