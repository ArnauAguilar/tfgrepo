using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

namespace Alone.SaveAndLoad
{
    public class MultiSceneSerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            MultiScene_SO multiScene = (MultiScene_SO) obj;
            
            info.AddValue("$", multiScene.mainScene);
            info.AddValue("-", multiScene.aditiveScenes.Count);
            for(var i = 0; i < multiScene.aditiveScenes.Count; i++)
            {
                info.AddValue("!" + i, multiScene.aditiveScenes[i]);
            }
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context,
            ISurrogateSelector selector)
        {
            MultiScene_SO multiScene = (MultiScene_SO) obj;

            multiScene.mainScene = (SceneReference) info.GetValue("$", typeof(SceneReference));
            multiScene.aditiveScenes = new List<SceneReference>();
            int additiveCount = (int) info.GetValue("-", typeof(int));
            for (int i = 0; i < additiveCount; i++)
            {
                multiScene.aditiveScenes.Add(
                    (SceneReference) info.GetValue("!" + i, typeof(SceneReference)));
            }
            
            obj = multiScene;
            return obj;
        }
    }   
}
