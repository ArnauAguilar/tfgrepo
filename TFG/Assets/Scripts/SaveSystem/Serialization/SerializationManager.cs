﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Alone.SaveAndLoad
{
    public class SerializationManager : MonoBehaviour
    {
        private static string SavesFolderPath => $"{Application.persistentDataPath}/saves/";
        private static string SettingsFolderPath => $"{Application.persistentDataPath}/";
        private const string Extension = ".save";

#if UNITY_SWITCH && !UNITY_EDITOR
        private SwitchSaveSystem switchSaveSystem = new SwitchSaveSystem();

        private void Start()
        {
            switchSaveSystem.Initialize();
        }

        private void Update()
        {
            switchSaveSystem.Update();
        }
#endif

        private static string CreateFilePath(string saveName)
        {
            return CreateFilePath(SavesFolderPath, saveName, Extension);
        }

        private static string CreateFilePath(string folderPath, string fileName, string extension)
        {
            return folderPath + fileName + extension;
        }

        private static bool CheckForFile(string path)
        {
            if (Directory.Exists(SavesFolderPath)) return File.Exists(path);
            
            Directory.CreateDirectory(SavesFolderPath);
            return false;
        }

        public static void DestroyFile(string path)
        {
            if (Application.platform == RuntimePlatform.Switch)
            {
                return;
            }
            
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        [Button(ButtonSizes.Medium), GUIColor(1, 0, 0)]
        public void DestroyAllFiles(bool printLog = true)
        {
            if (Application.platform == RuntimePlatform.Switch)
            {
                return;
            }
            
            if (!Directory.Exists(SavesFolderPath)) return;
            
            foreach (var file in Directory.GetFiles(SavesFolderPath))
            {
                DestroyFile(file);
            }
            
            if(printLog) Debug.LogError("All save files destroyed");
        }

        private static void CreateEmptyFile(string saveName)
        {
            FileStream file = File.Open(CreateFilePath(saveName), FileMode.Create);
            file.Close();
        }

        public void InitializeSwitchSave()
        {


#if UNITY_SWITCH && !UNITY_EDITOR
            switchSaveSystem.InitializeSaveData();
#endif
        }

            public void Save(string saveName = "")
        {
#if UNITY_SWITCH && !UNITY_EDITOR
            switchSaveSystem.Save(SaveData.Current.progress);
#else
            SaveFile(saveName, SaveData.Current);
#endif
        }
        
        public void Load(string saveName = "")
        {
#if UNITY_SWITCH && !UNITY_EDITOR
            SaveData.Current.progress = switchSaveSystem.Load();
#else
            SaveData.Current = LoadFile(saveName);
#endif
        }

        public int GetProgressIndex(string saveName)
        {
#if UNITY_SWITCH
            return -1;
#endif
            return LoadFileWithFullPath(saveName).progress;
        }

        public string[] GetSaveFiles()
        {
#if UNITY_SWITCH
            return null;
#endif
            
            return Directory.Exists(SavesFolderPath) ? GetCorrectSaveFiles() : null;
        }

        private string[] GetCorrectSaveFiles()
        {
            List<string> listOfSaveFiles = new List<string>();
            string[] arrayOfSaveFiles = Directory.GetFiles(SavesFolderPath);

            foreach (var saveFile in arrayOfSaveFiles)
            {
                if(saveFile.Contains("Slot")) listOfSaveFiles.Add(saveFile);
            }

            return listOfSaveFiles.ToArray();
        }
        
        private static void SaveFile(string saveName, object state)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            if (!Directory.Exists(SavesFolderPath))
            {
                Directory.CreateDirectory(SavesFolderPath);
            }

            FileStream file = File.Create(CreateFilePath(saveName));
            formatter.Serialize(file, SaveData.Current);
            file.Close();
        }

        private SaveData LoadFile(string saveName)
        { 
            if(!Directory.Exists(SavesFolderPath))
            {
                Directory.CreateDirectory(SavesFolderPath);
            }
            
            if(!File.Exists(CreateFilePath(saveName)))
            {
                CreateEmptyFile(saveName);
            }
            
            using (FileStream stream = File.Open(CreateFilePath(saveName), FileMode.Open))
            {
                var formatter = new BinaryFormatter();
                return (SaveData) formatter.Deserialize(stream);
            }
        }

        private SaveData LoadFileWithFullPath(string fullPath)
        {
            using (FileStream stream = File.Open(fullPath, FileMode.Open))
            {
                var formatter = new BinaryFormatter();
                return (SaveData) formatter.Deserialize(stream);
            }
        }

        public void SaveSettings(AloneSettings settings)
        {
#if !UNITY_SWITCH
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream file = File.Create(CreateFilePath(SettingsFolderPath, "Settings", Extension));
            formatter.Serialize(file, settings);
            file.Close();
#else
            Debug.LogError("Save settings for switch");
#endif
        }

        public bool LoadSettings()
        {
#if !UNITY_SWITCH
            
            string settingsPath = CreateFilePath(SettingsFolderPath, "Settings", Extension);

            if (!File.Exists(settingsPath))
            {
                FileStream file = File.Open(settingsPath, FileMode.Create);
                file.Close();
            }

            using (FileStream stream = File.Open(settingsPath, FileMode.Open))
            {
                if (stream.Length > 0)
                {
                    var formatter = new BinaryFormatter();
                    GameManager.Instance.settings = (AloneSettings) formatter.Deserialize(stream);
                    return true;
                }

                return false;
            }
#else
            Debug.LogError("Load settings for switch");
            return false;
#endif
        }
        
        // Deprecated functions

        public void CheckForDeprecatedSaveFiles()
        {
#if UNITY_SWITCH
            return;
#endif
            
            if(CheckForGeneralFile()) DestroyAllFiles(false);
        }
        
        private static bool CheckForGeneralFile()
        {
            return CheckForFile(CreateFilePath(SavesFolderPath, "General", Extension));
        }
    }
}


