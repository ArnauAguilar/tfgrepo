﻿
using UnityEngine;

public static class ColorExtensionMethods
{
    public static Color Semitransparent(this Color color, float alpha = 0.5f)
    {
        color.a = alpha;
        return color;
    }

    public static Color Opaque(this Color color)
    {
        color.a = 1;
        return color;
    }
}
