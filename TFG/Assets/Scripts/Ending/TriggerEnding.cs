using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerEnding : MonoBehaviour
{

    [Header("Screen animation")]
    private Fader fader;

    private void Init()
    {
        fader = FindObjectOfType<Fader>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!fader)
        {
            Init();
        }

        StartCoroutine(StartTriggerEnding());


    }

    IEnumerator StartTriggerEnding()
    {
        fader.FadeIn(0.5f);

        yield return new WaitForSeconds(0.5f);

        SceneManager.LoadScene("Ending");
    }

}
