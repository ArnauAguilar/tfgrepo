using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndingSceneBehaviour : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(EndingAnimation());
    }

    IEnumerator EndingAnimation()
    {
        yield return new WaitForSeconds(3.0f);
        SceneManager.LoadScene("MainMenuExample");
    }
}
