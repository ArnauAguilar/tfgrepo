
using System;
using Alone;
using UnityEngine;
using Rewired;

public enum DeviceType
{
    Keyboard,
    XBox,
    PlayStation, 
    Gamepad,
    Switch
}

public class InputController : MonoBehaviour
{
    private static Player _rewired;

    public static DeviceType DeviceType { get; private set; }

    public static Signal OnControllerConnected;
    public static Signal OnControllerDisconnected;
    public Signal OnControlsChange;

    private void Awake()
    {
        _rewired = ReInput.players.GetPlayer(0);
    }

    private void Start()
    {
        ReInput.ControllerConnectedEvent += ControllerConnected;
        ReInput.ControllerDisconnectedEvent += ControllerDisconnected;
        
        _rewired.controllers.AddLastActiveControllerChangedDelegate(SetLastActiveController);
    }

    private void OnDestroy()
    {
        ReInput.ControllerConnectedEvent -= ControllerConnected;
        ReInput.ControllerDisconnectedEvent -= ControllerDisconnected;
        
        _rewired.controllers.RemoveLastActiveControllerChangedDelegate(SetLastActiveController);
    }
    
    
//     GET INPUT METHODS -----------------------------------------------------------------------------------------------
    
    // Key is the value common between the 2 axis. For example:
    // Key: "Move"
    // Horizontal Axis: "Move Horizontal"
    // Vertical Axis: "Move Vertical"
    public static Vector2 Get2DAxis(string key, bool inverted = false)
    {
        Vector2 axis = _rewired.GetAxis2D(key + " Horizontal", key + " Vertical");
        return inverted ? new Vector2(axis.y, axis.x) : axis;
    }

    public static bool GetButtonDown(string actionName)
    {
        return _rewired.GetButtonDown(actionName);
    }

    public static bool GetAnyButtonDown()
    {
        return _rewired.GetAnyButtonDown();
    }
    
    // Subscribe method to an input action
    public static void SubscribeToInput(Action<InputActionEventData> callback, string actionName)
    {
#if UNITY_SWITCH
        _rewired.AddInputEventDelegate(callback, UpdateLoopType.FixedUpdate, 
            InputActionEventType.ButtonJustPressed, actionName);
#else
        _rewired.AddInputEventDelegate(callback, UpdateLoopType.Update, 
            InputActionEventType.ButtonJustPressed, actionName);
#endif
    }
    
    // UnSubscribe method to an input action
    public static void UnSubscribeToInput(Action<InputActionEventData> callback, string actionName)
    {
#if UNITY_SWITCH
        _rewired.RemoveInputEventDelegate(callback, UpdateLoopType.FixedUpdate, 
            InputActionEventType.ButtonJustPressed, actionName);
#else
        _rewired.RemoveInputEventDelegate(callback, UpdateLoopType.Update, 
            InputActionEventType.ButtonJustPressed, actionName);
#endif
    }

    public static void SubscribeToInputUp(Action<InputActionEventData> callback, string actionName)
    {
#if UNITY_SWITCH
        _rewired.AddInputEventDelegate(callback, UpdateLoopType.FixedUpdate, 
            InputActionEventType.ButtonJustReleased, actionName);
#else
        _rewired.AddInputEventDelegate(callback, UpdateLoopType.Update, 
            InputActionEventType.ButtonJustReleased, actionName);
#endif
    }
    
    public static void UnSubscribeToInputUp(Action<InputActionEventData> callback, string actionName)
    {
#if UNITY_SWITCH
        _rewired.RemoveInputEventDelegate(callback, UpdateLoopType.FixedUpdate, 
            InputActionEventType.ButtonJustReleased, actionName);
#else
        _rewired.RemoveInputEventDelegate(callback, UpdateLoopType.Update, 
            InputActionEventType.ButtonJustReleased, actionName);
#endif
    }

//     CONTROLLERS CONNECTION AND DISCONNECTION ------------------------------------------------------------------------

    private void SetLastActiveController(Player player, Controller controller)
    {
        if (controller == null) return;

        if (GameManager.Instance.isOnSwitch)
        {
            DeviceType = DeviceType.Switch;
            return;
        }
        
        switch (controller.type)
        {
            case ControllerType.Keyboard:
            case ControllerType.Mouse:
                DeviceType = DeviceType.Keyboard;
                break;
                
            case ControllerType.Joystick:
                DeviceType = SortJoyStickTypes(controller);
                break;
        }
        OnControlsChange.Dispatch();
        GameManager.Instance.currentDeviceType = DeviceType;
    }

    private static DeviceType SortJoyStickTypes(Controller controller)
    {
        if (controller.name.Contains("XInput")) return DeviceType.XBox;
        if (controller.name.Contains("Sony")) return DeviceType.PlayStation;
        return DeviceType.Gamepad;
    }
    
    private static void ControllerConnected(ControllerStatusChangedEventArgs args)
    {
        OnControllerConnected.Dispatch();
    }

    private static void ControllerDisconnected(ControllerStatusChangedEventArgs args)
    {
        OnControllerDisconnected.Dispatch();
    }
}
