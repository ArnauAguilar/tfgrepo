
using UnityEngine;

[CreateAssetMenu(fileName = "InputActions", menuName = "InputActionsForPlatform", order = 1)]
public class InputActionForPlatform : ScriptableObject
{
    public DeviceType deviceType;
    
    public Sprite navigate;
    public Sprite navigateSecondary;
    
    public Sprite select;
    public Sprite selectSecondary;
    
    public Sprite cancelBack;

    public Sprite delete;

    public Sprite previousTab;
    public Sprite nextTab;

    public Sprite resetToDefault;
}
