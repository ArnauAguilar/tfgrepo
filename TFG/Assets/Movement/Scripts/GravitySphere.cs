﻿using System;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

public class GravitySphere : GravitySource
{
    [SerializeField] private float gravity = 9.81f;

    [SerializeField, Min(0f)] private float outerRadius = 10.0f, outerFalloffRadius = 15f;
    [SerializeField, Min(0f)] private float innerRadius = 5.0f, innerFalloffRadius = 1f;

    private float _outerFalloffFactor;
    private float _innerFalloffFactor;

    [SerializeField] private bool showExterior;
    
    private void Awake()
    {
        OnValidate();
    }

    private void OnValidate()
    {
        innerFalloffRadius = Mathf.Max(innerFalloffRadius, 0f);
        innerRadius = Mathf.Max(innerRadius, innerFalloffRadius);
        outerRadius = Mathf.Max(outerRadius, innerRadius);
        outerFalloffRadius = Mathf.Max(outerFalloffRadius, outerRadius);
        
        _innerFalloffFactor = 1f / (innerRadius - innerFalloffRadius);
        _outerFalloffFactor = 1f / (outerFalloffRadius - outerRadius);
    }

    public override Vector3 GetGravity(Vector3 position)
    {
        Vector3 vector = transform.position - position;
        float distance = vector.magnitude;
        if (distance > outerFalloffRadius)
        {
            return Vector3.zero;
        }

        float g = gravity / distance;
        if (distance > outerRadius)
        {
            g *= 1f - (distance - outerRadius) * _outerFalloffFactor;
        }
        else if (distance < innerRadius)
        {
            g *= 1f - (innerRadius - distance) * _innerFalloffFactor;
        }
        return g * vector;
    }

    private void OnDrawGizmos()
    {
        if (!gravityActivated || !enabled) return;

        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        
        if (innerFalloffRadius > 0f && innerFalloffRadius < innerRadius)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(Vector3.zero, innerFalloffRadius);
        }
        Gizmos.color = Color.yellow;
        if (innerRadius > 0f && innerRadius < outerRadius)
        {
            Gizmos.DrawWireSphere(Vector3.zero, innerRadius);
        }

        Gizmos.color = Color.yellow.Semitransparent();
        DrawSphere(Vector3.zero, outerRadius);
        
        if (outerFalloffRadius > outerRadius)
        {
            Gizmos.color = Color.cyan.Semitransparent();
            DrawSphere(Vector3.zero, outerFalloffRadius);
        }
    }
}
