﻿using UnityEngine;

[CreateAssetMenu(fileName = "GizmosHDSettings", menuName = "GizmosHD", order = 1)]
public class GizmosHDSettings : ScriptableObject
{
    public Mesh sphereHDMesh;
    public Mesh planeMesh;
}
