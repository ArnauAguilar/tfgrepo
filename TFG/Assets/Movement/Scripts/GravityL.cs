using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEditor;

public class GravityL : GravitySource
{
    [SerializeField] private float gravity = 9.81f;
    [SerializeField] private float edgeGravityMultilpier = 2f;
    
    [Title("Boundaries")]
    [SerializeField, Min(0f)] private Vector3 boundaryDistance;
    
    [Title("Distances")]
    [SerializeField, Min(0f)] private float outerDistance;
    [SerializeField, Min(0f)] private float outerFalloffDistance;
    [Space(10)]
    [SerializeField, Min(0f)] private float innerDistance;
    [SerializeField, Min(0f)] private float innerFalloffDistance;

    private float _outerFalloffFactor;

    private void OnValidate()
    {
        outerFalloffDistance = Mathf.Max(outerFalloffDistance, outerDistance);
        innerFalloffDistance = Mathf.Max(innerFalloffDistance, innerDistance);

        _outerFalloffFactor = 1f / (outerFalloffDistance - outerDistance);
    }

    public override Vector3 GetGravity(Vector3 position)
    {
        float x = Vector3.Dot(transform.right, position - transform.position);
        float y = Vector3.Dot(transform.up, position - transform.position);
        float z = Vector3.Dot(transform.forward, position - transform.position);

        // Y Face
        if (x > -boundaryDistance.x && x < boundaryDistance.x &&
            z > -boundaryDistance.z && z < boundaryDistance.z )
        {
            Vector3 up = transform.up;
            float distance = Vector3.Dot(up, position - (transform.position + transform.up * boundaryDistance.y));
            if (distance > outerFalloffDistance)
            {
                return Vector3.zero;
            }

            float g = gravity;
            if (distance > 0f)
            {
                if (distance > outerDistance)
                {
                    g *= 1f - (distance - outerDistance) * _outerFalloffFactor;

                }
            }
            else //Inside Red Y
            {
                if (x > -boundaryDistance.x && x < boundaryDistance.x && //Inside Red Z
                    y > -boundaryDistance.y && y < boundaryDistance.y)
                {

                    if (x > -boundaryDistance.x && x < boundaryDistance.x && //In Yellow Y and above Yellow Z
                        z > -boundaryDistance.z && z < boundaryDistance.z - innerDistance)
                    {
                        if (distance > -innerDistance) // Between Red and Yellow Y
                        {
                            return g * transform.up;
                        }
                        else //Above Yellow Y 
                        {
                            if (innerDistance > 0)
                            {
                                //Check closest wall
                                if (Mathf.Abs(z - boundaryDistance.z) < Mathf.Abs(y - boundaryDistance.y))
                                {
                                    return g * transform.forward;
                                }
                                else
                                {
                                    return g * transform.up;
                                }
                            }
                            else return Vector3.zero;

                        }
                    }

                    else if (x > -boundaryDistance.x && x < boundaryDistance.x && //Between Red and Yellow Z
                            y > -boundaryDistance.y && y < boundaryDistance.y - innerDistance)
                    {
                        return g * transform.forward;
                    }

                    else //On the corner
                    {
                        Vector3 projectedPoint = transform.position + transform.forward * (boundaryDistance.z - innerDistance)
                            + transform.up * (boundaryDistance.y - innerDistance) + transform.right * x;
                        return g * (position - projectedPoint).normalized;

                    }
                }
                else
                {
                    return Vector3.zero;
                }
            }
            return g * up;
            
        }

        // Z Face
        if (x > -boundaryDistance.x && x < boundaryDistance.x &&
            y > -boundaryDistance.y && y < boundaryDistance.y)
        {
            Vector3 up = transform.forward;
            float distance = Vector3.Dot(up, position - (transform.position + transform.forward * boundaryDistance.z));
            if (distance > outerFalloffDistance)
            {
                return Vector3.zero;
            }

            float g = gravity;
            if (distance > 0f)
            {
                if (distance > outerDistance)
                {
                    g *= 1f - (distance - outerDistance) * _outerFalloffFactor;

                }
            }
            else //Inside Red Z
            {
                if (x > -boundaryDistance.x && x < boundaryDistance.x && //Inside Red Y
                    z > -boundaryDistance.z && z < boundaryDistance.z)
                {

                    if (x > -boundaryDistance.x && x < boundaryDistance.x && //In Yellow Z and above Yellow Y
                        y > -boundaryDistance.y && y < boundaryDistance.y - innerDistance)
                    {
                        if (distance > -innerDistance) // Between Red and Yellow Z
                        {
                            return g * transform.forward;
                        }
                        else //Above Yellow Z 
                        {
                            if (innerDistance > 0)
                            {
                                //Check closest wall
                                if (Mathf.Abs(z - boundaryDistance.z) < Mathf.Abs(y - boundaryDistance.y))
                                {
                                    return g * transform.forward;
                                }
                                else
                                {
                                    return g * transform.up;
                                }
                            }
                            else return Vector3.zero;
                        }
                    }

                    else if (x > -boundaryDistance.x && x < boundaryDistance.x && //Between Red and Yellow Y
                            z > -boundaryDistance.z && z < boundaryDistance.z - innerDistance)
                    {
                        return g * transform.up;
                    }

                    else //On the corner
                    {
                        Debug.Log("Corner");
                        Vector3 projectedPoint = transform.position + transform.forward * (boundaryDistance.z - innerDistance)
                            + transform.up * (boundaryDistance.y - innerDistance) + transform.right * x;
                        return g * (position - projectedPoint).normalized;

                    }
                }
                else
                {
                    return Vector3.zero;
                }
            }
            return g * up;

        }


        // Edge
        if (x > -boundaryDistance.x && x < boundaryDistance.x &&
            z > boundaryDistance.z && y > boundaryDistance.y)
        {
            Vector3 projectedPoint = transform.position + transform.forward * boundaryDistance.z + transform.up * boundaryDistance.y + transform.right * x;

            float distance = Vector3.Distance(projectedPoint, position);

            if (distance > outerFalloffDistance)
            {
                return Vector3.zero;
            }

            float g = -gravity * edgeGravityMultilpier;
            if (distance < outerDistance)
            {
                g *= 1f - distance / outerDistance;
            }
            else
            {
                g *= 1f - (distance - outerDistance) * _outerFalloffFactor;
            }

            return (projectedPoint - position).normalized * g;
        }




        return Vector3.zero;
    }

    private void OnDrawGizmos()
    {
        if (!gravityActivated) return;
        
        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        float diagonalMult = 0.5773502692f;

        Gizmos.color = Color.red;
        DrawCube(Vector3.up * boundaryDistance.y, 
            new Vector3(boundaryDistance.x, 0, boundaryDistance.z) * 2);
        DrawCube(Vector3.forward * boundaryDistance.z, 
            new Vector3(boundaryDistance.x, boundaryDistance.y, 0) * 2);
        
        if (outerDistance > 0)
        {
            Gizmos.color = Color.yellow;
            DrawCube(Vector3.up * (boundaryDistance.y + outerDistance), 
                new Vector3(boundaryDistance.x, 0, boundaryDistance.z) * 2);
            DrawCube(Vector3.forward * (boundaryDistance.z + outerDistance), 
                new Vector3(boundaryDistance.x, boundaryDistance.y, 0) * 2);
        
            Vector3 p1 = new Vector3(boundaryDistance.x, boundaryDistance.y + outerDistance * diagonalMult,
                boundaryDistance.z + outerDistance * diagonalMult);
            Vector3 p2 = new Vector3(-boundaryDistance.x, boundaryDistance.y + outerDistance * diagonalMult, 
                boundaryDistance.z + outerDistance * diagonalMult);

            DrawLine(p1, p2, 5);
        }

        if (outerFalloffDistance > outerDistance)
        {
            Gizmos.color = Color.cyan;
            DrawCube(Vector3.up * (boundaryDistance.y + outerFalloffDistance), 
                new Vector3(boundaryDistance.x, 0, boundaryDistance.z) * 2);
            DrawCube(Vector3.forward * (boundaryDistance.z + outerFalloffDistance), 
                new Vector3(boundaryDistance.x, boundaryDistance.y, 0) * 2);

            Vector3 p1 = new Vector3(boundaryDistance.x, boundaryDistance.y + outerFalloffDistance * diagonalMult,
                boundaryDistance.z + outerFalloffDistance * diagonalMult);
            Vector3 p2 = new Vector3(-boundaryDistance.x, boundaryDistance.y + outerFalloffDistance * diagonalMult,
                boundaryDistance.z + outerFalloffDistance * diagonalMult);
            
            DrawLine(p1, p2, 5);
        }

        if (innerDistance > 0)
        {
            Gizmos.color = Color.yellow;
            DrawCube(Vector3.up * (boundaryDistance.y - innerDistance) + Vector3.forward * -innerDistance / 2,
                new Vector3(boundaryDistance.x, 0, boundaryDistance.z - innerDistance/2) * 2);
            DrawCube(Vector3.forward * (boundaryDistance.z - innerDistance) + Vector3.up * -innerDistance / 2,
                new Vector3(boundaryDistance.x, boundaryDistance.y - innerDistance/2, 0) * 2);

            //Vector3 p1 = new Vector3(boundaryDistance.x, boundaryDistance.y - innerDistance * diagonalMult,
            //    boundaryDistance.z - innerDistance * diagonalMult);
            //Vector3 p2 = new Vector3(-boundaryDistance.x, boundaryDistance.y - innerDistance * diagonalMult,
            //    boundaryDistance.z - innerDistance * diagonalMult);

            //DrawLine(p1, p2, 5);
        }

        if (innerFalloffDistance > innerDistance)
        {
            Gizmos.color = Color.cyan;
            DrawCube(Vector3.up * (boundaryDistance.y - innerFalloffDistance) + Vector3.forward * -innerFalloffDistance / 2,
                new Vector3(boundaryDistance.x, 0, boundaryDistance.z - innerFalloffDistance / 2) * 2);
            DrawCube(Vector3.forward * (boundaryDistance.z - innerFalloffDistance) + Vector3.up * -innerFalloffDistance / 2,
                new Vector3(boundaryDistance.x, boundaryDistance.y - innerFalloffDistance / 2, 0) * 2);

            //Vector3 p1 = new Vector3(boundaryDistance.x, boundaryDistance.y - innerFalloffDistance * diagonalMult,
            //    boundaryDistance.z - innerFalloffDistance * diagonalMult);
            //Vector3 p2 = new Vector3(-boundaryDistance.x, boundaryDistance.y - innerFalloffDistance * diagonalMult,
            //    boundaryDistance.z - innerFalloffDistance * diagonalMult);

            //DrawLine(p1, p2, 5);
        }

    }
}
