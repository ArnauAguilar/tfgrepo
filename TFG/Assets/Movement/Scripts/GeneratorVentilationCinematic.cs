using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class GeneratorVentilationCinematic : MonoBehaviour
{

    [SerializeField] private GravityGenerator _gravityGenerator;

    private GameObject _player;
    private GameObject _orbitcamera;
    private PlayableDirector _ventPlayableDirector;
    private Fader _fader;

    private void Start()
    {
        _gravityGenerator.OnGeneratorCinematic.Subscribe(PlayVentilatorCinematic);
    }

    private void PlayVentilatorCinematic()
    {
        if(!_player) _player = GameObject.Find("Player");
        if(!_fader) _fader = FindObjectOfType<Fader>();
        if (!_orbitcamera) _orbitcamera = FindObjectOfType<OrbitCamera>().gameObject;
        if (!_ventPlayableDirector) _ventPlayableDirector = GameObject.Find("VentilationCutscene").GetComponent<PlayableDirector>();
        StartCoroutine(VentilationCinematic());
    }

    IEnumerator VentilationCinematic()
    {
        _player.GetComponent<MovingSphere>().ManualInput = false;

        yield return null;

        _fader.FadeIn(0.5f);

        yield return new WaitForSeconds(0.6f);

        _ventPlayableDirector.Play();

        _fader.FadeOut(0.5f);

        yield return new WaitForSeconds(6.0f);

        _fader.FadeIn(0.5f);

        yield return new WaitForSeconds(1f);

        _fader.FadeOut(0.5f);

        yield return new WaitForSeconds(1.8f);

        _fader.FadeIn(0.5f);

        yield return new WaitForSeconds(1f);

        _fader.FadeOut(0.5f);

        yield return new WaitForSeconds(0.5f);

        _player.GetComponent<MovingSphere>().ManualInput = true;
    }

    private void OnDestroy()
    {
        _gravityGenerator.OnGeneratorCinematic.UnSubscribe(PlayVentilatorCinematic);
    }
}
