﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class GravityGenerator : MonoBehaviour
{
    private InteractCanvasManager _interactCanvasManager;
    
    [SerializeField] private PlayerTrigger trigger;
    
    [Space(10)]

    [SerializeField] private bool generatorIsActive;
    public bool GeneratorIsActive => generatorIsActive;

    private bool canInteract;

    public MeshRenderer rendererToChange;
    [SerializeField] private Material activeMaterial;
    [SerializeField] private Material inactiveMaterial;
    
    [SerializeField] private ReflectionProbe reflectionProbeToRender;
    [SerializeField] private GuidReference[] _gameobjectInteractionReferences;

    [Title("Gravities associated")]
    [SerializeField] private List<GravitySourceManager> _gravitySourceManagers;

    [Title("Animations to Trigger")]
    public Animator animator;
    public string animationTriggerName;

    private EmissionManager longestEmissionManager;
    public Signal OnGeneratorSwitch;
    public Signal OnGeneratorCinematic;
    
    [System.Serializable]
    private struct GravitySourceManager
    {
        [Required, InlineEditor] 
        public GravitySource gravitySource;

        public enum Status
        {
            On,
            Off
        };
        
        [ShowIf("gravitySource", null), EnumToggleButtons]
        public Status onGeneratorActive;
        
        [ShowIf("gravitySource", null), EnumToggleButtons]
        public Status onGeneratorInactive;

        public bool hasVisuals;
        [ShowIf("hasVisuals")]
        public EmissionManager EmissionManager;
    }

    private void Start()
    {
        if (reflectionProbeToRender != null) reflectionProbeToRender.RenderProbe();
        _interactCanvasManager = FindObjectOfType<InteractCanvasManager>();
        
        if (!trigger)
        {
            trigger = GetComponentInChildren<PlayerTrigger>();
        }

        if (rendererToChange == null)
            rendererToChange = GetComponent<MeshRenderer>();
        
        SetMaterial();

        trigger.onPlayerEnter.Subscribe(OnGeneratorTriggerEnter);
        trigger.onPlayerExit.Subscribe(OnGeneratorTriggerExit);

        float longestTime = 0;
        foreach (GravitySourceManager gravitySourceManager in _gravitySourceManagers)
        {
            if(gravitySourceManager.hasVisuals)
                if (gravitySourceManager.EmissionManager.totalTimeToLit > longestTime)
                {
                    longestEmissionManager = gravitySourceManager.EmissionManager;
                    longestTime = gravitySourceManager.EmissionManager.totalTimeToLit;
                }
        }
        
        InputController.SubscribeToInput(OnInteract, "Interact");
        
        OnGeneratorSwitch.Dispatch();
    }

    private void OnDestroy()
    {
        InputController.UnSubscribeToInput(OnInteract, "Interact");
    }

    private void OnGeneratorTriggerEnter()
    {
        canInteract = true;
        CheckForInteractCanvasManager();
        _interactCanvasManager.ShowInteractMessage();
    }
    
    private void OnGeneratorTriggerExit()
    {
        canInteract = false;
        CheckForInteractCanvasManager();
        _interactCanvasManager.HideInteractMessage();
    }

    private void CheckForInteractCanvasManager()
    {
        if (!_interactCanvasManager) _interactCanvasManager = FindObjectOfType<InteractCanvasManager>();
    }

    private void OnInteract(Rewired.InputActionEventData data)
    {
        if (!canInteract) return;

        generatorIsActive = !generatorIsActive;
        OnGeneratorSwitch.Dispatch();

        OnGeneratorCinematic.Dispatch();
        
        ToggleActive();

        if (animator != null)
        {
            animator.SetTrigger(animationTriggerName);
        }
    }

    private void SetMaterial()
    {
        rendererToChange.material = generatorIsActive ? activeMaterial : inactiveMaterial;
    }

    private void ToggleActiveNonVisuals(bool b)
    {
        foreach (var manager in _gravitySourceManagers)
        {
            if (!manager.hasVisuals)
            {
                if (generatorIsActive)
                {
                    manager.gravitySource.ToggleGravity(manager.onGeneratorActive == GravitySourceManager.Status.On);
                }
                else
                {
                    manager.gravitySource.ToggleGravity(manager.onGeneratorInactive == GravitySourceManager.Status.On);
                }
            }
        }
    }

    private void ReRenderReflectionProbe(bool a)
    {
        if (reflectionProbeToRender == null) return;
        reflectionProbeToRender.RenderProbe();
    }

    private void ToggleActive()
    {
        SetMaterial();
        longestEmissionManager.emissionAnimationDone.SubscribeOnce(ToggleActiveNonVisuals);
        ToggleGameObjectsInteraction();
        foreach (var manager in _gravitySourceManagers)
        {
            if (manager.hasVisuals)
            {
                if (generatorIsActive)
                {
                    if (manager.onGeneratorActive == GravitySourceManager.Status.On)
                    {
                        manager.EmissionManager.TurnOn();
                        manager.EmissionManager.emissionAnimationDone.SubscribeOnce(manager.gravitySource.ToggleGravity);                        
                        manager.EmissionManager.emissionAnimationDone.SubscribeOnce(ReRenderReflectionProbe);                        
                    }
                    else if (manager.onGeneratorActive == GravitySourceManager.Status.Off)
                    {
                        manager.EmissionManager.TurnOff();
                        manager.EmissionManager.emissionAnimationDone.SubscribeOnce(manager.gravitySource.ToggleGravity); 
                        manager.EmissionManager.emissionAnimationDone.SubscribeOnce(ReRenderReflectionProbe); 
                    }
                        
                }
                else
                {
                    if(manager.onGeneratorInactive == GravitySourceManager.Status.On)
                    {
                        manager.EmissionManager.TurnOn();
                        manager.EmissionManager.emissionAnimationDone.SubscribeOnce(manager.gravitySource.ToggleGravity);                        
                        manager.EmissionManager.emissionAnimationDone.SubscribeOnce(ReRenderReflectionProbe);                        
                    }
                    else if(manager.onGeneratorInactive == GravitySourceManager.Status.Off)
                    {
                        manager.EmissionManager.TurnOff();
                        manager.EmissionManager.emissionAnimationDone.SubscribeOnce(manager.gravitySource.ToggleGravity); 
                        manager.EmissionManager.emissionAnimationDone.SubscribeOnce(ReRenderReflectionProbe); 
                    }
                }
            }
        }
    }

    private void ToggleGameObjectsInteraction()
    {
        if (_gameobjectInteractionReferences.Length > 0)
        {
            foreach (GuidReference current in _gameobjectInteractionReferences)
            {
                current.gameObject.SetActive(!current.gameObject.activeSelf);
            }
        }
    }

    public void ToggleGenerator(bool shouldBe)
    {
        if (generatorIsActive == shouldBe) return;

        generatorIsActive = shouldBe;
        ToggleActive();
        
        if (animator != null)
        {
            animator.SetTrigger(animationTriggerName);
        }
    }
}
