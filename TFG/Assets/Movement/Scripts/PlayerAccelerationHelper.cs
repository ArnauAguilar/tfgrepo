using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAccelerationHelper : MonoBehaviour
{

    [SerializeField]
    private float _targetAirAcceleration;

    private float _originalAirAcceleration;
    private MovingSphere player;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            player = other.gameObject.GetComponent<MovingSphere>();
            _originalAirAcceleration = player.maxAirAcceleration;
            player.maxAirAcceleration = _targetAirAcceleration;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            player.maxAirAcceleration = _originalAirAcceleration;
        }
    }
}
