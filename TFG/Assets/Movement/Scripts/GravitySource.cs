﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEditor;

public class GravitySource : MonoBehaviour
{
    [SerializeField, OnValueChanged("ToggleGravity")] 
    protected bool gravityActivated;

    protected bool advancedGizmosMode;
    
    protected virtual void OnEnable()
    {
        if (!gravityActivated) return;
        
        CustomGravity.Register(this);
    }

    protected virtual void OnDisable()
    {
        if (!gravityActivated) return;
        CustomGravity.Unregister(this);
    }

    private void ActivateGravity()
    {
        gravityActivated = true;
        CustomGravity.Register(this);
    }

    private void DeactivateGravity()
    {
        gravityActivated = false;
        CustomGravity.Unregister(this);
    }

    public virtual void ToggleGravity(bool active)
    {
        gravityActivated = active;
        (gravityActivated ? (Action)ActivateGravity : DeactivateGravity)();
        
    }

    private void CheckForGravityActivated()
    {
        if (!Application.isPlaying) return;

        (gravityActivated ? (Action)ActivateGravity : DeactivateGravity)();
    }
    
    public virtual Vector3 GetGravity (Vector3 position)
    {
        return Physics.gravity;
    }
    
    [Button]
    public void ToggleGizmosMode()
    {
        advancedGizmosMode = !advancedGizmosMode;
    }

    protected void DrawSphere(Vector3 position, float radius)
    {
        if (advancedGizmosMode)
        {
            Gizmos.color = Gizmos.color.Semitransparent();
            Gizmos.DrawMesh(GizmosHD.HDSphere, position, Quaternion.identity, Vector3.one * radius);
        }
        else
        {
            Gizmos.color = Gizmos.color.Opaque();
            Gizmos.DrawWireSphere(position, radius);
        }
    }
    
    protected void DrawCube(Vector3 position, Vector3 size)
    {
        if (advancedGizmosMode)
        {
            Gizmos.color = Gizmos.color.Semitransparent();
            Gizmos.DrawCube(position, size);
        }
        else
        {
            Gizmos.color = Gizmos.color.Opaque();
            Gizmos.DrawWireCube(position, size);
        }
    }

    protected void DrawLine(Vector3 p1, Vector3 p2, float thickness = 0)
    {
        if (advancedGizmosMode)
        {
#if UNITY_EDITOR
            Handles.color = Gizmos.color.Semitransparent();
            Handles.matrix = Gizmos.matrix;
            
            Handles.DrawBezier(p1,p2,p1,p2, Handles.color,null, thickness);
#endif
        }
        else
        {
            Gizmos.color = Gizmos.color.Opaque();
            Gizmos.DrawLine(p1, p2);
        }
    }
}
