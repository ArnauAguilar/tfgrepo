using System;
using UnityEngine;

namespace formulas
{
    public static class CMath
    {
        public static Vector3 MultiplyVectors(Vector3 a, Vector3 b)
        {
            return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
        }

        public static Vector3 NormalizeThreshold(Vector3 vector, float threshold = 0.05f)
        {
            return vector.magnitude > threshold ? vector.normalized : Vector3.zero;
        }

        public static Vector3 CreateMask(Vector3 originalVector, float tolerance = -1)
        {
            Vector3 absVector = Abs(originalVector);
            
            float minValue = tolerance < 0 ? Mathf.Max(absVector.x,Mathf.Max(absVector.y, absVector.z)) : tolerance;
            minValue = minValue == 0 ? 1 : minValue;
            
            return new Vector3(
                absVector.x >= minValue ? 1 : 0,
                absVector.y >= minValue ? 1 : 0,
                absVector.z >= minValue ? 1 : 0
            );
        }

        public static Vector3 Abs(Vector3 vector)
        {
            return new Vector3(Mathf.Abs(vector.x), Mathf.Abs(vector.y), Mathf.Abs(vector.z));
        }
        
        public static Vector3 CreateUnitaryMask(Vector3 originalVector)
        {
            Vector3 absVector = Abs(originalVector);
            
            float maxValue = Mathf.Max(absVector.x, Mathf.Max(absVector.y, absVector.z));
            maxValue = maxValue > 0 ? maxValue : 1;
            
            if (absVector.x >= maxValue)
            {
                return new Vector3(1,0,0);
            }
            if (absVector.y >= maxValue)
            {
                return new Vector3(0,1,0);
            }
            if (absVector.z >= maxValue)
            {
                return new Vector3(0,0,1);
            }
            return new Vector3(0,0,0);
        }
        
        public static Vector3 CreateSignedUnitaryMask(Vector3 originalVector)
        {
            Vector3 absVector = Abs(originalVector);
            
            float maxValue = Mathf.Max(absVector.x, Mathf.Max(absVector.y, absVector.z));
            maxValue = maxValue > 0 ? maxValue : 1;
            
            if (absVector.x >= maxValue)
            {
                return new Vector3(Mathf.Sign(originalVector.x),0,0);
            }
            if (absVector.y >= maxValue)
            {
                return new Vector3(0,Mathf.Sign(originalVector.y),0);
            }
            if (absVector.z >= maxValue)
            {
                return new Vector3(0,0,Mathf.Sign(originalVector.z));
            }
            return new Vector3(0,0,0);
        }

        public static Vector3 InverseMask(Vector3 a)
        {
            return new Vector3(a.x == 0 ? 1 : 0, a.y == 0 ? 1 : 0, a.z == 0 ? 1 : 0);
        }

        public static Quaternion GetQuaternion(Vector3 rotationAxis, float angle)
        {
            return new Quaternion(rotationAxis.x * Mathf.Sin(angle/2f), rotationAxis.y * Mathf.Sin(angle/2f), rotationAxis.z * Mathf.Sin(angle/2f), Mathf.Cos(angle/2f));
        }
        
        public static float RoundToDecimals(float value, int decimals)
        {
            return (float)((int)(value * Math.Pow(10, decimals)) / Math.Pow(10, decimals));
        }

    }
}