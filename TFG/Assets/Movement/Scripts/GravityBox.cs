﻿using Sirenix.OdinInspector;
using UnityEngine;

public class GravityBox : GravitySource
{
    [SerializeField] private float gravity = 9.81f;
    
    [Title("Boundaries")]
    [SerializeField, MinValue(0f)]
    private Vector3 boundaryDistance = Vector3.one;

    [Title("Distances")]
    [SerializeField, Min(0f)]
    private float innerDistance;
    
    [SerializeField, Min(0f)]
    private float innerFalloffDistance;
    private float _innerFalloffFactor;
    
    [Space]
    
    [SerializeField, Min(0f)] 
    private float outerDistance;
    
    [SerializeField, Min(0f)]
    private float outerFalloffDistance;
    private float _outerFalloffFactor;
    
    [Title("Faces")]
    [SerializeField]
    [ListDrawerSettings(ShowIndexLabels = true, ShowItemCount = false, HideRemoveButton = true, HideAddButton = true, 
        DraggableItems = false, Expanded = true)]
    private bool[] faces = {true, true, true, true, true, true};

    private void Awake()
    {
        OnValidate();
    }
    
    private void OnValidate()
    {
        boundaryDistance = Vector3.Max(boundaryDistance, Vector3.zero);
        float maxInner = Mathf.Min(Mathf.Min(boundaryDistance.x, boundaryDistance.y), boundaryDistance.z);
        innerDistance = Mathf.Min(innerDistance, maxInner);
        innerFalloffDistance = Mathf.Max(Mathf.Min(innerFalloffDistance, maxInner), innerDistance);
        outerFalloffDistance = Mathf.Max(outerFalloffDistance, outerDistance);

        _innerFalloffFactor = 1f / (innerFalloffDistance - innerDistance);
        _outerFalloffFactor = 1f / (outerFalloffDistance - outerDistance);
    }
    
    private float GetGravityComponent (float coordinate, float distance)
    {
        if (distance > innerFalloffDistance) return 0f;

        float g = gravity;
        if (distance > innerDistance)
        {
            g *= 1f - (distance - innerDistance) * _innerFalloffFactor;
        }
        return coordinate > 0f ? -g : g;
    }

    public override Vector3 GetGravity(Vector3 position)
    {
        position = transform.InverseTransformDirection(position - transform.position);
        Vector3 vector = Vector3.zero;
        Vector3 vector2 = Vector3.zero;
        int outside = 0;
        if (position.x > boundaryDistance.x)
        {
            vector.x = boundaryDistance.x - position.x;
            outside = 1;
            if (faces[0])
            {
                vector2.x = vector.x;
            }
        }
        else if (position.x < -boundaryDistance.x)
        {
            vector.x = -boundaryDistance.x - position.x;
            outside = 1;
            if (faces[1])
            {
                vector2.x = vector.x;
            }
        }
        
        if (position.y > boundaryDistance.y)
        {
            vector.y = boundaryDistance.y - position.y;
            outside += 1;
            if (faces[4])
            {
                vector2.y = vector.y;
            }
        }
        else if (position.y < -boundaryDistance.y)
        {
            vector.y = -boundaryDistance.y - position.y;
            outside += 1;
            if (faces[5])
            {
                vector2.y = vector.y;
            }
        }

        if (position.z > boundaryDistance.z)
        {
            vector.z = boundaryDistance.z - position.z;
            outside += 1;
            if (faces[2])
            {
                vector2.z = vector.z;
            }
        }
        else if (position.z < -boundaryDistance.z)
        {
            vector.z = -boundaryDistance.z - position.z;
            outside += 1;
            if (faces[3])
            {
                vector2.z = vector.z;
            }
        }

        if (outside > 0)
        {
            float distance = outside == 1 ? Mathf.Abs(vector.x + vector.y + vector.z) : vector.magnitude;
            if (distance > outerFalloffDistance)
            {
                return Vector3.zero;
            }
            float g = gravity / distance;
            if (distance > outerDistance)
            {
                g *= 1f - (distance - outerDistance) * _outerFalloffFactor;
            }
            return transform.TransformDirection(g * vector2);
        }

        float[] distances = new float[]
        {
            // 0
            Mathf.Abs(boundaryDistance.x - position.x),
            // 1
            Mathf.Abs(-boundaryDistance.x - position.x),
            // 2
            Mathf.Abs(boundaryDistance.z - position.z),
            // 3
            Mathf.Abs(-boundaryDistance.z - position.z),
            // 4
            Mathf.Abs(boundaryDistance.y - position.y),
            // 5
            Mathf.Abs(-boundaryDistance.y - position.y)
        };

        float minDistance = float.MaxValue;
        int minDistanceIndex = -1;
        
        for(int i = 0; i < distances.Length; i++)
        {
            if (distances[i] < minDistance && faces[i])
            {
                minDistance = distances[i];
                minDistanceIndex = i;
            }
        }

        switch (minDistanceIndex)
        {
            case 0:
                vector.x = GetGravityComponent(position.x, distances[0]);
                break;
            case 1:
                vector.x = GetGravityComponent(position.x, distances[1]);
                break;
            case 2:
                vector.z = GetGravityComponent(position.z, distances[2]);
                break;
            case 3:
                vector.z = GetGravityComponent(position.z, distances[3]);
                break;
            case 4:
                vector.y = GetGravityComponent(position.y, distances[4]);
                break;
            case 5:
                vector.y = GetGravityComponent(position.y, distances[5]);
                break;
        }
        return transform.TransformDirection(vector);
    }

    
    // -----------------------------------------------------------------------------------------------------------------
    // GIZMOS ----------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    private void OnDrawGizmos()
    {
        if (!gravityActivated) return;

        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        
        DrawOuterDistance();
        DrawInnerDistance();
        
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(Vector3.zero, 2f * boundaryDistance);

        DrawFacesNumbers();
    }

    private void DrawFacesNumbers()
    {
#if UNITY_EDITOR
        var style = new GUIStyle();
        var color = Color.red;
        style.normal.textColor = UnityEditor.Handles.color = color;
        
        UnityEditor.Handles.matrix = Gizmos.matrix;

        style.normal.textColor = UnityEditor.Handles.color = faces[0] ? Color.green : Color.red;
        UnityEditor.Handles.Label (Vector3.right * boundaryDistance.x, "0", style);
        
        style.normal.textColor = UnityEditor.Handles.color = faces[1] ? Color.green : Color.red;
        UnityEditor.Handles.Label (Vector3.right * -boundaryDistance.x, "1", style);
        
        style.normal.textColor = UnityEditor.Handles.color = faces[2] ? Color.green : Color.red;
        UnityEditor.Handles.Label (Vector3.forward * boundaryDistance.x, "2", style);
        
        style.normal.textColor = UnityEditor.Handles.color = faces[3] ? Color.green : Color.red;
        UnityEditor.Handles.Label (Vector3.forward * -boundaryDistance.x, "3", style);
        
        style.normal.textColor = UnityEditor.Handles.color = faces[4] ? Color.green : Color.red;
        UnityEditor.Handles.Label (Vector3.up * boundaryDistance.x, "4", style);
        
        style.normal.textColor = UnityEditor.Handles.color = faces[5] ? Color.green : Color.red;
        UnityEditor.Handles.Label (Vector3.up * -boundaryDistance.x, "5", style);
#endif
    }

    private void DrawInnerDistance()
    {
        Vector3 size;
        if (innerFalloffDistance > innerDistance)
        {
            Gizmos.color = Color.cyan;
            size.x = 2f * (boundaryDistance.x - innerFalloffDistance);
            size.y = 2f * (boundaryDistance.y - innerFalloffDistance);
            size.z = 2f * (boundaryDistance.z - innerFalloffDistance);
            Gizmos.DrawWireCube(Vector3.zero, size);
        }
        if (innerDistance > 0f)
        {
            Gizmos.color = Color.yellow;
            size.x = 2f * (boundaryDistance.x - innerDistance);
            size.y = 2f * (boundaryDistance.y - innerDistance);
            size.z = 2f * (boundaryDistance.z - innerDistance);
            Gizmos.DrawWireCube(Vector3.zero, size);
        }
    }

    private void DrawOuterDistance()
    {
        if (outerDistance > 0f)
        {
            Gizmos.color = Color.yellow;
            DrawOuterGizmos(outerDistance);
        }

        if (outerFalloffDistance > outerDistance)
        {
            Gizmos.color = Color.cyan;
            DrawOuterGizmos(outerFalloffDistance);
        }
    }

    private void DrawOuterGizmos(float distance)
    {
        Vector3 size = boundaryDistance;
        
        if(faces[0]) Gizmos.DrawWireCube(Vector3.right * (boundaryDistance.x + distance), 
            new Vector3(0, boundaryDistance.y, boundaryDistance.z) * 2f);
        if(faces[1]) Gizmos.DrawWireCube(-Vector3.right * (boundaryDistance.x + distance), 
            new Vector3(0, boundaryDistance.y, boundaryDistance.z) * 2f);
        if(faces[2]) Gizmos.DrawWireCube(Vector3.forward * (boundaryDistance.z + distance), 
            new Vector3(boundaryDistance.x, boundaryDistance.y, 0) * 2f);
        if(faces[3]) Gizmos.DrawWireCube(-Vector3.forward * (boundaryDistance.z + distance), 
            new Vector3(boundaryDistance.x, boundaryDistance.y, 0) * 2f);
        if(faces[4]) Gizmos.DrawWireCube(Vector3.up * (boundaryDistance.y + distance), 
            new Vector3(boundaryDistance.x, 0, boundaryDistance.z) * 2f);
        if(faces[5]) Gizmos.DrawWireCube(-Vector3.up * (boundaryDistance.y + distance), 
            new Vector3(boundaryDistance.x, 0, boundaryDistance.z) * 2f);

        distance *= 0.5773502692f;
        size.x = 2f * (size.x + distance);
        size.y = 2f * (size.y + distance);
        size.z = 2f * (size.z + distance);
            
        Gizmos.DrawWireCube(Vector3.zero, size);
    }
}
