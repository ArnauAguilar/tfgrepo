﻿using UnityEngine;

public class GravitySquarePlane : GravitySource
{
    [SerializeField] private float gravity = 9.81f;
    
    [SerializeField] private Vector2 boundaryDistance;
    
    [SerializeField, Min(0f)] private float range = 1f;
    [SerializeField, Min(0f)] private float falloffDistance = 1f;

    private float _falloffFactor;
    
    private void Awake()
    {
        OnValidate();
    }

    private void OnValidate()
    {
        boundaryDistance = Vector2.Max(boundaryDistance, Vector2.zero);

        falloffDistance = Mathf.Max(range, falloffDistance);
        _falloffFactor = 1f / (falloffDistance - range);
    }

    public override Vector3 GetGravity(Vector3 position)
    {
        float x = Vector3.Dot(transform.right, position - transform.position);
        float z = Vector3.Dot(transform.forward, position - transform.position);

        if (x > boundaryDistance.x / 2f || x < -boundaryDistance.x /2f 
            || z > boundaryDistance.y / 2f || z < -boundaryDistance.y / 2f)
        {
            return Vector3.zero;
        }
        
        Vector3 up = transform.up;
        float distance = Vector3.Dot(up, position - transform.position);
        if (distance > falloffDistance)
        {
            return Vector3.zero;
        }
        
        float g = -gravity;
        if (distance > 0f)
        {
            if (distance > range)
            {
                g *= 1f - (distance - range) * _falloffFactor;

            }
        }
        else
        {
            return Vector3.zero;
        }
        return g * up;
    }

    private void OnDrawGizmos()
    {
        if (!gravityActivated) return;
        
        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        
        Vector3 size = new Vector3(boundaryDistance.x, 0f, boundaryDistance.y);
        
        Gizmos.color = Color.red;
        DrawCube(Vector3.zero, size);

        if (range > 0f)
        {
            Gizmos.color = Color.yellow;
            DrawCube(Vector3.up * range, size);
        }

        if (falloffDistance > range)
        {
            Gizmos.color = Color.cyan;
            DrawCube(Vector3.up * falloffDistance, size);
        }
    }
}
