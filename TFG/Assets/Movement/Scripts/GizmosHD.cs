﻿using UnityEngine;

public static class GizmosHD
{
    public static Mesh HDSphere => Resources.Load<GizmosHDSettings>("GizmosHDSettings").sphereHDMesh;

}
