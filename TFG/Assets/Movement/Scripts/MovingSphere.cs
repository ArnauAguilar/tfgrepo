﻿using System;
using Alone.Menu;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

public class MovingSphere : MonoBehaviour
{
    [SerializeField] private Transform playerInputSpace = default;
    public Quaternion InputSpaceOffset = Quaternion.identity;

    [SerializeField] private bool useRightHandedScheme;
    public bool UseRightHandedScheme { set => useRightHandedScheme = value; }
    
    [SerializeField, Range(0f, 100f)] public float maxSpeed = 10f;
    [SerializeField, Range(0f, 100f)] public float animationSpeed = 1f;
    [SerializeField, Range(0f, 100f)] private float maxAcceleration = 10f;
    [SerializeField, Range(0f, 100f)] public float maxAirAcceleration = 1f;

    [SerializeField, Range(0f, 10f)] private float jumpHeight = 2f;
    [SerializeField, Range(0, 5)] private int maxAirJumps = 0;

    [SerializeField, Range(0f, 90f)] private float maxGroundAngle = 25f, maxStairsAngle = 50f;
    [SerializeField, Range(0f, 100f)] private float maxSnapSpeed = 100f;

    [SerializeField, Min(0f)] private float probeDistance = 1f;
    [SerializeField] private LayerMask probeMask = -1, stairsMask = -1;

    Rigidbody body;

    private bool manualInput;
    public bool ManualInput { set => manualInput = value; }
    
    Vector2 playerInput = Vector2.zero;
    [HideInInspector] public Vector3 velocity = Vector3.zero;
    Vector3 desiredVelocity = Vector3.zero;
    public Vector3 DesiredVelocity { set => desiredVelocity = value * maxSpeed; }
    Vector3 contactNormal, steepNormal;
    float acceleration;
    float maxSpeedChange;
    float minGroundDotProduct, minStairsDotProduct;
    bool desiredJump;
    public int groundContactCount;
    int steepContactCount;
    int jumpPhase;
    int stepsSinceLastGrounded, stepsSinceLastJump;
    

    private Vector3 upAxis, rightAxis, forwardAxis;

    public Vector3 UpAxis => upAxis;

    public bool OnGround => groundContactCount > 0;
    public bool OnSteep => steepContactCount > 0;

    [Header("Sound")]
    [SerializeField] private AudioSource jumpAudioSource;
    [SerializeField] private Vector2 minMaxVolume;
    [SerializeField] private Vector2 minMaxPitch;
    
    private void OnValidate()
    {
        minGroundDotProduct = Mathf.Cos(maxGroundAngle * Mathf.Deg2Rad);
        minStairsDotProduct = Mathf.Cos(maxStairsAngle * Mathf.Deg2Rad);
    }

    private void Awake()
    {
        manualInput = true;
        Cursor.lockState = CursorLockMode.Locked;
        body = GetComponent<Rigidbody>();
        body.useGravity = false;
        OnValidate();
        
        InputController.SubscribeToInput(OnJumpPressed, "Jump");
        
        AloneSettings aloneSettings = GameManager.Instance.settings;
        useRightHandedScheme = aloneSettings.useRightHandedStickMoveLook;
    }

    private void OnDestroy()
    {
        InputController.UnSubscribeToInput(OnJumpPressed, "Jump");
    }

    private void Update()
    {
#if !UNITY_SWITCH
        if (PauseMenu.IsPaused()) return;
        GetInput();
#endif
    }

    private void FixedUpdate()
    {
#if UNITY_SWITCH
        GetInput();
#endif
        
        Vector3 gravity = CustomGravity.GetGravity(body.position, out upAxis);

        UpdateState();
        AdjustVelocity();

        if (desiredJump)
        {
            desiredJump = false;
            Jump(gravity);
        }

        velocity += gravity * Time.fixedDeltaTime;
        body.velocity = velocity;

        ClearState();
    }

    private void GetInput()
    {
        //Debug.Log("F Axys: " + ProjectDirectionOnPlane(playerInputSpace.forward, upAxis));
        //Debug.Log("F mod Axys: " + ProjectDirectionOnPlane(playerInputSpace.forward, InputSpaceOffset*upAxis));
        rightAxis = ProjectDirectionOnPlane(playerInputSpace.right, upAxis);
        forwardAxis = ProjectDirectionOnPlane(playerInputSpace.forward, InputSpaceOffset*upAxis);
        
        if (!manualInput) return;
        
        
        DeviceType currentDeviceType = InputController.DeviceType;
        playerInput = currentDeviceType == DeviceType.Keyboard ? InputController.Get2DAxis("Move") :
            InputController.Get2DAxis( useRightHandedScheme ? "Move" : "Look");
        playerInput = Vector2.ClampMagnitude(playerInput, 1f);

        desiredVelocity = new Vector3(playerInput.x, 0.0f, playerInput.y) * maxSpeed;
    }
    
    private void ClearState()
    {
        groundContactCount = steepContactCount = 0;
        contactNormal = steepNormal = Vector3.zero;
    }

    private void UpdateState()
    {
        stepsSinceLastGrounded += 1;
        stepsSinceLastJump += 1;
        velocity = body.velocity;
        if (OnGround || SnapToGround() || CheckSteepContacts())
        {
            stepsSinceLastGrounded = 0;
            if (stepsSinceLastJump > 1)
            {
                jumpPhase = 0;
            }
            if (groundContactCount > 1)
            {
                contactNormal.Normalize();
            }
        }
        else
        {
            contactNormal = upAxis;
        }
    }

    private void OnJumpPressed(Rewired.InputActionEventData data)
    {
        if (!PauseMenu.IsPaused() && manualInput)
        {
            desiredJump = true;
        }
    }

    private void Jump(Vector3 gravity)
    {
        Vector3 jumpDirection;
        if (OnGround)
        {
            jumpDirection = contactNormal;
        }
        else if (OnSteep)
        {
            jumpDirection = steepNormal;
            jumpPhase = 0;
        }
        else if (maxAirJumps > 0 && jumpPhase <= maxAirJumps)
        {
            if (jumpPhase == 0)
            {
                jumpPhase = 1;
            }
            jumpDirection = contactNormal;
        }
        else
        {
            return;
        }

        stepsSinceLastJump = 0;
        jumpPhase += 1;
        float jumpSpeed = Mathf.Sqrt(2f * gravity.magnitude * jumpHeight);
        jumpDirection = (jumpDirection + upAxis).normalized;
        float alignedSpeed = Vector3.Dot(velocity, jumpDirection);
        if (alignedSpeed > 0f)
        {
            jumpSpeed = Mathf.Max(jumpSpeed - alignedSpeed, 0f);
        }
        velocity += jumpDirection * jumpSpeed;

        jumpAudioSource.volume = Random.Range(minMaxVolume.x, minMaxVolume.y);
        jumpAudioSource.pitch = Random.Range(minMaxPitch.x, minMaxPitch.y);
        jumpAudioSource.Play();
    }

    private bool SnapToGround()
    {
        if (stepsSinceLastGrounded > 1 || stepsSinceLastJump <= 2)
        {
            return false;
        }
        float speed = velocity.magnitude;
        if (speed > maxSnapSpeed)
        {
            return false;
        }
        if (!Physics.Raycast(body.position, -upAxis, out RaycastHit hit, probeDistance, probeMask))
        {
            return false;
        }

        float upDot = Vector3.Dot(upAxis, hit.normal);
        if (upDot < GetMinDot(hit.collider.gameObject.layer))
        {
            return false;
        }

        groundContactCount = 1;
        contactNormal = hit.normal;
        float dot = Vector3.Dot(velocity, hit.normal);
        if (dot > 0f)
        {
            velocity = (velocity - hit.normal * dot).normalized * speed;
        }

        return true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        EvaluateCollision(collision);
    }

    private void OnCollisionStay(Collision collision)
    {
        EvaluateCollision(collision);
    }

    private void EvaluateCollision(Collision collision)
    {
        float minDot = GetMinDot(collision.gameObject.layer);
        for (int i = 0; i < collision.contactCount; i++)
        {
            Vector3 normal = collision.GetContact(i).normal;
            float upDot = Vector3.Dot(upAxis, normal);

            if (upDot >= minDot)
            {
                groundContactCount += 1;
                contactNormal += normal;
            }
            //if steep needed, tweek the 0.01 value
            else if (upDot > 0.681998f)
            {
                steepContactCount += 1;
                steepNormal += normal;
            }
        }
    }

    private bool CheckSteepContacts ()
    {
        if (steepContactCount > 1)
        {
            steepNormal.Normalize();
            float upDot = Vector3.Dot(upAxis, steepNormal);
            if (upDot >= minGroundDotProduct)
            {
                groundContactCount = 1;
                contactNormal = steepNormal;
                return true;
            }
        }
        return false;
    }

    private static Vector3 ProjectDirectionOnPlane(Vector3 direction, Vector3 normal)
    {
        return (direction - normal * Vector3.Dot(direction, normal)).normalized;
    }

    private float GetMinDot (int layer)
    {
        return (stairsMask & (1 << layer)) == 0 ? minGroundDotProduct : minStairsDotProduct;
    }

    private void AdjustVelocity()
    {
        if (!manualInput)
        {
            velocity = desiredVelocity;
            return;
        }
        
        Vector3 xAxis = ProjectDirectionOnPlane(rightAxis, contactNormal);
        Vector3 zAxis = ProjectDirectionOnPlane(forwardAxis, contactNormal);

        float currentX = Vector3.Dot(velocity, xAxis);
        float currentZ = Vector3.Dot(velocity, zAxis);

        acceleration = OnGround ? maxAcceleration : maxAirAcceleration;
        maxSpeedChange = acceleration * Time.deltaTime;

        float newX = Mathf.MoveTowards(currentX, desiredVelocity.x, maxSpeedChange);
        float newZ = Mathf.MoveTowards(currentZ, desiredVelocity.z, maxSpeedChange);

        velocity += xAxis * (newX - currentX) + zAxis * (newZ - currentZ);
    }
}
