﻿using System;
using System.Collections;
using System.Collections.Generic;
using QFSW.QC;
using UnityEngine;

[CommandPrefix("ClosestPoint."), ExecuteInEditMode]
public class ClosestPointDetector : MonoBehaviour
{
    public LayerMask layers;
    public List<Collider> closeObjects = new List<Collider>();
    public Transform debugObject;
    public Transform samplePoint;
    private QuerryManager qm;
    
    public bool evaluate = true;
    private void Start()
    {
        qm = FindObjectOfType<QuerryManager>();
    }

    private void Update()
    {
        if(!evaluate) return;
        if(qm == null)
            qm = FindObjectOfType<QuerryManager>();
        
        bool inArea = qm.GetClosestSurfaceDev(samplePoint.position, out Vector3 surfacePos);
        
        if (inArea)
            debugObject.transform.localScale = Vector3.one * 0.1f;
        else
            debugObject.transform.localScale = Vector3.one;
            
        debugObject.transform.position = surfacePos;
    }
    
    [Command("ToggleEvaluation", "Toggle evaluate value for the camera", MonoTargetType.All)]
    public void ToggleEvaluate()
    {
        evaluate = !evaluate;
        Debug.Log(evaluate);
    }
}
