using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratorDisableComponent : MonoBehaviour
{
    void Update()
    {
        gameObject.SetActive(false);
        Destroy(this);
    }
}
