using UnityEngine;
using UnityEngine.Assertions;

public class GeneratorStageSwapper : MonoBehaviour
{

    [SerializeField] private GameObject generatorGO1;
    [SerializeField] private GameObject generatorGO2;

    [SerializeField] private GravityGenerator _gravityGenerator;

    private void Start()
    {
        _gravityGenerator.OnGeneratorSwitch.Subscribe(SwapGenerators);
    }

    private void SwapGenerators()
    {
        if (generatorGO1 != null & generatorGO2 != null)
        {
            generatorGO1.SetActive(!generatorGO1.activeSelf);
            generatorGO2.SetActive(!generatorGO2.activeSelf);
        }
    }

    private void OnDestroy()
    {
        _gravityGenerator.OnGeneratorSwitch.UnSubscribe(SwapGenerators);
    }
}
