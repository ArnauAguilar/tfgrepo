﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RigidBodyForCustomGravity : MonoBehaviour
{
    private Rigidbody body;

    [SerializeField] private bool floatToSleep = false;
    private float floatDelay;
    
    private void Awake()
    {
        body = GetComponent<Rigidbody>();
        body.useGravity = false;
    }

    private void FixedUpdate()
    {
        if (floatToSleep)
        {
            if (body.IsSleeping())
            {
                floatDelay = 0f;
                return;
            }

            if (body.velocity.sqrMagnitude < 0.0001f)
            {
                floatDelay += Time.deltaTime;
                if (floatDelay >= 1.0f)
                {
                    return;
                }
            }
            else
            {
                floatDelay = 0f;
            } 
        }

        body.AddForce(CustomGravity.GetGravity(body.position) * body.mass, ForceMode.Acceleration);
    }
}
