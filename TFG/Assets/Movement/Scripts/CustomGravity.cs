﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public static class CustomGravity
{
    private static List<GravitySource> sources = new List<GravitySource>();
    private static List<GravitySource> lastResourceSources = new List<GravitySource>();
    public static GravityPlaneGlobal globalGravity;
    public static float minimumGravityMagnitude = 0.01f;

    public static Vector3 GetGravity(Vector3 position)
    {
        Vector3 gravity = sources.Aggregate(Vector3.zero, 
            (current, gravitySource) => current + gravitySource.GetGravity(position));

        if (GravityTooSmall(gravity) && lastResourceSources.Count > 0)
        {
            gravity = lastResourceSources.Aggregate(Vector3.zero,
                (current, gravitySource) => current + gravitySource.GetGravity(position));
        }
        
        if (GravityTooSmall(gravity) && globalGravity) return globalGravity.GetGravity(position);
        
        return gravity;
    }
    
    public static Vector3 GetGravity(Vector3 position, out Vector3 upAxis)
    {
        Vector3 gravity = GetGravity(position);
        upAxis = -gravity.normalized;
        return gravity;
    }

    public static Vector3 GetGravityOfSingleGravitySource(Vector3 position, out Vector3 upAxis,
        GravitySource gravitySource)
    {
        Vector3 gravity = gravitySource.GetGravity(position);
        upAxis = -gravity.normalized;
        return gravity;
    }

    public static Vector3 GetUpAxis(Vector3 position)
    {
        return -GetGravity(position).normalized;
    }

    public static Vector3 GetUpAxisOfSingleGravitySource(Vector3 position, GravitySource gravitySource)
    {
        return -gravitySource.GetGravity(position).normalized;
    }

    private static bool GravityTooSmall(Vector3 gravity)
    {
        return gravity.magnitude < minimumGravityMagnitude;
    }
    
    public static void Register (GravitySource source)
    {
        if(sources.Contains(source))
        {
            Debug.LogError("Duplicate registration of gravity source!");
            return;
        }

        sources.Add(source);
    }

    public static void Unregister(GravitySource source)
    {
        if(!sources.Contains(source))
        {
            Debug.LogError("Unregistration of unknown gravity source!");
            return;
        }
        
        sources.Remove(source);
    }

    public static void RegisterLastResource(GravitySource source)
    {
        if (lastResourceSources.Contains(source))
        {
            Debug.LogError("Duplicate registration of LAST RESOURCE gravity source!");
            return;
        }
        
        lastResourceSources.Add(source);
    }
    
    public static void UnregisterLastResource(GravitySource source)
    {
        if(!lastResourceSources.Contains(source))
        {
            Debug.LogError("Unregistration of unknown LAST RESOURCE gravity source!");
            return;
        }
        
        lastResourceSources.Remove(source);
    }
}