using System;

public class GravityLastResource : GravitySquarePlane
{
    protected override void OnEnable()
    {
        if (!gravityActivated) return;
        
        CustomGravity.RegisterLastResource(this);
    }

    protected override void OnDisable()
    {
        if (!gravityActivated) return;
        
        CustomGravity.UnregisterLastResource(this);
    }

    private void OnDestroy()
    {
        CustomGravity.UnregisterLastResource(this);
    }

    public override void ToggleGravity(bool active)
    {
        gravityActivated = active;

        (gravityActivated ? (Action)ActivateGravity : DeactivateGravity)();
    }
    
    private void ActivateGravity()
    {
        gravityActivated = true;
        CustomGravity.Register(this);
    }

    private void DeactivateGravity()
    {
        gravityActivated = false;
        CustomGravity.Unregister(this);
    }
}