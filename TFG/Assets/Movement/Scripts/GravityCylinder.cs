using UnityEditor;
using UnityEngine;

public class GravityCylinder : GravitySource
{
    [SerializeField] private float gravity = 9.81f;

    [SerializeField, Min(0f)] private float length;
    [SerializeField, Min(0f)] private float radius;

    [SerializeField, Min(0f)] private float outerRadius;
    [SerializeField, Min(0f)] private float outerRadiusFalloff;
    private float _outerRadiusFactor;

    private void Awake()
    {
        OnValidate();
    }

    private void OnValidate()
    {
        outerRadiusFalloff = Mathf.Max(outerRadiusFalloff, outerRadius);
        
        _outerRadiusFactor = 1f / (outerRadiusFalloff - outerRadius);
    }

    public override Vector3 GetGravity(Vector3 position)
    {
        float x = Vector3.Dot(transform.right, position - transform.position);
        if (x < length / 2f && x > -length / 2f)
        {
            Vector3 projectedPoint = transform.position + transform.right * x;
            float distance = Vector3.Distance(projectedPoint, position);
            
            if (distance > radius + outerRadiusFalloff)
            {
                return Vector3.zero;
            }

            float g = gravity;
            
            if (distance < radius + outerRadius)
            {
                g *= -1;
            }
            else
            {
                g *= 1f - distance / (radius + outerRadius);
            }

            return (position - projectedPoint).normalized * g;
        }

        return Vector3.zero;
    }

    private void OnDrawGizmos()
    {
        if (!gravityActivated) return;
        
        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        
        Gizmos.color = Color.red;
        Vector3 endPosition = Vector3.right * length / 2f;

        Gizmos.DrawSphere(endPosition, 0.05f);
        Gizmos.DrawSphere(-endPosition, 0.05f);
        
        DrawCylinderLines(endPosition, radius);

        //Outer Radius
        Gizmos.color = Color.yellow;
        float newOuterRadius = radius + outerRadius;
        if (outerRadius > 0f)
        {
            DrawCylinderLines(endPosition, newOuterRadius);
        }
        
        //Outer Radius Falloff
        float newOuterRadiusFalloff = radius + outerRadiusFalloff;
        Gizmos.color = Color.cyan;
        if (outerRadiusFalloff > outerRadius)
        {
            DrawCylinderLines(endPosition, newOuterRadiusFalloff);
        }


#if UNITY_EDITOR
        Handles.matrix = Gizmos.matrix;
        
        Handles.color = Color.red;
        DrawCylinderWireDiscs(radius);
        
        //Outer Radius
        Handles.color = Color.yellow;
        if (outerRadius > 0f)
        {
            DrawCylinderWireDiscs(newOuterRadius);
        }
        
        //Outer Radius Falloff
        Handles.color = Color.cyan;
        if (outerRadiusFalloff > outerRadius)
        {
            DrawCylinderWireDiscs(newOuterRadiusFalloff);
        }
#endif
    }

    private static void DrawCylinderLines(Vector3 endPosition, float r)
    {
        Gizmos.DrawLine(-endPosition + Vector3.up * r, endPosition + Vector3.up * r);
        Gizmos.DrawLine(-endPosition - Vector3.up * r, endPosition - Vector3.up * r);
        Gizmos.DrawLine(-endPosition + Vector3.forward * r, endPosition + Vector3.forward * r);
        Gizmos.DrawLine(-endPosition - Vector3.forward * r, endPosition - Vector3.forward * r);
    }
    
    private void DrawCylinderWireDiscs(float r)
    {
#if UNITY_EDITOR
        Handles.DrawWireDisc(Vector3.zero, Vector3.right, r);
        Handles.DrawWireDisc(Vector3.right * length / 2f, Vector3.right, r);
        Handles.DrawWireDisc(-Vector3.right * length / 2f, Vector3.right, r);
#endif
    }
}
