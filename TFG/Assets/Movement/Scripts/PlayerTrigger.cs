﻿
using UnityEngine;

public class PlayerTrigger : MonoBehaviour
{
    public Signal onPlayerEnter;
    public Signal onPlayerExit;

    private GameObject _player;
    public GameObject Player => _player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _player = other.gameObject;
            onPlayerEnter.Dispatch();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Player")) onPlayerExit.Dispatch();
    }
}
