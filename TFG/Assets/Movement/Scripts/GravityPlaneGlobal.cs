﻿using UnityEngine;

public class GravityPlaneGlobal : GravitySource
{
    [SerializeField] private float gravity = 9.81f;

    private new void OnEnable()
    {
        if (!gravityActivated) return;

        CustomGravity.globalGravity = this;
    }

    private new void OnDisable()
    {
        if (gravityActivated) return;

        CustomGravity.globalGravity = null;
    }

    public override void ToggleGravity(bool active)
    {
        gravityActivated = active;

        CustomGravity.globalGravity = gravityActivated ? this : null;
    }

    public override Vector3 GetGravity(Vector3 position)
    {
        return transform.up * gravity;
    }

    private void OnDrawGizmos()
    {
        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);

        Gizmos.color = Color.yellow;
        Gizmos.DrawCube(Vector3.up * 5, new Vector3(2, 10, 2));
        Gizmos.DrawSphere(Vector3.zero, 2);
        Gizmos.DrawWireCube(Vector3.zero, new Vector3(100, 0, 100));
    }
}