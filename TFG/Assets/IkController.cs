﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using Alone.Menu;
using Mono.CSharp;
using UnityEditor;
using UnityEngine;
using Enum = System.Enum;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;
using Vector4 = UnityEngine.Vector4;

public class IkController : MonoBehaviour
{
    public Transform player;
    public float playerOffset;
    [Header("Leg Movement")]
    [Tooltip("1 Forward Left, 2 Forward Right, 3 Back Left, 4 Back Right")]
    public Transform[] legsTargets;
    [Tooltip("1 Forward Left, 2 Forward Right, 3 Back Left, 4 Back Right")]
    public Transform[] restingPositions;
    [Tooltip("1 Forward Left, 2 Forward Right, 3 Back Left, 4 Back Right")]
    public Transform[] polePositions;

    public Vector4[] movementConstraints;

    public float stepMaxDistance = 2;
    [Tooltip("How much forward you move the feet from the restingPosition when it´s moving (1 to go straight to the other side)")]
    public float movingStepMaxAdvancementRate = 1;
    public float maxErrorTolerance = 2;
    public float stepMaxHeight = 2;

    public AnimationCurve legMovementHeightCurve;
    public AnimationCurve legMovementAdvancementCurve;
    public float stepRelativeSpeed;
    public LegMovement[] movingLegs;
    public LayerMask interactionLayers;


    public Vector3 upDirection = Vector3.up;

    private Vector3 lastPosition;

    public Vector3 Velocity { get => (translationTransform.position - lastPosition)/Time.deltaTime;}

    private float StepSpeed { get => Mathf.Max(stepRelativeSpeed, stepRelativeSpeed + Velocity.magnitude);}
    private bool OnAir { get => !playerDriver.OnGround && !playerDriver.OnSteep;}
    private float lastStepSpeed;

    [Header("Rotation Params")]
    public Transform rotationTransform;
    private Transform headTransform;
    public Transform restingPositionsParent;
    public float maxRadiansDelta = 1;
    public float speedMaxRadiansDelta = 1;
    public float speedMaxRadiansDeltaThreshold = 1;
    public float maxRadiansDeltaForFace = 1;
    public float maxDegreesFromUP = 45;


    public int checkLegsFrameInterval = 15;
    private int lastCheck = 0;

    private QuerryManager environmentQuerryManager;

    private Transform translationTransform;
    private Transform cameraTransform;

    [Header("Head Movement")] 
    [Tooltip("X min Y max")]
    public Vector2 headUpAndDownMovementMultiplier;
    public float minimumMovement = 0.1f;
    public bool inverseToSpeed;

    [Header("Jump Movement")] 
    public float legReturningSpeed = 2;
    public float frontLegReturningSpeed = 8;
    public float legPreparingForImpactSpeed = 10;
    public float startRetractingLegsTime = 0.5f;
    public float fallStateTolerance = 0.5f;
    public Vector2 fallStateVelocityAndDownWheight = Vector2.one/2.0f;
    public float bodyRetractmentOnJump = 0.5f;


    private MovingSphere playerDriver;
    public enum Status {READY, MOVING, DONE};
    [Serializable]
    public struct LegMovement
    {
        public float movingTime;
        public Status legStatus;
        public Vector3 movementStart;
        public Vector3 movementEnd;
    }

    [Header("Footsteps")] 
    [SerializeField] private AudioClip[] footSteps;
    [SerializeField] private AudioSource[] footAudioSources;
    [SerializeField] private Vector2 minMaxVolume;
    [SerializeField] private Vector2 minMaxPitch;
    
    void Start()
    {
        translationTransform = this.transform;
        if (Camera.main is { }) cameraTransform = Camera.main.transform;
        movingLegs = new LegMovement[legsTargets.Length];
        for (int i = 0; i < legsTargets.Length; i++)
        {
            movingLegs[i] = new LegMovement()
            {
                movingTime = stepRelativeSpeed + 1,
                movementEnd = Vector3.zero,
                movementStart = Vector3.zero,
                legStatus = ((i == 0 || i == 3) ? Status.READY : Status.DONE)
            };
        }

        environmentQuerryManager = FindObjectOfType<QuerryManager>();
        playerDriver = player.GetComponent<MovingSphere>();
    }

    void Update()
    {
        if (PauseMenu.IsPaused()) return;
        
        upDirection = CustomGravity.GetUpAxis(player.position);
        
        SetPositionAndRotationToPlayer();
        if (!OnAir)
            MoveLegs();
        else
            MoveLegsAir();
        
        if (lastCheck == checkLegsFrameInterval)
        {
            CheckAllMovingLegsTargets();
            lastCheck = 0;
        }

        lastCheck++;

        lastPosition = translationTransform.position;
    }

    private float onAirTime = 0;
    private void MoveLegsAir()
    {
        if (onAirTime <= startRetractingLegsTime)
        {
            onAirTime += Time.deltaTime;
            legsTargets[2].position = Vector3.MoveTowards(legsTargets[2].position, restingPositions[2].position + upDirection * (bodyRetractmentOnJump * playerOffset), Time.deltaTime * frontLegReturningSpeed);
            legsTargets[3].position = Vector3.MoveTowards(legsTargets[3].position, restingPositions[3].position + upDirection * (bodyRetractmentOnJump * playerOffset), Time.deltaTime * frontLegReturningSpeed);
            return;
        }
        if(Vector3.Dot(-upDirection, playerDriver.velocity)>=fallStateTolerance)
        {
            for (int i = 0; i < legsTargets.Length; i++)
            {
                if(i<2)
                    legsTargets[i].position = Vector3.MoveTowards(legsTargets[i].position, restingPositions[i].position + upDirection * (bodyRetractmentOnJump * playerOffset), Time.deltaTime * legPreparingForImpactSpeed);
                else
                    legsTargets[i].position = Vector3.MoveTowards(legsTargets[i].position,GetEndPositionFallingAir(i), Time.deltaTime * legPreparingForImpactSpeed);
            }
        }
        else
        {
            for (int i = 0; i < legsTargets.Length; i++)
            {
                if(i<2)
                    legsTargets[i].position = Vector3.MoveTowards(legsTargets[i].position, restingPositions[i].position, Time.deltaTime * legReturningSpeed);
                else
                    legsTargets[i].position = Vector3.MoveTowards(legsTargets[i].position, restingPositions[i].position + upDirection * (bodyRetractmentOnJump * playerOffset), Time.deltaTime * frontLegReturningSpeed);
            }
        }
    }

    private int lastMovedLeg = -1;
    private List<int> updateOrder = new List<int>(new []{0, 1, 2, 3});
    private int toFix = -1;
    private void MoveLegs()
    {
        onAirTime = 0;
        toFix = -1;
        for(int i = 0; i<legsTargets.Length; i++)
        {
            int index = updateOrder[i];
            if (MoveLeg(index))
            {
                toFix = index;

            }
        }

        if (toFix >= 0)//reorder the legs execution if needed
        {
            GetNonConstrainedLegs(toFix, out List<int> friendLegs);
            foreach (int friend in friendLegs)
                updateOrder.Remove(friend);
                
            foreach (int friend in friendLegs)
                updateOrder.Add(friend);
        }
        
        lastStepSpeed = FixStepSpeed();
    }

    private bool MoveLeg(int i)
    {
        if (movingLegs[i].legStatus == Status.MOVING)
        {
            float sample = Mathf.Min(movingLegs[i].movingTime * FixStepSpeed(), 1);

            float height = legMovementHeightCurve.Evaluate(sample) * stepMaxHeight;
            float forwardEvaluate = legMovementAdvancementCurve.Evaluate(sample);

            legsTargets[i].position = Vector3.Lerp( movingLegs[i].movementStart + translationTransform.position, movingLegs[i].movementEnd + translationTransform.position, forwardEvaluate) + upDirection * height;
            //legsTargets[i].position += transform.position;


            if (sample >= 1)
            {
                movingLegs[i].legStatus = Status.DONE;

                PlayRandomFootStep(i);
                
                //Check friend
                GetNonConstrainedLegs(i, out List<int> friendLegs);
                if (CheckStatus(friendLegs, Status.DONE))
                {
                    for( int index = 0; index < movingLegs.Length; index++)
                    {
                        if (!friendLegs.Contains(index)) movingLegs[index].legStatus = Status.READY;
                    }
                }
                return true;
            }
            movingLegs[i].movingTime += Time.deltaTime;
        }
        else if (movingLegs[i].legStatus == Status.READY)
        {

            Vector3 separation = restingPositions[i].position - legsTargets[i].position - Vector3.Project(restingPositions[i].position - legsTargets[i].position, upDirection);
            if ((separation.magnitude > stepMaxDistance / 2f && CanMoveLeg(i)) || separation.magnitude > maxErrorTolerance) //&& not the last that has been moved)
            {
                movingLegs[i].movingTime = 0;
                movingLegs[i].legStatus = Status.MOVING;
                movingLegs[i].movementStart = legsTargets[i].position - translationTransform.position;
                movingLegs[i].movementEnd = GetEndPosition(i) - translationTransform.position;
            }
        }

        return false;
    }

    public bool CheckStatus(List<int>friendLegs, Status status)
    {
        foreach (int legIndex in friendLegs)
        {
            if (movingLegs[legIndex].legStatus != status) return false;
        }

        return true;
    }

    void CheckAllMovingLegsTargets()
    {
        for (int i = 0; i < movingLegs.Length; i++)
        {
            if (movingLegs[i].legStatus != Status.MOVING) continue;
            Color c = Color.black;
            if (i == 0) c= Color.red;
            if (i == 1) c= Color.green;
            if (i == 2) c= Color.blue;
            if (i == 3) c= Color.yellow;
            //if (debug) Debug.DrawRay(polePositions[i].position, (movingLegs[i].movementEnd+ transform.position) - polePositions[i].position, c, 2);
            if (Physics.Raycast(polePositions[i].position,  (movingLegs[i].movementEnd+ translationTransform.position) - polePositions[i].position, out hit, ((movingLegs[i].movementEnd+ translationTransform.position) - polePositions[i].position).magnitude, interactionLayers))
            {
                movingLegs[i].movementEnd = hit.point - translationTransform.position;
            }
        }
    }
    
    private float fixStepSpeedDelta = 0.1f;
    float FixStepSpeed()
    {
        float dif = StepSpeed - lastStepSpeed;
        if (Math.Abs(dif) > fixStepSpeedDelta) return lastStepSpeed + Mathf.Sign(dif) * fixStepSpeedDelta;
        else return StepSpeed;
    }

    RaycastHit hit;
    Vector3 GetEndPosition(int i)
    {
        //Vector3 virtualEndPos = (restingPositions[i].position + (restingPositions[i].position - legsTargets[i].position).normalized * ((stepMaxDistance / 2.0f) * playerDriver.velocity.magnitude.Remap(0, playerDriver.maxSpeed, 0, movingStepMaxAdvancementRate)));
        Vector3 virtualEndPos = (restingPositions[i].position + playerDriver.velocity.normalized * ((stepMaxDistance / 2.0f) * playerDriver.velocity.magnitude.Remap(0, playerDriver.maxSpeed, 0, movingStepMaxAdvancementRate)));
        if (environmentQuerryManager.GetClosestSurface(virtualEndPos, out Vector3 surfacePoint))
        {
            //if (debug) Debug.DrawRay(polePositions[i].position, surfacePoint - polePositions[i].position, Color.cyan, 2);
            if (Physics.Raycast(polePositions[i].position,  surfacePoint - polePositions[i].position, out hit, (surfacePoint - polePositions[i].position).magnitude, interactionLayers))
            {
                return hit.point;
            }

            return surfacePoint;
        }
        else
            return  virtualEndPos;
    }
    
    Vector3 GetEndPositionFallingAir(int i)
    {
        Vector3 virtualEndPos = restingPositions[i].position + (playerDriver.velocity.normalized * fallStateVelocityAndDownWheight.x + upDirection * -fallStateVelocityAndDownWheight.y);
        if (environmentQuerryManager.GetClosestSurface(virtualEndPos, out Vector3 surfacePoint))
        {
            //if (debug) Debug.DrawRay(polePositions[i].position, surfacePoint - polePositions[i].position, Color.cyan, 2);
            if (Physics.Raycast(polePositions[i].position,  surfacePoint - polePositions[i].position, out hit, (surfacePoint - polePositions[i].position).magnitude, interactionLayers))
            {
                return hit.point;
            }

            return surfacePoint;
        }
        else
            return  virtualEndPos;
    }

    bool CanMoveLeg(int i)
    {
        Vector4 constraints = movementConstraints[i];
        if (constraints.x > 0)
        {
            if (movingLegs[0].legStatus != Status.DONE) return false;
        }
        if (constraints.y > 0)
        {
            if (movingLegs[1].legStatus != Status.DONE) return false;
        }
        if (constraints.z > 0)
        {
            if (movingLegs[2].legStatus != Status.DONE) return false;
        }
        if (constraints.w > 0)
        {
            if (movingLegs[3].legStatus != Status.DONE) return false;
        }
        return true;
    }
    
    void GetNonConstrainedLegs(int i, out List<int>friendLegs)
    {
        Vector4 constraints = movementConstraints[i];
        friendLegs = new List<int>();
        if (constraints.x == 0)
        {
            friendLegs.Add(0);
        }
        if (constraints.y == 0)
        {
            friendLegs.Add(1);
        }
        if (constraints.z == 0)
        {
            friendLegs.Add(2);
        }
        if (constraints.w == 0)
        {
            friendLegs.Add(3);
        }
    }

    float GetUpAndDownOffset()
    {
        float offset = 0;
        for (int i = 0; i < legsTargets.Length; i++)
        {
            offset += Vector3.Project(restingPositions[i].position - legsTargets[i].position, upDirection).magnitude;
        }

        return offset/legsTargets.Length;
    }
    
    Vector3 ComputeTranslationOffset()
    {
        if (OnAir)
        {
            return Vector3.zero;
        }
        
        float speed01 = Mathf.Clamp01(playerDriver.velocity.magnitude / playerDriver.maxSpeed);
        return upDirection * playerOffset + upDirection * 
        (playerOffset * 
            (GetUpAndDownOffset().Remap(0, stepMaxHeight,headUpAndDownMovementMultiplier.x, headUpAndDownMovementMultiplier.y) * 
                (inverseToSpeed? 
                    Mathf.Abs(1-speed01).Remap(0,1, minimumMovement,1): 
                    speed01.Remap(0,1, minimumMovement,1)
                )
            )
        );
    }

    Vector3 oldCorrectForward = Vector3.zero;
    Vector3 correctForward = Vector3.zero;
    Vector3 oldCorrectUp = Vector3.zero;
    Vector3 correctUp = Vector3.zero;

    void SetPositionAndRotationToPlayer()
    {
        translationTransform.position = player.position + ComputeTranslationOffset();
        Debug.DrawRay(player.position + upDirection * playerOffset,upDirection * playerOffset, Color.blue);
        if (oldCorrectForward == Vector3.zero)
        {
            oldCorrectForward = Vector3.ProjectOnPlane(translationTransform.forward, upDirection).normalized;
            oldCorrectUp = rotationTransform.up;
        }
        CrossLegs(0,1,2, out Vector3 up1);
        CrossLegs(3,2,1, out Vector3 up2);
        CrossLegs(1,3,0, out Vector3 up3);
        CrossLegs(2,0,3, out Vector3 up4);

        if (Vector3.Dot(up1, upDirection) < 0) up1 *= -1;
        if (Vector3.Dot(up2, upDirection) < 0) up2 *= -1;
        if (Vector3.Dot(up3, upDirection) < 0) up3 *= -1;
        if (Vector3.Dot(up4, upDirection) < 0) up4 *= -1;
        
        
        Vector3 inclination = (up1 + up2 + up3 + up4) / 4.0f;
        float inclinationAngle = Vector3.Angle(upDirection, inclination);
        float upsInclinationAngle = Vector3.Angle(upDirection, rotationTransform.up);
        float radians = maxRadiansDelta;
        Vector3 target = inclination;
        
        if (upsInclinationAngle >= speedMaxRadiansDeltaThreshold)
        {
            radians = speedMaxRadiansDelta;
            target = upDirection;
        }
        else
        {
            if (inclinationAngle >= maxDegreesFromUP)
            {
                inclination = Vector3.RotateTowards(upDirection, inclination, maxDegreesFromUP * Mathf.Deg2Rad, 0).normalized;
            } 
            target = inclination;
        }
        
        correctUp = Vector3.RotateTowards(oldCorrectUp, target, radians* Time.deltaTime, 0);
        
        correctForward =  Vector3.ProjectOnPlane(playerDriver.velocity, upDirection);

        Debug.DrawRay(translationTransform.position, playerDriver.velocity, Color.magenta);
        Debug.DrawRay(translationTransform.position, correctForward, Color.yellow);
        if (correctForward.magnitude <= 0.01f)
        {
            correctForward = oldCorrectForward;
            Debug.DrawRay(translationTransform.position, correctForward, Color.cyan);
        }
        else
        {
            correctForward = Vector3.ProjectOnPlane(Vector3.RotateTowards(oldCorrectForward, correctForward.normalized, maxRadiansDeltaForFace, 0), correctUp);
            Debug.DrawRay(translationTransform.position, correctForward, Color.green);
        }

        
        rotationTransform.rotation = Quaternion.LookRotation(correctForward, correctUp);

        oldCorrectForward = correctForward;
        oldCorrectUp = correctUp;
        
        
        restingPositionsParent.position = translationTransform.position - upDirection * playerOffset/2.0f;
        restingPositionsParent.transform.rotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(oldCorrectForward, upDirection).normalized, upDirection);
    }

    void CrossLegs(int i, int j, int k, out Vector3 crossVector)
    {
        crossVector = 
            Vector3.Cross(
            (movingLegs[j].legStatus == Status.MOVING ? movingLegs[j].movementEnd + translationTransform.position : legsTargets[j].position) -
                (movingLegs[i].legStatus == Status.MOVING ? movingLegs[i].movementEnd + translationTransform.position : legsTargets[i].position),
            
            (movingLegs[k].legStatus == Status.MOVING ? movingLegs[k].movementEnd + translationTransform.position : legsTargets[k].position) -
                (movingLegs[i].legStatus == Status.MOVING ? movingLegs[i].movementEnd + translationTransform.position : legsTargets[i].position)
            ).normalized;
    }

    private void PlayRandomFootStep(int legIndex)
    {
        if (legIndex == 1 || legIndex == 0) return;
        
        footAudioSources[legIndex].volume = Random.Range(minMaxVolume.x, minMaxVolume.y);
        footAudioSources[legIndex].pitch = Random.Range(minMaxPitch.x, minMaxPitch.y);
        footAudioSources[legIndex].clip = footSteps[Random.Range(0, footSteps.Length)];
        
        footAudioSources[legIndex].Play();
    }
}
;