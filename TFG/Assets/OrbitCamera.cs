﻿using System;
using Alone.Menu;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using formulas;

[RequireComponent(typeof(Camera))]
public class OrbitCamera : MonoBehaviour
{
    private Camera _regularCamera;
    
    [Title("Camera focus")]
    [SerializeField] private Transform focus;
    [SerializeField] private MovingSphere movingSphere;
    
    [Title("Distance")]
    [SerializeField] private AnimationCurve distanceFromPivot;
    [SerializeField, Range(0f, 2f)] private float distanceMultiplier = 1f;
    [SerializeField] private AnimationCurve focusHeightOffset;
    private float _currentHeightOffset;
    private float _currentDistance;
    
    [Title("Field of View")]
    [SerializeField] private AnimationCurve fieldOfView;
    
    [Title("Smoothness")]
    [SerializeField, Min(0f)] private float focusRadius = 1f;
    [SerializeField, Range(0f, 1f)] private float focusCentering = 0.5f;
    [SerializeField] private float focusSpeed = 10.0f;
    [SerializeField, Min(0f)] private float upAlignmentSpeedOnGround = 360f;
    [SerializeField, Min(0f)] private float upAlignmentSpeedOnAir = 360f;
    [SerializeField, Min(0f)] private float upAlignmentSpeedSmooth = 15f;
    private float _upAlignmentSpeed = 360f;
    private Vector3 _focusPoint;

    [Title("Rotation")] 
    [SerializeField] private bool useRightHandedScheme;
    public bool UseRightHandedScheme { set => useRightHandedScheme = value; }
    
    [SerializeField, Range(-89f, 89f)] private float minVerticalAngle = -30f;
    [SerializeField, Range(-89f, 89f)] private float maxVerticalAngle = 60f;
    [Space]
    [SerializeField, Range(1f, 1000f)] private float keyboardRotationSpeed = 90f;
    public float KeyboardRotationSpeed { get => keyboardRotationSpeed; set => keyboardRotationSpeed = value; }
    
    [SerializeField, Range(1f, 1000f)] private float gamepadRotationSpeed = 90f;
    public float GamepadRotationSpeed { get => gamepadRotationSpeed; set => gamepadRotationSpeed = value; }
    private float rotationSpeed;
    [SerializeField] private bool useMouseAcceleration;
    [SerializeField] public bool UseMouseAcceleration { set => useMouseAcceleration = value; }
    private bool invertedYAxisMouse;
    public bool InvertedYAxisMouse { set => invertedYAxisMouse = value; }
    [SerializeField] private bool invertedYAxisGamepad;
    public bool InvertedYAxisGamepad { set => invertedYAxisGamepad = value; }
    [Space]
    [SerializeField, ReadOnly] private Vector2 orbitAngles;
    public Vector2 OrbitAngles => orbitAngles;

    private Quaternion _gravityAlignment = Quaternion.identity;
    private Quaternion _orbitRotation;

    [Title("Obstruction")] 
    [SerializeField] private bool cameraCanCollide = true;
    [SerializeField] private LayerMask obstructionMask = -1;
    [SerializeField, Range(0.0f, 1.0f)] private float obstructionSecurityDistance = 0.2f;
    [SerializeField] private bool makeCollisionsSmoother = true;
    [SerializeField] private float desiredCollisionInSpeed = 2;
    [SerializeField] private float desiredCollisionOutSpeed = 2;
    [SerializeField] private float maxCollisionInSpeed = 18;
    [SerializeField] private float maxCollisionOutSpeed = 15;
    [SerializeField] private float cameraExtendedMultiplier = 2;
    [SerializeField] private Vector3 securityAreaSize = new Vector3(1,1, 0);
    [SerializeField]private Vector3 defaultLocalPassDirection = Vector3.up;
    private Vector3 _lastGlobalDirection;
    private bool _isColliding;
    [SerializeField] private bool debug = false;

    [Title("Initial Position and Rotation")]
    [SerializeField] private Vector2 initialRotation;

    [Title("Animation Settings")]
    [SerializeField] private Vector2 desiredOrbitAngles;
    public Vector2 DesiredOrbitAngles { set => desiredOrbitAngles = value; }

    [SerializeField] private GravitySource desiredGravitySource;
    public GravitySource DesiredGravitySource { set => desiredGravitySource = value; }
    
    [SerializeField] private float animationRotationSpeed;
    public float AnimationRotationSpeed => animationRotationSpeed;

    private ScriptedAnimations _cameraAnimation;
    public ScriptedAnimations CameraAnimation { set => _cameraAnimation = value; }
    private const float e = 0.001f;

    private Vector3 CameraHalfExtends
    {
        get
        {
            Vector3 halfExtends;
            halfExtends.y = _regularCamera.nearClipPlane * Mathf.Tan(0.5f * Mathf.Deg2Rad * _regularCamera.fieldOfView);
            halfExtends.x = halfExtends.y * _regularCamera.aspect;
            halfExtends.z = 0.01f;
            return halfExtends * cameraExtendedMultiplier;
        }
    }
    
    private void OnValidate()
    {
        if (maxVerticalAngle < minVerticalAngle)
        {
            maxVerticalAngle = minVerticalAngle;
        }
    }
    private Vector3 _globalDirection;
    private Vector3 _lastGlobalPos;
    private void Start()
    {
        _regularCamera = GetComponent<Camera>();

        UpdateGravityAlignment(true);

        orbitAngles = initialRotation;
        
        _currentHeightOffset = focusHeightOffset.Evaluate(orbitAngles.x);
        _focusPoint = focus.position + CustomGravity.GetUpAxis(focus.position) * _currentHeightOffset;
        UpdateFocusPoint();

        _lastGlobalDirection = transform.TransformDirection(defaultLocalPassDirection);
        transform.localRotation = _orbitRotation = Quaternion.Euler(orbitAngles);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        _cameraAnimation = ScriptedAnimations.Stop;
        _lastGlobalPos = transform.position;

        AloneSettings aloneSettings = GameManager.Instance.settings;
        keyboardRotationSpeed = aloneSettings.keyboardRotationSpeed;
        invertedYAxisMouse = aloneSettings.invertMouse;
        useMouseAcceleration = aloneSettings.mouseAcceleration;

        useRightHandedScheme = aloneSettings.useRightHandedStickMoveLook;
        gamepadRotationSpeed = aloneSettings.gamepadRotationSpeed;
        invertedYAxisGamepad = aloneSettings.invertLook;
    }

    private Vector3 _lockPosition;
    private bool _locked;
    public void LookCameraPosition()
    {
        _lockPosition = transform.position;
        _locked = true;
    }

    public void UnlockCameraPosition()
    {
        _locked = false;
        _lockedRotationOffset = Quaternion.identity;
    }
    
    

    private void LateUpdate()
    {
        if (PauseMenu.IsPaused()) return;

        
        UpdateGravityAlignment(false, desiredGravitySource);

        if (_cameraAnimation == ScriptedAnimations.Playing || _cameraAnimation == ScriptedAnimations.ForceCameraUpdate)
        {
            if (_cameraAnimation == ScriptedAnimations.ForceCameraUpdate)
            {
                UpdateFocusPoint();
            }
        
            ConstrainAngles();
            UpdateFieldOfView();
            _orbitRotation = Quaternion.Euler(desiredOrbitAngles);
            orbitAngles = desiredOrbitAngles;
        }
        else
        {
            UpdateFocusPoint();
            
            if (ManualRotation())
            {
                ConstrainAngles();
                UpdateFieldOfView();
                _orbitRotation = Quaternion.Euler(orbitAngles);   
            }
        }
        

        _isColliding = CheckCollision(out Quaternion lookRotation, out Vector3 lookPosition);
        transform.SetPositionAndRotation(lookPosition, lookRotation);
        _lastGlobalDirection = _globalDirection;

        _cameraSpeed = ((_focusPoint - _lookDirection * _currentDistance) - _lastGlobalPos).magnitude;
        _globalDirection = CMath.NormalizeThreshold(((_focusPoint - _lookDirection * _currentDistance) - _lastGlobalPos), 0.005f);
        _lastGlobalPos = _focusPoint - _lookDirection * _currentDistance;

        
    }

    private float _cameraSpeed;
    

    private void UpdateGravityAlignment(bool force = false, bool useDesiredGravitySource = false)
    {
        Vector3 fromUp = _gravityAlignment * Vector3.up;
        Vector3 toUp = useDesiredGravitySource
            ? CustomGravity.GetUpAxisOfSingleGravitySource(focus.position, desiredGravitySource)
            : CustomGravity.GetUpAxis(focus.position);
        
        float rotationAngles = Vector3.Angle(fromUp, toUp);
        Vector3 modifiedUp = toUp;
        if (Mathf.Abs(rotationAngles) > 170)
        {
            modifiedUp = Quaternion.AngleAxis(rotationAngles/2f, transform.forward) * toUp;
        }
       
        float dot = Mathf.Clamp(Vector3.Dot(fromUp, toUp), -1f, 1f);
        float angle = Mathf.Acos(dot) * Mathf.Rad2Deg;
        
        
        
        _upAlignmentSpeed = Mathf.Lerp(_upAlignmentSpeed,
            movingSphere.OnGround ? upAlignmentSpeedOnGround : upAlignmentSpeedOnAir,
            Time.unscaledDeltaTime * upAlignmentSpeedSmooth);

        float maxAngle = _upAlignmentSpeed * Time.deltaTime;

        
        
        Vector3 rotationAxy = transform.forward;
        
        Quaternion newAlignment = Quaternion.FromToRotation(fromUp, modifiedUp) * _gravityAlignment;
        
        if (_locked)
        {
            _lockedRotationOffset = Quaternion.Inverse(newAlignment);
        }
        else
        {
            if (angle <= maxAngle || force)
            {
                _gravityAlignment = newAlignment;
            }
            else
            {
                _gravityAlignment = Quaternion.SlerpUnclamped(_gravityAlignment, newAlignment, maxAngle / angle);
            }   
        }
    }

    private Quaternion _lockedRotationOffset;
    private void UpdateFocusPoint()
    {
        _currentHeightOffset = focusHeightOffset.Evaluate(orbitAngles.x);
        Vector3 targetPoint = Vector3.LerpUnclamped(_focusPoint, 
            focus.position + CustomGravity.GetUpAxis(focus.position) * _currentHeightOffset, 
            Time.unscaledDeltaTime * focusSpeed);

        if (focusRadius > 0f)
        {
            float dis = Vector3.Distance(targetPoint, _focusPoint);
            float t = 1f;
            if (dis > 0.01f && focusCentering > 0f)
            {
                t = Mathf.Pow(1f - focusCentering, Time.unscaledDeltaTime);
            }
            if (dis > focusRadius)
            {
                t = Mathf.Min(t,focusRadius / dis);
            }
            _focusPoint = Vector3.Lerp(_focusPoint, targetPoint, t);
        }
        else
        {
            _focusPoint = targetPoint;
        }

        if (Physics.Raycast(focus.position, targetPoint - focus.position, out RaycastHit hit, 
            distanceFromPivot.Evaluate(minVerticalAngle) * 0.9f, obstructionMask))
        {
            _focusPoint = focus.position;
        }
    }

    
    private bool ManualRotation()
    {
        DeviceType currentDeviceType = InputController.DeviceType;

        Vector2 controlInput = currentDeviceType == DeviceType.Keyboard ? InputController.Get2DAxis("Look") :
            InputController.Get2DAxis( useRightHandedScheme ? "Look" : "Move");
        
        bool invertedYAxis = currentDeviceType == DeviceType.PlayStation || currentDeviceType == DeviceType.XBox
            ? invertedYAxisGamepad
            : invertedYAxisMouse;
        Vector2 input = new Vector2((invertedYAxis ? 1 : -1) * controlInput.y, controlInput.x);
            

        if (!(input.x < -e) && !(input.x > e) && !(input.y < -e) && !(input.y > e) && !_locked) return false;

        rotationSpeed = currentDeviceType == DeviceType.PlayStation || currentDeviceType == DeviceType.XBox
            ? gamepadRotationSpeed
            : keyboardRotationSpeed;
        
        orbitAngles += rotationSpeed * Time.unscaledDeltaTime * input;
        
        return true;
    }

    private void ConstrainAngles()
    {
        orbitAngles.x = Mathf.Clamp(orbitAngles.x, minVerticalAngle, maxVerticalAngle);

        if (orbitAngles.y < 0f)
        {
            orbitAngles.y += 360f;
        }
        else if (orbitAngles.y >= 360f)
        {
            orbitAngles.y -= 360f;
        }
    }

    private void UpdateFieldOfView()
    {
        _regularCamera.fieldOfView = fieldOfView.Evaluate(orbitAngles.x);
    }
    
    private Vector3 _lookDirection;
    private bool CheckCollision(out Quaternion lookRotation, out Vector3 lookPosition)
    {
        lookRotation = _gravityAlignment * _orbitRotation;
        _lookDirection = lookRotation * Vector3.forward;
        _currentDistance = distanceFromPivot.Evaluate(orbitAngles.x) * distanceMultiplier;
        lookPosition = _focusPoint - _lookDirection * _currentDistance;
        Vector3 castFrom = _focusPoint;
        float castDistance = Vector3.Distance(transform.position, _focusPoint);
        Vector3 castDirection = (lookPosition - castFrom).normalized;

        //if no movement, keep last pass direction
        //_globalDirection = _globalDirection == Vector3.zero ? _lastGlobalDirection : _globalDirection;
        
        Vector3 projectedGlobalDirection = Quaternion.Inverse(lookRotation) * _globalDirection;
        Vector3 centerOffset = 
            lookRotation * 
            CMath.MultiplyVectors(new Vector3(projectedGlobalDirection.x, projectedGlobalDirection.y, 0), securityAreaSize / 2f);
        Vector3 center = castFrom + centerOffset;
        Vector3 halfExtends = CameraHalfExtends + CMath.MultiplyVectors(projectedGlobalDirection.Abs(),securityAreaSize)/2f;
        Vector3 localPassDirection = transform.InverseTransformDirection(_globalDirection);

        if (cameraCanCollide)
        {
            RaycastHit debugHitPoint = new RaycastHit();
            float newDistance = Mathf.Infinity;
            
            //Optimal direction
            if(DirectionalBoxCast(center, halfExtends, castDirection,
                out RaycastHit hit, lookRotation, castDistance, obstructionMask,  localPassDirection))
            {
                if (!makeCollisionsSmoother)
                {
                    newDistance = Vector3.Distance(_focusPoint, hit.point) - obstructionSecurityDistance;
                }
                else
                {
                    Vector3 projectedHitPoint = _focusPoint + Vector3.Project(hit.point - _focusPoint, transform.position - _focusPoint);
                    float distanceToProjection = Vector3.Distance(hit.point, projectedHitPoint)  - (CameraHalfExtends.x+CameraHalfExtends.y)/2f;
                    //Debug.Log("Projected Distance: " + distanceToProjection);
                    var collisionInSpeed = Mathf.Abs(distanceToProjection) > 0.4f ? desiredCollisionInSpeed : maxCollisionInSpeed;
                    newDistance = Mathf.Lerp(
                        castDistance, 
                        Vector3.Distance(_focusPoint, hit.point) - obstructionSecurityDistance,
                        Time.unscaledDeltaTime * collisionInSpeed);
                }

                debugHitPoint = hit;
            }
            //Opposite direction
            if(DirectionalBoxCast(center, halfExtends, castDirection,
                out RaycastHit hit2, lookRotation, castDistance, obstructionMask,  -localPassDirection))
            {
                float collisionDistance;
                if (!makeCollisionsSmoother)
                {
                    collisionDistance = Vector3.Distance(_focusPoint, hit2.point) - obstructionSecurityDistance;
                }
                else
                {
                    Vector3 projectedHitPoint = _focusPoint + Vector3.Project(hit2.point - _focusPoint, transform.position - _focusPoint);
                    float distanceToProjection = Vector3.Distance(hit2.point, projectedHitPoint)  - (CameraHalfExtends.x+CameraHalfExtends.y)/2f;
                    //Debug.Log("Projected Distance: " + distanceToProjection);
                    var collisionInSpeed = Mathf.Abs(distanceToProjection) > 0.4f ? desiredCollisionInSpeed : maxCollisionInSpeed;
                    collisionDistance = Mathf.Lerp(
                        castDistance, 
                        Vector3.Distance(_focusPoint, hit2.point) - obstructionSecurityDistance,
                        Mathf.Clamp01(Time.unscaledDeltaTime * collisionInSpeed));
                }

                newDistance = newDistance < collisionDistance ? newDistance : collisionDistance;
                debugHitPoint = newDistance < collisionDistance ? debugHitPoint : hit2;
            }
            //Default pass to prevent obstacle oclusion
            RaycastHit[] hits = Physics.BoxCastAll(castFrom, CameraHalfExtends, castDirection, lookRotation, Mathf.Min(castDistance+0.5f, _currentDistance),
                obstructionMask);
            RaycastHit hit3 = GetClosestPointToTarget(_focusPoint, hits);
            if(hit3.distance != 0)
            {
                float collisionDistance;
                if (!makeCollisionsSmoother)
                {
                    collisionDistance = Vector3.Distance(_focusPoint, hit3.point) - obstructionSecurityDistance;
                }
                else
                {
                    Vector3 projectedHitPoint = _focusPoint + Vector3.Project(hit3.point - _focusPoint, transform.position - _focusPoint);
                    float distanceToProjection = Vector3.Distance(hit3.point, projectedHitPoint) - (CameraHalfExtends.x+CameraHalfExtends.y)/2f;
                    //Debug.Log("Projected Distance: " + distanceToProjection);
                    var collisionInSpeed = Mathf.Abs(distanceToProjection) > 0.4f ? desiredCollisionInSpeed : maxCollisionInSpeed;
                    collisionDistance = Mathf.Lerp(
                        castDistance, 
                        Vector3.Distance(_focusPoint, hit3.point) - obstructionSecurityDistance,
                        Mathf.Clamp01(Time.unscaledDeltaTime * collisionInSpeed));
                }
                
                if (debug)
                {
                    PhysicsDebug.DrawBoxCastBox(castFrom, CameraHalfExtends, lookRotation, castDirection, castDistance, Color.yellow);
                    Debug.DrawLine(castFrom, castFrom + castDirection*castDistance*1.5f, Color.yellow);
                }

                newDistance = newDistance < collisionDistance ? newDistance : collisionDistance;
                debugHitPoint = newDistance < collisionDistance ? debugHitPoint : hit3;
            }
            
            if (!float.IsPositiveInfinity(newDistance))
            {
                //Debug.Log("MODIFIED: "+ CMath.RoundToDecimals(newDistance, 2));
                lookPosition = _focusPoint - _lookDirection * CMath.RoundToDecimals(newDistance, 2);
                _debugPos = debugHitPoint.point;
                //Debug.Log("Hit distance: " + Vector3.Angle(_focusPoint-transform.position, _focusPoint - debugHitPoint.point));
                return true;
            }
        }   
        
        if(makeCollisionsSmoother)
        {
            var collisionOutSpeed = desiredCollisionOutSpeed;
            lookPosition = _focusPoint - _lookDirection * Mathf.Lerp(
                castDistance, _currentDistance, Time.unscaledDeltaTime * collisionOutSpeed);
        }

        return false;
    }
    
    private Vector3 _debugPos;
    private bool DirectionalBoxCast(Vector3 originalCenter, Vector3 originalHalfExtends, Vector3 originalCastDirection, 
        out RaycastHit hitInfo, Quaternion orientation, float originalMaxDistance, int originalLayerMask, Vector3 localPassDirection)
    {
        localPassDirection = localPassDirection == Vector3.zero ? defaultLocalPassDirection : CMath.CreateSignedUnitaryMask(localPassDirection);
        Vector3 detectionBoxArea = new Vector3(originalHalfExtends.x*2f, originalHalfExtends.y*2f, originalMaxDistance);
        Vector3 localDirectionMask = CMath.CreateUnitaryMask(localPassDirection);
        Vector3 boxCenter = originalCenter + originalCastDirection * (originalMaxDistance/2f);
        Vector3 newCastDirection = orientation * localPassDirection;
        Vector3 newCenter = boxCenter - newCastDirection * CMath.MultiplyVectors(detectionBoxArea/2f, localDirectionMask).magnitude;
        Vector3 newHalfExtends = CMath.MultiplyVectors(detectionBoxArea/2f, CMath.InverseMask(localDirectionMask) + localDirectionMask*0.01f);
        float castDistance = CMath.MultiplyVectors(detectionBoxArea, localDirectionMask).magnitude;
        
        //Optimized direction box detection
        RaycastHit[] hits = Physics.BoxCastAll(newCenter, newHalfExtends, newCastDirection,
            orientation, castDistance, originalLayerMask);
        hitInfo = GetClosestPointToTarget(_focusPoint, hits);
        
        if(hitInfo.distance != 0)
        {
            hitInfo = GetClosestPointToTarget(_focusPoint, hits);
            if (debug)
            {
                PhysicsDebug.DrawBoxCastBox(newCenter, newHalfExtends, orientation, newCastDirection, castDistance, Color.blue);
                Debug.DrawLine(newCenter, newCenter + newCastDirection*castDistance*1.5f, Color.yellow);
            }
            return true;
        }
        
        //If there is not a collision
        if (debug)
        {
            Debug.DrawLine(newCenter, newCenter + newCastDirection*castDistance*1.5f);
            PhysicsDebug.DrawBoxCastBox(newCenter, newHalfExtends, orientation, newCastDirection, castDistance, Color.red);
        }

        _globalDirection = transform.TransformDirection(defaultLocalPassDirection);
        hitInfo = new RaycastHit();
        return false;
    }

    private RaycastHit GetClosestPointToTarget(Vector3 target, RaycastHit[] data)
    {
        if(data.IsNullOrEmpty()) return new RaycastHit();
        
        int index = 0;
        float distance = Mathf.Infinity;

        for (int i = 0; i < data.Length; i++)
        {
            if(data[i].point == Vector3.zero || distance < Vector3.Distance(target, data[i].point)) continue;
            distance = Vector3.Distance(target, data[i].point);
            index = i;
        }

        return data[index];
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(_debugPos, 0.2f);
        //Gizmos.DrawSphere(_focusPoint, 0.1f);
    }
}