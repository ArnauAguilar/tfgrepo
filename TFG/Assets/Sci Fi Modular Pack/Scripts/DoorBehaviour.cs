﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBehaviour : MonoBehaviour
{
    // Start is called before the first frame update

    public float speed = 1;

    Vector3 closePos;
    Vector3 openPos;

    bool opening, closing;
    void Start()
    {
        closePos = transform.position;
        openPos = transform.position + new Vector3(-1.3f,0,0);
    }

    // Update is called once per frame
    void Update()
    {
        
        if (opening)
        {
            transform.position = Vector3.MoveTowards(transform.position, openPos, speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, openPos) < 0.01f)
            {
                opening = false;
                transform.position = openPos;
            }
            
        }
        else if (closing)
        {
            transform.position  = Vector3.MoveTowards(transform.position, closePos, speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, closePos) < 0.01f)
            {
                closing = false;
                transform.position = closePos;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        opening = true;
    }

    private void OnTriggerExit(Collider other)
    {
        closing = true;
    }




}
