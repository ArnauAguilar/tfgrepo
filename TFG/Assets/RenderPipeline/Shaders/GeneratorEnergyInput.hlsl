﻿#ifndef GENERATOR_ENERGY_INPUT_INCLUDED
#define GENERATOR_ENERGY_INPUT_INCLUDED

TEXTURE2D(_FresnellNoise);
SAMPLER(sampler_FresnellNoise);


UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
	UNITY_DEFINE_INSTANCED_PROP(float4, _FresnellNoise_ST)
	UNITY_DEFINE_INSTANCED_PROP(float4, _FresnellColor)
	UNITY_DEFINE_INSTANCED_PROP(float4, _InnerColor)
	UNITY_DEFINE_INSTANCED_PROP(float4, _CenterColor)
	UNITY_DEFINE_INSTANCED_PROP(float3, _FresnellNoiseScaleAndRotation)
	//UNITY_DEFINE_INSTANCED_PROP(float4, _BaseColor)
	UNITY_DEFINE_INSTANCED_PROP(float, _FresnelStrength)
	UNITY_DEFINE_INSTANCED_PROP(float, _FresnelNoiseSpeed)
	UNITY_DEFINE_INSTANCED_PROP(float, _CenterStrength)
	UNITY_DEFINE_INSTANCED_PROP(float, _OverlaySpeed)
	UNITY_DEFINE_INSTANCED_PROP(float, _Cutoff)
	UNITY_DEFINE_INSTANCED_PROP(float, _ZWrite)
UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

#define INPUT_PROP(name) UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, name)

struct InputConfig {
	Fragment fragment;
	float2 baseUV;
	float2 detailUV;
	bool useMask;
	bool useDetail;
};

InputConfig GetInputConfig (float4 positionSS, float2 baseUV, float2 detailUV = 0.0) {
	InputConfig c;
	c.fragment = GetFragment(positionSS);
	c.baseUV = baseUV;
	c.detailUV = detailUV;
	c.useMask = false;
	c.useDetail = false;
	return c;
}

float GetFresnelStrength() {
	return INPUT_PROP(_FresnelStrength);
}
float GetCenterStrength() {
	return INPUT_PROP(_CenterStrength);
}
float GetOverlaySpeed() {
	return INPUT_PROP(_OverlaySpeed);
}
float GetFresnelNoiseSpeed() {
	return INPUT_PROP(_FresnelNoiseSpeed);
}
float4 GetFresnelColor() {
	return INPUT_PROP(_FresnellColor);
}
float4 GetInnerColor() {
	return INPUT_PROP(_InnerColor);
}

float3 GetFresnellNoiseScaleAndRotation()
{
	return INPUT_PROP(_FresnellNoiseScaleAndRotation);
}

float2 TransformBaseUV (float2 baseUV) {
	float4 baseST = INPUT_PROP(_FresnellNoise_ST);
	return baseUV * baseST.xy + baseST.zw;
}
float4 GetFresnellNoise(float2 uv)
{
	return SAMPLE_TEXTURE2D(_FresnellNoise, sampler_FresnellNoise, uv);
}
float4 GetBase (InputConfig c) {
	return GetFresnellNoise(c.baseUV);
}

float GetFinalAlpha (float alpha) {
	return INPUT_PROP(_ZWrite) ? 1.0 : alpha;
}

float GetCutoff (InputConfig c) {
	return INPUT_PROP(_Cutoff);
}

float3 GetEmission(InputConfig c,float3 normal, float3 viewDirection, float2 screenPos) {

	float outerNoiseMask = FresnelEffect(normal, viewDirection, GetFresnelStrength());
	float3 fresnellNoiseAndRotation = GetFresnellNoiseScaleAndRotation();//xy scale, z rotation
	
	outerNoiseMask = outerNoiseMask * GradientNoise(RotateUV(c.baseUV + float2(0, _Time * GetFresnelNoiseSpeed()), fresnellNoiseAndRotation.xy/2.0f, fresnellNoiseAndRotation.z)*fresnellNoiseAndRotation.xy, 10);
	outerNoiseMask *= saturate(GradientNoise(c.baseUV + (float2(_CosTime, _SinTime) * GetOverlaySpeed()),4) - 0.3);
	outerNoiseMask += FresnelEffect(normal, viewDirection, 4);
	float inverseOuterNoiseMask = abs(1-outerNoiseMask);

	float blackInsideMask =  saturate( FresnelEffect(normal, viewDirection, GetCenterStrength())) + outerNoiseMask;

	//return inverseOuterNoiseMaskSmall;
	float3 color =((outerNoiseMask * GetFresnelColor().xyz) + inverseOuterNoiseMask * GetInnerColor().xyz) * blackInsideMask;
	
	return color + (GetFresnellNoise(screenPos) * abs(1-blackInsideMask));
}
#endif