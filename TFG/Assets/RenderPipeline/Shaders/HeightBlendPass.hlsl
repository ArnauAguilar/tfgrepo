﻿#ifndef CUSTOM_HEIGHT_BLEND_PASS_INCLUDED
#define CUSTOM_HEIGHT_BLEND_PASS_INCLUDED

#include "../ShaderLibrary/Surface.hlsl"
#include "../ShaderLibrary/Shadows.hlsl"
#include "../ShaderLibrary/Light.hlsl"
#include "../ShaderLibrary/BRDF.hlsl"
#include "../ShaderLibrary/GI.hlsl"
#include "../ShaderLibrary/Lighting.hlsl"

struct Attributes {
	float3 positionOS : POSITION;
	float3 normalOS : NORMAL;
	float4 tangentOS : TANGENT;
	float2 baseUV : TEXCOORD0;
	float4 color : COLOR;
	GI_ATTRIBUTE_DATA
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings {
	float4 positionCS_SS : SV_POSITION;
	float3 positionWS : VAR_POSITION;
	float3 normalWS : VAR_NORMAL;
	#if defined(_NORMAL_MAP)
		float4 tangentWS : VAR_TANGENT;
	#endif
	float2 baseUVR : VAR_BASE_UV0;
	float2 baseUVG : VAR_BASE_UV1;
	float2 baseUVB : VAR_BASE_UV2;
	float2 baseUVA : VAR_BASE_UV3;
	#if defined(_DETAIL_MAP)
		float2 detailUV : VAR_DETAIL_UV;
	#endif
		float4 color : VAR_COLOR;
	GI_VARYINGS_DATA
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

Varyings LitPassVertex (Attributes input) {
	Varyings output;
	UNITY_SETUP_INSTANCE_ID(input);
	UNITY_TRANSFER_INSTANCE_ID(input, output);
	TRANSFER_GI_DATA(input, output);
	output.positionWS = TransformObjectToWorld(input.positionOS);
	output.positionCS_SS = TransformWorldToHClip(output.positionWS);
	output.normalWS = TransformObjectToWorldNormal(input.normalOS);
		output.color = input.color;
	#if defined(_NORMAL_MAP)
		output.tangentWS = float4(
			TransformObjectToWorldDir(input.tangentOS.xyz), input.tangentOS.w
		);
	#endif
	output.baseUVR = TransformBaseUVR(input.baseUV);
	output.baseUVG = TransformBaseUVG(input.baseUV);
	output.baseUVB = TransformBaseUVB(input.baseUV);
	output.baseUVA = TransformBaseUVA(input.baseUV);
	#if defined(_DETAIL_MAP)
		output.detailUV = TransformDetailUV(input.baseUV);
	#endif
	return output;
}

float4 LitPassFragment (Varyings input) : SV_Target {
	UNITY_SETUP_INSTANCE_ID(input);
	InputConfig configR = GetInputConfig(input.positionCS_SS, input.baseUVR);
	InputConfig configG = GetInputConfig(input.positionCS_SS, input.baseUVG);
	InputConfig configB = GetInputConfig(input.positionCS_SS, input.baseUVB);
	InputConfig configA = GetInputConfig(input.positionCS_SS, input.baseUVA);
	ClipLOD(configR.fragment, unity_LODFade.x);
	
	#if defined(_MASK_MAP)
		configR.useMask = true;
		configG.useMask = true;
		configB.useMask = true;
		configA.useMask = true;
	#endif
	#if defined(_DETAIL_MAP)
		config.detailUV = input.detailUV;
		config.useDetail = true;
	#endif

	float4 BaseR =GetBaseR(configR);
	float4 BaseG =GetBaseG(configG);
	float4 BaseB =GetBaseB(configB);
	float4 BaseA =GetBaseA(configA);

	float HeightR = GetHeightR(configR) * input.color.r;
	float HeightG = GetHeightR(configG) * input.color.g;
	float HeightB = GetHeightR(configB) * input.color.b;
	float HeightA = GetHeightR(configA) * input.color.a;
	
	float4 base = HeightBlend4(	BaseR, HeightR,
                                BaseG, HeightG,
                                BaseB, HeightB,
                                BaseA, HeightA,
                                GetFactor12(),
                                GetFactor23(),
                                GetFactor34()
                                );
	//base = Heightblend(BaseR, HeightR, BaseG, HeightG, GetFactor12());
	#if defined(_CLIPPING)
		clip(base.a - GetCutoff(config));
	#endif
	
	Surface surface;
	surface.position = input.positionWS;
	#if defined(_NORMAL_MAP)
		float3 normalR =NormalTangentToWorld(GetNormalTSR(configR), input.normalWS, input.tangentWS);
		float3 normalG =NormalTangentToWorld(GetNormalTSG(configG), input.normalWS, input.tangentWS);
		float3 normalB =NormalTangentToWorld(GetNormalTSB(configB), input.normalWS, input.tangentWS);
		float3 normalA =NormalTangentToWorld(GetNormalTSA(configA), input.normalWS, input.tangentWS);
		surface.normal = HeightBlend4(	float4(normalR,1), HeightR,
										float4(normalG,1), HeightG,
										float4(normalB,1), HeightB,
										float4(normalA,1), HeightA,
										GetFactor12(),
										GetFactor23(),
										GetFactor34()
										).xyz;
	#else
		surface.normal = normalize(input.normalWS);
	#endif
	surface.interpolatedNormal = surface.normal;
	surface.viewDirection = normalize(_WorldSpaceCameraPos - input.positionWS);
	surface.depth = -TransformWorldToView(input.positionWS).z;
	surface.color = base.rgb;
	surface.alpha = base.a;

	float4 metalicR =GetMetallicR(configR);
	float4 metalicG =GetMetallicG(configG);
	float4 metalicB =GetMetallicB(configB);
	float4 metalicA =GetMetallicA(configA);
	
	surface.metallic =  HeightBlend4(metalicR, HeightR,
                                metalicG, HeightG,
                                metalicB, HeightB,
                                metalicA, HeightA,
                                GetFactor12(),
                                GetFactor23(),
                                GetFactor34()
                                );
	
	float4 oclusionR =GetOcclusionR(configR);
	float4 oclusionG =GetOcclusionG(configG);
	float4 oclusionB =GetOcclusionB(configB);
	float4 oclusionA =GetOcclusionA(configA);

	surface.occlusion = HeightBlend4(oclusionR, HeightR,
                                oclusionG, HeightG,
                                oclusionB, HeightB,
                                oclusionA, HeightA,
                                GetFactor12(),
                                GetFactor23(),
                                GetFactor34()
                                );

	float4 smoothnessR =GetSmoothnessR(configR);
	float4 smoothnessG =GetSmoothnessG(configG);
	float4 smoothnessB =GetSmoothnessB(configB);
	float4 smoothnessA =GetSmoothnessA(configA);
	surface.smoothness = HeightBlend4(smoothnessR, HeightR,
                                smoothnessG, HeightG,
                                smoothnessB, HeightB,
                                smoothnessA, HeightA,
                                GetFactor12(),
                                GetFactor23(),
                                GetFactor34()
                                );
	
	surface.fresnelStrength = GetFresnelR(configR);
	surface.dither = InterleavedGradientNoise(configR.fragment.positionSS, 0);
	surface.renderingLayerMask = asuint(unity_RenderingLayer.x);
	#if defined(_PREMULTIPLY_ALPHA)
		BRDF brdf = GetBRDF(surface, true);
	#else
		BRDF brdf = GetBRDF(surface);
	#endif
	GI gi = GetGI(GI_FRAGMENT_DATA(input), surface, brdf);
	float3 color = GetLighting(surface, brdf, gi);
	color += GetEmissionR(configR);

	return float4(color, GetFinalAlpha(surface.alpha));
}

#endif