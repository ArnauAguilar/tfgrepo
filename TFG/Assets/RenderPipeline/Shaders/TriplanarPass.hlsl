﻿#ifndef CUSTOM_TRIPLANAR_PASS_INCLUDED
#define CUSTOM_TRIPLANAR_PASS_INCLUDED

#include "../ShaderLibrary/Surface.hlsl"
#include "../ShaderLibrary/Shadows.hlsl"
#include "../ShaderLibrary/Light.hlsl"
#include "../ShaderLibrary/BRDF.hlsl"
#include "../ShaderLibrary/GI.hlsl"
#include "../ShaderLibrary/Lighting.hlsl"



struct Attributes
{
    float3 positionOS : POSITION;
    float3 normalOS : NORMAL;
    float4 tangentOS : TANGENT;
    float2 baseUV : TEXCOORD0;
    GI_ATTRIBUTE_DATA
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings
{
    float4 positionCS_SS : SV_POSITION;
    float4 positionCS : VAR_POSITION1;
    float3 positionWS : VAR_POSITION;
    float3 normalWS : VAR_NORMAL;
#if defined(_NORMAL_MAP)
    float4 tangentWS : VAR_TANGENT;
#endif
    float2 baseUV : VAR_BASE_UV;
    GI_VARYINGS_DATA
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

Varyings TriplanarPassVertex(Attributes input)
{
    Varyings output;
    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_TRANSFER_INSTANCE_ID(input, output);
    TRANSFER_GI_DATA(input, output);
    output.positionWS = TransformObjectToWorld(input.positionOS);
    output.positionCS_SS = TransformWorldToHClip(output.positionWS);
    output.positionCS = TransformWorldToHClip(output.positionWS);
    output.normalWS = TransformObjectToWorldNormal(input.normalOS);
#if defined(_NORMAL_MAP)
    output.tangentWS = float4(TransformObjectToWorldDir(input.tangentOS.xyz), input.tangentOS.w);
#endif
    output.baseUV = TransformBaseUV(input.baseUV);
    return output;
}
       
float4 GetFaceColorTop(InputConfig config, Varyings input)
{
    #if defined(_MASK_MAP)
    config.useMask = true;
    #endif
    float4 base = GetBaseTop(config);
    #if defined(_CLIPPING)
    clip(base.a - GetCutoff(config) * abs(dot(normalize(input.normalWS), float3(0,1,0))));
    #endif
    //Prepare surface
    Surface surface;
    surface.position = input.positionWS;
    
    #if defined(_NORMAL_MAP)
    surface.normal = NormalTangentToWorld(GetNormalTSTop(config), input.normalWS, input.tangentWS);
    #else
    surface.normal = normalize(input.normalWS);
    #endif
    surface.interpolatedNormal = input.normalWS;
    surface.viewDirection = normalize(_WorldSpaceCameraPos - input.positionWS);
    surface.depth = -TransformWorldToView(input.positionWS).z;
    surface.color = base.rgb;
    surface.alpha = base.a;
    surface.metallic = GetMetallicTop(config);
    surface.occlusion = GetOcclusionTop(config);
    surface.smoothness = GetSmoothnessTop(config);
    surface.fresnelStrength = GetFresnelTop(config);
    surface.dither = InterleavedGradientNoise(input.positionCS.xy, 0);
    surface.renderingLayerMask = asuint(unity_RenderingLayer.x);
    
    //generate BRDF data from surface
    #if defined(_PREMULTIPLY_ALPHA)
    BRDF brdf = GetBRDF(surface, true);
    #else
    BRDF brdf = GetBRDF(surface);
    #endif
    
    //get Lighting from BRDF & surface params
    GI gi = GetGI(GI_FRAGMENT_DATA(input), surface, brdf);
    float3 color = GetLighting(surface, brdf, gi);
    color += GetEmissionTop(config);
    return float4(color, surface.alpha) * normalize(SafePositivePow(input.normalWS, GetBlendSharpness(config))).y;
}
float4 GetFaceColorFront(InputConfig config, Varyings input)
{
    #if defined(_MASK_MAP)
    config.useMask = true;
    #endif
    float4 base = GetBaseFront(config);
    #if defined(_CLIPPING)
    clip(base.a - GetCutoff(config) * abs(dot(normalize(input.normalWS), float3(0,0,1))));
    #endif
    //Prepare surface
    Surface surface;
    surface.position = input.positionWS;
    
    #if defined(_NORMAL_MAP)
    surface.normal = NormalTangentToWorld(GetNormalTSFront(config), input.normalWS, input.tangentWS);
    #else
    surface.normal = normalize(input.normalWS);
    #endif
    surface.interpolatedNormal = input.normalWS;
    surface.viewDirection = normalize(_WorldSpaceCameraPos - input.positionWS);
    surface.depth = -TransformWorldToView(input.positionWS).z;
    surface.color = base.rgb;
    surface.alpha = base.a;
    surface.metallic = GetMetallicFront(config);
    surface.occlusion = GetOcclusionFront(config);
    surface.smoothness = GetSmoothnessFront(config);
    surface.fresnelStrength = GetFresnelFront(config);
    surface.dither = InterleavedGradientNoise(input.positionCS.xy, 0);
    surface.renderingLayerMask = asuint(unity_RenderingLayer.x);
    
    //generate BRDF data from surface
    #if defined(_PREMULTIPLY_ALPHA)
    BRDF brdf = GetBRDF(surface, true);
    #else
    BRDF brdf = GetBRDF(surface);
    #endif
    
    //get Lighting from BRDF & surface params
    GI gi = GetGI(GI_FRAGMENT_DATA(input), surface, brdf);
    float3 color = GetLighting(surface, brdf, gi);
    color += GetEmissionFront(config);
    return float4(color, surface.alpha) * normalize(SafePositivePow(input.normalWS, GetBlendSharpness(config))).z;
}

float4 GetFaceColorLeft(InputConfig config, Varyings input)
{
    #if defined(_MASK_MAP)
    config.useMask = true;
    #endif
    float4 base = GetBaseLeft(config);
    #if defined(_CLIPPING)
    clip(base.a - GetCutoff(config) * abs(dot(normalize(input.normalWS), float3(1,0,0))));
    #endif
    //Prepare surface
    Surface surface;
    surface.position = input.positionWS;
    
    #if defined(_NORMAL_MAP)
    surface.normal = NormalTangentToWorld(GetNormalTSLeft(config), input.normalWS, input.tangentWS);
    #else
    surface.normal = normalize(input.normalWS);
    #endif
    surface.interpolatedNormal = input.normalWS;
    surface.viewDirection = normalize(_WorldSpaceCameraPos - input.positionWS);
    surface.depth = -TransformWorldToView(input.positionWS).z;
    surface.color = base.rgb;
    surface.alpha = base.a;
    surface.metallic = GetMetallicLeft(config);
    surface.occlusion = GetOcclusionLeft(config);
    surface.smoothness = GetSmoothnessLeft(config);
    surface.fresnelStrength = GetFresnelLeft(config);
    surface.dither = InterleavedGradientNoise(input.positionCS.xy, 0);
    surface.renderingLayerMask = asuint(unity_RenderingLayer.x);
    
    //generate BRDF data from surface
    #if defined(_PREMULTIPLY_ALPHA)
    BRDF brdf = GetBRDF(surface, true);
    #else
    BRDF brdf = GetBRDF(surface);
    #endif
    
    //get Lighting from BRDF & surface params
    GI gi = GetGI(GI_FRAGMENT_DATA(input), surface, brdf);
    float3 color = GetLighting(surface, brdf, gi);
    color += GetEmissionLeft(config);
    return float4(color, surface.alpha) * normalize(SafePositivePow(input.normalWS, GetBlendSharpness(config))).x;
}

float4 TriplanarPassFragment(Varyings input) : SV_TARGET
{

    UNITY_SETUP_INSTANCE_ID(input);
    InputConfig config = GetInputConfig(input.positionCS_SS, input.baseUV);
    ClipLOD(config.fragment, unity_LODFade.x);
    InputConfig configX = GetInputConfig(input.positionCS_SS,TransformLeftUV(input.positionWS.zy));
    InputConfig configY = GetInputConfig(input.positionCS_SS,TransformTopUV(input.positionWS.xz));
    InputConfig configZ = GetInputConfig(input.positionCS_SS,TransformFrontUV(input.positionWS.xy));
    float4 color = GetFaceColorLeft(configX, input) + GetFaceColorTop(configY, input) + GetFaceColorFront(configZ, input);
    return color;
}

#endif