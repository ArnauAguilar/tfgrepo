﻿#ifndef TRIPLANAR_INPUT_INCLUDED
#define TRIPLANAR_INPUT_INCLUDED

#define INPUT_PROP(name) UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, name)

TEXTURE2D(_BaseMapTop);
TEXTURE2D(_BaseMapLeft);
TEXTURE2D(_BaseMapFront);

TEXTURE2D(_EmissionMapTop);
TEXTURE2D(_EmissionMapLeft);
TEXTURE2D(_EmissionMapFront);

TEXTURE2D(_NormalMapTop);
TEXTURE2D(_NormalMapLeft);
TEXTURE2D(_NormalMapFront);

TEXTURE2D(_MaskMapTop);
TEXTURE2D(_MaskMapLeft);
TEXTURE2D(_MaskMapFront);

SAMPLER(sampler_BaseMapTop);
SAMPLER(sampler_BaseMapLeft);
SAMPLER(sampler_BaseMapFront);

UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
    UNITY_DEFINE_INSTANCED_PROP(float, _BlendSharpness)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseMapTop_ST)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseMapLeft_ST)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseMapFront_ST)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseColorTop)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseColorLeft)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseColorFront)
	UNITY_DEFINE_INSTANCED_PROP(float, _Cutoff)
	UNITY_DEFINE_INSTANCED_PROP(float, _MetallicTop)
	UNITY_DEFINE_INSTANCED_PROP(float, _MetallicLeft)
	UNITY_DEFINE_INSTANCED_PROP(float, _MetallicFront)
	UNITY_DEFINE_INSTANCED_PROP(float, _SmoothnessTop)
	UNITY_DEFINE_INSTANCED_PROP(float, _SmoothnessLeft)
	UNITY_DEFINE_INSTANCED_PROP(float, _SmoothnessFront)
    UNITY_DEFINE_INSTANCED_PROP(float4, _EmissionColorTop)
    UNITY_DEFINE_INSTANCED_PROP(float4, _EmissionColorLeft)
    UNITY_DEFINE_INSTANCED_PROP(float4, _EmissionColorFront)
    UNITY_DEFINE_INSTANCED_PROP(float, _FresnelTop)
    UNITY_DEFINE_INSTANCED_PROP(float, _FresnelLeft)
    UNITY_DEFINE_INSTANCED_PROP(float, _FresnelFront)
    UNITY_DEFINE_INSTANCED_PROP(float, _OcclusionTop)
    UNITY_DEFINE_INSTANCED_PROP(float, _OcclusionLeft)
    UNITY_DEFINE_INSTANCED_PROP(float, _OcclusionFront)
    UNITY_DEFINE_INSTANCED_PROP(float, _NormalScaleTop)
    UNITY_DEFINE_INSTANCED_PROP(float, _NormalScaleLeft)
    UNITY_DEFINE_INSTANCED_PROP(float, _NormalScaleFront)
UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

struct InputConfig {
    Fragment fragment;
    float2 baseUV;
    float2 detailUV;
    bool useMask;
    bool useDetail;
};

InputConfig GetInputConfig (float4 positionSS, float2 baseUV, float2 detailUV = 0.0) {
    InputConfig c;
    c.fragment = GetFragment(positionSS);
    c.baseUV = baseUV;
    c.detailUV = detailUV;
    c.useMask = false;
    c.useDetail = false;
    return c;
}

float3 GetNormalTSTop(InputConfig c)
{
    float4 map = SAMPLE_TEXTURE2D(_NormalMapTop, sampler_BaseMapTop, c.baseUV);
    float scale = INPUT_PROP(_NormalScaleTop);
    float3 normal = DecodeNormal(map, scale);
    return normal;
}
float3 GetNormalTSLeft(InputConfig c)
{
    float4 map = SAMPLE_TEXTURE2D(_NormalMapLeft, sampler_BaseMapTop, c.baseUV);
    float scale = INPUT_PROP(_NormalScaleLeft);
    float3 normal = DecodeNormal(map, scale);
    return normal;
}

float3 GetNormalTSFront(InputConfig c)
{
    float4 map = SAMPLE_TEXTURE2D(_NormalMapFront, sampler_BaseMapTop, c.baseUV);
    float scale = INPUT_PROP(_NormalScaleFront);
    float3 normal = DecodeNormal(map, scale);
    return normal;
}


float4 GetMaskTop(InputConfig c)
{
    if (c.useMask)
    {
        return SAMPLE_TEXTURE2D(_MaskMapTop, sampler_BaseMapTop, c.baseUV);
    }
    return 1.0;
}
float4 GetMaskLeft(InputConfig c)
{
    if (c.useMask)
    {
        return SAMPLE_TEXTURE2D(_MaskMapLeft, sampler_BaseMapLeft, c.baseUV);
    }
    return 1.0;
}
float4 GetMaskFront(InputConfig c)
{
    if (c.useMask)
    {
        return SAMPLE_TEXTURE2D(_MaskMapFront, sampler_BaseMapFront, c.baseUV);
    }
    return 1.0;
}

float GetFresnelTop(InputConfig c)
{
    return INPUT_PROP(_FresnelTop);
}
float GetFresnelFront(InputConfig c)
{
    return INPUT_PROP(_FresnelFront);
}
float GetFresnelLeft(InputConfig c)
{
    return INPUT_PROP(_FresnelLeft);
}

float GetBlendSharpness(InputConfig c)
{
    return INPUT_PROP(_BlendSharpness);
}

float4 GetBaseTop(InputConfig c)
{
    float4 map = SAMPLE_TEXTURE2D(_BaseMapTop, sampler_BaseMapTop, c.baseUV);
    float4 color = INPUT_PROP(_BaseColorTop);
    return map * color;
}
float4 GetBaseFront(InputConfig c)
{
    float4 map = SAMPLE_TEXTURE2D(_BaseMapFront, sampler_BaseMapFront, c.baseUV);
    float4 color = INPUT_PROP(_BaseColorFront);
    return map * color;
}
float4 GetBaseLeft(InputConfig c)
{
    float4 map = SAMPLE_TEXTURE2D(_BaseMapLeft, sampler_BaseMapLeft, c.baseUV);
    float4 color = INPUT_PROP(_BaseColorLeft);
    return map * color;
}

float3 GetEmissionTop(InputConfig c)
{
    float4 map = SAMPLE_TEXTURE2D(_EmissionMapTop, sampler_BaseMapTop, c.baseUV);
    float4 color = INPUT_PROP(_EmissionColorTop);
    return map.rgb * color.rgb;
}
float3 GetEmissionFront(InputConfig c)
{
    float4 map = SAMPLE_TEXTURE2D(_EmissionMapFront, sampler_BaseMapFront, c.baseUV);
    float4 color = INPUT_PROP(_EmissionColorFront);
    return map.rgb * color.rgb;
}
float3 GetEmissionLeft(InputConfig c)
{
    float4 map = SAMPLE_TEXTURE2D(_EmissionMapLeft, sampler_BaseMapLeft, c.baseUV);
    float4 color = INPUT_PROP(_EmissionColorLeft);
    return map.rgb * color.rgb;
}


float2 TransformBaseUV(float2 baseUV)
{
    float4 baseST = INPUT_PROP(_BaseMapTop_ST);
    return baseUV * baseST.xy + baseST.zw;
}
float2 TransformTopUV(float2 baseUV)
{
    float4 baseST = INPUT_PROP(_BaseMapTop_ST);
    return baseUV * baseST.xy + baseST.zw;
}
float2 TransformLeftUV(float2 baseUV)
{
    float4 baseST = INPUT_PROP(_BaseMapLeft_ST);
    return baseUV * baseST.xy + baseST.zw;
}
float2 TransformFrontUV(float2 baseUV)
{
    float4 baseST = INPUT_PROP(_BaseMapFront_ST);
    return baseUV * baseST.xy + baseST.zw;
}


float GetCutoff(InputConfig c)
{
    return INPUT_PROP(_Cutoff);
}

float GetMetallicTop(InputConfig c)
{
    float metallic = INPUT_PROP(_MetallicTop);
    metallic *= GetMaskTop(c).r;
    return metallic;
}
float GetMetallicLeft(InputConfig c)
{
    float metallic = INPUT_PROP(_MetallicLeft);
    metallic *= GetMaskLeft(c).r;
    return metallic;
}
float GetMetallicFront(InputConfig c)
{
    float metallic = INPUT_PROP(_MetallicFront);
    metallic *= GetMaskFront(c).r;
    return metallic;
}

float GetSmoothnessTop(InputConfig c)
{
    float smoothness = INPUT_PROP(_SmoothnessTop);
#if defined(_MASK_MAP)
    smoothness *= remap(GetMaskTop(c).a, 0.0, 1.0, 1.0, 0.0);
#else
    smoothness *= GetMaskTop(c).a;
#endif
    return smoothness;
}
float GetSmoothnessLeft(InputConfig c)
{
    float smoothness = INPUT_PROP(_SmoothnessLeft);
    #if defined(_MASK_MAP)
    smoothness *= remap(GetMaskLeft(c).a, 0.0, 1.0, 1.0, 0.0);
    #else
    smoothness *= GetMaskLeft(c).a;
    #endif
    return smoothness;
}
float GetSmoothnessFront(InputConfig c)
{
    float smoothness = INPUT_PROP(_SmoothnessFront);
    #if defined(_MASK_MAP)
    smoothness *= remap(GetMaskFront(c).a, 0.0, 1.0, 1.0, 0.0);
    #else
    smoothness *= GetMaskFront(c).a;
    #endif
    return smoothness;
}

float GetOcclusionTop(InputConfig c)
{
    float strength = INPUT_PROP(_OcclusionTop);
    float occlusion = GetMaskTop(c).g;
    occlusion = lerp(occlusion, 1.0, strength);
    return occlusion;
}
float GetOcclusionFront(InputConfig c)
{
    float strength = INPUT_PROP(_OcclusionFront);
    float occlusion = GetMaskFront(c).g;
    occlusion = lerp(occlusion, 1.0, strength);
    return occlusion;
}
float GetOcclusionLeft(InputConfig c)
{
    float strength = INPUT_PROP(_OcclusionLeft);
    float occlusion = GetMaskLeft(c).g;
    occlusion = lerp(occlusion, 1.0, strength);
    return occlusion;
}

#endif