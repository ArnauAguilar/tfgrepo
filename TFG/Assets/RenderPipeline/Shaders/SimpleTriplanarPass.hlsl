﻿#ifndef CUSTOM_SIMPLE_TRIPLANAR_PASS_INCLUDED
#define CUSTOM_SIMPLE_TRIPLANAR_PASS_INCLUDED

#include "../ShaderLibrary/Surface.hlsl"
#include "../ShaderLibrary/Shadows.hlsl"
#include "../ShaderLibrary/Light.hlsl"
#include "../ShaderLibrary/BRDF.hlsl"
#include "../ShaderLibrary/GI.hlsl"
#include "../ShaderLibrary/Lighting.hlsl"



struct Attributes
{
    float3 positionOS : POSITION;
    float3 normalOS : NORMAL;
    float4 tangentOS : TANGENT;
    float2 baseUV : TEXCOORD0;
    GI_ATTRIBUTE_DATA
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings
{
    float4 positionCS_SS : SV_POSITION;
    float4 positionCS : VAR_POSITION;
    float3 positionWS: VAR_POSITION1;
    float3 normalWS : VAR_NORMAL;
#if defined(_NORMAL_MAP)
    float4 tangentWS : VAR_TANGENT;
#endif
    float2 baseUV : VAR_BASE_UV;
    GI_VARYINGS_DATA
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

Varyings SimpleTriplanarPassVertex(Attributes input)
{
    Varyings output;
    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_TRANSFER_INSTANCE_ID(input, output);
    TRANSFER_GI_DATA(input, output);
    output.positionWS = TransformObjectToWorld(input.positionOS);
    output.positionCS_SS = TransformWorldToHClip(output.positionWS);
    output.positionCS = TransformWorldToHClip(output.positionWS);
    output.normalWS = TransformObjectToWorldNormal(input.normalOS);
#if defined(_NORMAL_MAP)
    output.tangentWS = float4(TransformObjectToWorldDir(input.tangentOS.xyz), input.tangentOS.w);
#endif
    output.baseUV = TransformBaseUV(input.baseUV);
    return output;
}
       
float4 GetFaceColor(InputConfig config, Varyings input, float3 direction)
{
    #if defined(_MASK_MAP)
    config.useMask = true;
    #endif
    float4 base = GetBase(config);
    #if defined(_CLIPPING)
    clip(base.a - GetCutoff(config) * abs(dot(normalize(input.normalWS), direction)));
    #endif
    //Prepare surface
    Surface surface;
    surface.position = input.positionWS;
    
    #if defined(_NORMAL_MAP)
    surface.normal = NormalTangentToWorld(GetNormalTS(config), input.normalWS, input.tangentWS);
    #else
    surface.normal = normalize(input.normalWS);
    #endif
    surface.interpolatedNormal = input.normalWS;
    surface.viewDirection = normalize(_WorldSpaceCameraPos - input.positionWS);
    surface.depth = -TransformWorldToView(input.positionWS).z;
    surface.color = base.rgb;
    surface.alpha = base.a;
    surface.metallic = GetMetallic(config);
    surface.occlusion = GetOcclusion(config);
    surface.smoothness = GetSmoothness(config);
    surface.fresnelStrength = GetFresnel(config);
    surface.dither = InterleavedGradientNoise(input.positionCS.xy, 0);
    surface.renderingLayerMask = asuint(unity_RenderingLayer.x);
    
    //generate BRDF data from surface
    #if defined(_PREMULTIPLY_ALPHA)
    BRDF brdf = GetBRDF(surface, true);
    #else
    BRDF brdf = GetBRDF(surface);
    #endif
    
    //get Lighting from BRDF & surface params
    GI gi = GetGI(GI_FRAGMENT_DATA(input), surface, brdf);
    float3 color = GetLighting(surface, brdf, gi);
    color += GetEmission(config);
    return float4(color, surface.alpha) * length(normalize(SafePositivePow(input.normalWS, GetBlendSharpness(config))) * direction);
}

float4 SimpleTriplanarPassFragment(Varyings input) : SV_TARGET
{

    UNITY_SETUP_INSTANCE_ID(input);
    InputConfig config = GetInputConfig(input.positionCS_SS, input.baseUV);
    ClipLOD(config.fragment, unity_LODFade.x);
    InputConfig configX = GetInputConfig(input.positionCS_SS, TransformBaseUV(input.positionWS.yz));
    InputConfig configY = GetInputConfig(input.positionCS_SS, TransformBaseUV(input.positionWS.xz));
    InputConfig configZ = GetInputConfig(input.positionCS_SS, TransformBaseUV(input.positionWS.xy));
    float4 color = GetFaceColor(configX, input, float3(1,0,0)) + GetFaceColor(configY, input, float3(0,1,0)) + GetFaceColor(configZ, input, float3(0,0,1));
    return color;
}

#endif