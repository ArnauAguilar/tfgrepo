﻿#ifndef CUSTOM_META_PASS_INCLUDED
#define CUSTOM_META_PASS_INCLUDED

#include "../ShaderLibrary/Surface.hlsl"
#include "../ShaderLibrary/Shadows.hlsl"
#include "../ShaderLibrary/Light.hlsl"
#include "../ShaderLibrary/BRDF.hlsl"

bool4 unity_MetaFragmentControl;
float unity_OneOverOutputBoost;
float unity_MaxOutputValue;

struct Attributes {
	float3 positionOS : POSITION;
	float3 normalOS : NORMAL;
	float2 baseUV : TEXCOORD0;
	float2 lightMapUV : TEXCOORD1;
};

struct Varyings {
	float4 positionCS_SS : SV_POSITION;
	float4 screenPos: TEXCOORD7;
	float3 normalOS : TEXCOORD6;
	float3 viewDir : TEXCOORD8;
	float2 baseUV : VAR_BASE_UV;
};

Varyings MetaPassVertex (Attributes input) {
	Varyings output;
	input.positionOS.xy =
        input.lightMapUV * unity_LightmapST.xy + unity_LightmapST.zw;
	input.positionOS.z = input.positionOS.z > 0.0 ? FLT_MIN : 0.0;
	output.positionCS_SS = TransformWorldToHClip(input.positionOS);
	output.baseUV = TransformBaseUV(input.baseUV);
	output.normalOS = input.normalOS;
	output.screenPos = ComputeScreenPos(output.positionCS_SS);
	output.viewDir = ObjSpaceViewDir(input.positionOS);
	return output;
}

float4 MetaPassFragment (Varyings input) : SV_TARGET {
	InputConfig config = GetInputConfig(input.positionCS_SS, input.baseUV);
	float2 screenPos = float2(input.screenPos.xy / input.screenPos.w);
	float4 base = GetBase(config);
	Surface surface;
	ZERO_INITIALIZE(Surface, surface);
	surface.color = base.rgb;
	surface.metallic = 0;
	surface.smoothness = 0;
	BRDF brdf = GetBRDF(surface);
	float4 meta = 0.0;
	if (unity_MetaFragmentControl.x) {
		meta = float4(brdf.diffuse, 1.0);
		meta.rgb += brdf.specular * brdf.roughness * 0.5;
		meta.rgb = min(
            PositivePow(meta.rgb, unity_OneOverOutputBoost), unity_MaxOutputValue
        );
	}
	else if (unity_MetaFragmentControl.y) {

		meta = float4(GetEmission(config, input.normalOS, input.viewDir, screenPos.xy), 1.0);
	}
	return meta;
}

#endif