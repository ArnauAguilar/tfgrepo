﻿#ifndef CUSTOM_HEIGHT_BLEND_INPUT_INCLUDED
#define CUSTOM_HEIGHT_BLEND_INPUT_INCLUDED

TEXTURE2D(_BaseMapR);
TEXTURE2D(_BaseMapG);
TEXTURE2D(_BaseMapB);
TEXTURE2D(_BaseMapA);

TEXTURE2D(_HeightMapR);
TEXTURE2D(_HeightMapG);
TEXTURE2D(_HeightMapB);
TEXTURE2D(_HeightMapA);

TEXTURE2D(_MaskMapR);
TEXTURE2D(_MaskMapG);
TEXTURE2D(_MaskMapB);
TEXTURE2D(_MaskMapA);

TEXTURE2D(_NormalMapR);
TEXTURE2D(_NormalMapG);
TEXTURE2D(_NormalMapB);
TEXTURE2D(_NormalMapA);

TEXTURE2D(_EmissionMapR);
TEXTURE2D(_EmissionMapG);
TEXTURE2D(_EmissionMapB);
TEXTURE2D(_EmissionMapA);

SAMPLER(sampler_BaseMapR);
SAMPLER(sampler_BaseMapG);
SAMPLER(sampler_BaseMapB);
SAMPLER(sampler_BaseMapA);

TEXTURE2D(_DetailMap);
TEXTURE2D(_DetailNormalMap);
SAMPLER(sampler_DetailMap);

UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseMapR_ST)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseMapG_ST)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseMapB_ST)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseMapA_ST)
	UNITY_DEFINE_INSTANCED_PROP(float4, _DetailMap_ST)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseColorR)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseColorG)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseColorB)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseColorA)
	UNITY_DEFINE_INSTANCED_PROP(float4, _EmissionColorR)
	UNITY_DEFINE_INSTANCED_PROP(float4, _EmissionColorG)
	UNITY_DEFINE_INSTANCED_PROP(float4, _EmissionColorB)
	UNITY_DEFINE_INSTANCED_PROP(float4, _EmissionColorA)
	UNITY_DEFINE_INSTANCED_PROP(float, _Cutoff)
	UNITY_DEFINE_INSTANCED_PROP(float, _ZWrite)
	UNITY_DEFINE_INSTANCED_PROP(float, _MetallicR)
	UNITY_DEFINE_INSTANCED_PROP(float, _MetallicG)
	UNITY_DEFINE_INSTANCED_PROP(float, _MetallicB)
	UNITY_DEFINE_INSTANCED_PROP(float, _MetallicA)
	UNITY_DEFINE_INSTANCED_PROP(float, _OcclusionR)
	UNITY_DEFINE_INSTANCED_PROP(float, _OcclusionG)
	UNITY_DEFINE_INSTANCED_PROP(float, _OcclusionB)
	UNITY_DEFINE_INSTANCED_PROP(float, _OcclusionA)
	UNITY_DEFINE_INSTANCED_PROP(float, _SmoothnessR)
	UNITY_DEFINE_INSTANCED_PROP(float, _SmoothnessG)
	UNITY_DEFINE_INSTANCED_PROP(float, _SmoothnessB)
	UNITY_DEFINE_INSTANCED_PROP(float, _SmoothnessA)
	UNITY_DEFINE_INSTANCED_PROP(float, _FresnelR)
	UNITY_DEFINE_INSTANCED_PROP(float, _FresnelG)
	UNITY_DEFINE_INSTANCED_PROP(float, _FresnelB)
	UNITY_DEFINE_INSTANCED_PROP(float, _FresnelA)
	UNITY_DEFINE_INSTANCED_PROP(float, _DetailAlbedo)
	UNITY_DEFINE_INSTANCED_PROP(float, _DetailSmoothness)
	UNITY_DEFINE_INSTANCED_PROP(float, _DetailNormalScale)
	UNITY_DEFINE_INSTANCED_PROP(float, _NormalScaleR)
	UNITY_DEFINE_INSTANCED_PROP(float, _NormalScaleG)
	UNITY_DEFINE_INSTANCED_PROP(float, _NormalScaleB)
	UNITY_DEFINE_INSTANCED_PROP(float, _NormalScaleA)
	UNITY_DEFINE_INSTANCED_PROP(float, _Factor12)
	UNITY_DEFINE_INSTANCED_PROP(float, _Factor23)
	UNITY_DEFINE_INSTANCED_PROP(float, _Factor34)
UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

#define INPUT_PROP(name) UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, name)

struct InputConfig {
	Fragment fragment;
	float2 baseUV;
	float2 detailUV;
	bool useMask;
	bool useDetail;
};

InputConfig GetInputConfig (float4 positionSS, float2 baseUV, float2 detailUV = 0.0) {
	InputConfig c;
	c.fragment = GetFragment(positionSS);
	c.baseUV = baseUV;
	c.detailUV = detailUV;
	c.useMask = false;
	c.useDetail = false;
	return c;
}
float GetFactor12()
{
	return INPUT_PROP(_Factor12);
}
float GetFactor23()
{
	return INPUT_PROP(_Factor23);
}
float GetFactor34()
{
	return INPUT_PROP(_Factor34);
}

float2 TransformBaseUVR (float2 baseUV) {
	float4 baseST = INPUT_PROP(_BaseMapR_ST);
	return baseUV * baseST.xy + baseST.zw;
}
float2 TransformBaseUVG (float2 baseUV) {
	float4 baseST = INPUT_PROP(_BaseMapG_ST);
	return baseUV * baseST.xy + baseST.zw;
}
float2 TransformBaseUVB (float2 baseUV) {
	float4 baseST = INPUT_PROP(_BaseMapB_ST);
	return baseUV * baseST.xy + baseST.zw;
}
float2 TransformBaseUVA (float2 baseUV) {
	float4 baseST = INPUT_PROP(_BaseMapA_ST);
	return baseUV * baseST.xy + baseST.zw;
}

float2 TransformDetailUV (float2 detailUV) {
	float4 detailST = INPUT_PROP(_DetailMap_ST);
	return detailUV * detailST.xy + detailST.zw;
}

float GetHeightR (InputConfig c) {
	return SAMPLE_TEXTURE2D(_HeightMapR, sampler_BaseMapR, c.baseUV);
}
float GetHeightG (InputConfig c) {
	return SAMPLE_TEXTURE2D(_HeightMapG, sampler_BaseMapG, c.baseUV);
}
float GetHeightB (InputConfig c) {
	return SAMPLE_TEXTURE2D(_HeightMapB, sampler_BaseMapB, c.baseUV);
}
float GetHeightA (InputConfig c) {
	return SAMPLE_TEXTURE2D(_HeightMapA, sampler_BaseMapA, c.baseUV);
}

float4 GetMaskR (InputConfig c) {
	if (c.useMask) {
		return SAMPLE_TEXTURE2D(_MaskMapR, sampler_BaseMapR, c.baseUV);
	}
	return 1.0;
}
float4 GetMaskG (InputConfig c) {
	if (c.useMask) {
		return SAMPLE_TEXTURE2D(_MaskMapG, sampler_BaseMapG, c.baseUV);
	}
	return 1.0;
}
float4 GetMaskB (InputConfig c) {
	if (c.useMask) {
		return SAMPLE_TEXTURE2D(_MaskMapB, sampler_BaseMapB, c.baseUV);
	}
	return 1.0;
}
float4 GetMaskA (InputConfig c) {
	if (c.useMask) {
		return SAMPLE_TEXTURE2D(_MaskMapA, sampler_BaseMapA, c.baseUV);
	}
	return 1.0;
}

float4 GetDetail (InputConfig c) {
	if (c.useDetail) {
		float4 map = SAMPLE_TEXTURE2D(_DetailMap, sampler_DetailMap, c.detailUV);
		return map * 2.0 - 1.0;
	}
	return 0.0;
}

float4 GetBaseR (InputConfig c) {
	float4 map = SAMPLE_TEXTURE2D(_BaseMapR, sampler_BaseMapR, c.baseUV);
	float4 color = INPUT_PROP(_BaseColorR);

	if (c.useDetail) {
		float detail = GetDetail(c).r * INPUT_PROP(_DetailAlbedo);
		float mask = GetMaskR(c).b;
		map.rgb =
			lerp(sqrt(map.rgb), detail < 0.0 ? 0.0 : 1.0, abs(detail) * mask);
		map.rgb *= map.rgb;
	}
	
	return map * color;
}
float4 GetBaseG (InputConfig c) {
	float4 map = SAMPLE_TEXTURE2D(_BaseMapG, sampler_BaseMapG, c.baseUV);
	float4 color = INPUT_PROP(_BaseColorG);

	if (c.useDetail) {
		float detail = GetDetail(c).r * INPUT_PROP(_DetailAlbedo);
		float mask = GetMaskG(c).b;
		map.rgb =
            lerp(sqrt(map.rgb), detail < 0.0 ? 0.0 : 1.0, abs(detail) * mask);
		map.rgb *= map.rgb;
	}
	
	return map * color;
}
float4 GetBaseB (InputConfig c) {
	float4 map = SAMPLE_TEXTURE2D(_BaseMapB, sampler_BaseMapB, c.baseUV);
	float4 color = INPUT_PROP(_BaseColorB);

	if (c.useDetail) {
		float detail = GetDetail(c).r * INPUT_PROP(_DetailAlbedo);
		float mask = GetMaskB(c).b;
		map.rgb =
            lerp(sqrt(map.rgb), detail < 0.0 ? 0.0 : 1.0, abs(detail) * mask);
		map.rgb *= map.rgb;
	}
	
	return map * color;
}
float4 GetBaseA (InputConfig c) {
	float4 map = SAMPLE_TEXTURE2D(_BaseMapA, sampler_BaseMapA, c.baseUV);
	float4 color = INPUT_PROP(_BaseColorA);

	if (c.useDetail) {
		float detail = GetDetail(c).r * INPUT_PROP(_DetailAlbedo);
		float mask = GetMaskA(c).b;
		map.rgb =
            lerp(sqrt(map.rgb), detail < 0.0 ? 0.0 : 1.0, abs(detail) * mask);
		map.rgb *= map.rgb;
	}
	
	return map * color;
}

float GetFinalAlpha (float alpha) {
	return INPUT_PROP(_ZWrite) ? 1.0 : alpha;
}

float3 GetNormalTSR (InputConfig c) {
	float4 map = SAMPLE_TEXTURE2D(_NormalMapR, sampler_BaseMapR, c.baseUV);
	float scale = INPUT_PROP(_NormalScaleR);
	float3 normal = DecodeNormal(map, scale);

	if (c.useDetail) {
		//map = SAMPLE_TEXTURE2D(_DetailNormalMap, sampler_DetailMap, c.detailUV);
		//scale = INPUT_PROP(_DetailNormalScale) * GetMaskR(c).b;
		//float3 detail = DecodeNormal(map, scale);
		//normal = BlendNormalRNM(normal, detail);
	}
	
	return normal;
}
float3 GetNormalTSG (InputConfig c) {
	float4 map = SAMPLE_TEXTURE2D(_NormalMapG, sampler_BaseMapG, c.baseUV);
	float scale = INPUT_PROP(_NormalScaleG);
	float3 normal = DecodeNormal(map, scale);

	if (c.useDetail) {
		map = SAMPLE_TEXTURE2D(_DetailNormalMap, sampler_DetailMap, c.detailUV);
		scale = INPUT_PROP(_DetailNormalScale) * GetMaskG(c).b;
		float3 detail = DecodeNormal(map, scale);
		normal = BlendNormalRNM(normal, detail);
	}
	
	return normal;
}
float3 GetNormalTSB (InputConfig c) {
	float4 map = SAMPLE_TEXTURE2D(_NormalMapB, sampler_BaseMapB, c.baseUV);
	float scale = INPUT_PROP(_NormalScaleB);
	float3 normal = DecodeNormal(map, scale);

	if (c.useDetail) {
		map = SAMPLE_TEXTURE2D(_DetailNormalMap, sampler_DetailMap, c.detailUV);
		scale = INPUT_PROP(_DetailNormalScale) * GetMaskB(c).b;
		float3 detail = DecodeNormal(map, scale);
		normal = BlendNormalRNM(normal, detail);
	}
	
	return normal;
}
float3 GetNormalTSA (InputConfig c) {
	float4 map = SAMPLE_TEXTURE2D(_NormalMapA, sampler_BaseMapA, c.baseUV);
	float scale = INPUT_PROP(_NormalScaleA);
	float3 normal = DecodeNormal(map, scale);

	if (c.useDetail) {
		map = SAMPLE_TEXTURE2D(_DetailNormalMap, sampler_DetailMap, c.detailUV);
		scale = INPUT_PROP(_DetailNormalScale) * GetMaskA(c).b;
		float3 detail = DecodeNormal(map, scale);
		normal = BlendNormalRNM(normal, detail);
	}
	
	return normal;
}

float3 GetEmissionR (InputConfig c) {
	float4 map = SAMPLE_TEXTURE2D(_EmissionMapR, sampler_BaseMapR, c.baseUV);
	float4 color = INPUT_PROP(_EmissionColorR);
	return map.rgb * color.rgb;
}
float3 GetEmissionG (InputConfig c) {
	float4 map = SAMPLE_TEXTURE2D(_EmissionMapG, sampler_BaseMapG, c.baseUV);
	float4 color = INPUT_PROP(_EmissionColorG);
	return map.rgb * color.rgb;
}
float3 GetEmissionB (InputConfig c) {
	float4 map = SAMPLE_TEXTURE2D(_EmissionMapB, sampler_BaseMapB, c.baseUV);
	float4 color = INPUT_PROP(_EmissionColorB);
	return map.rgb * color.rgb;
}
float3 GetEmissionA (InputConfig c) {
	float4 map = SAMPLE_TEXTURE2D(_EmissionMapA, sampler_BaseMapA, c.baseUV);
	float4 color = INPUT_PROP(_EmissionColorA);
	return map.rgb * color.rgb;
}

float GetCutoff (InputConfig c) {
	return INPUT_PROP(_Cutoff);
}

float GetMetallicR (InputConfig c) {
	float metallic = INPUT_PROP(_MetallicR);
	metallic *= GetMaskR(c).r;
	return metallic;
}
float GetMetallicG (InputConfig c) {
	float metallic = INPUT_PROP(_MetallicG);
	metallic *= GetMaskG(c).r;
	return metallic;
}
float GetMetallicB (InputConfig c) {
	float metallic = INPUT_PROP(_MetallicB);
	metallic *= GetMaskB(c).r;
	return metallic;
}
float GetMetallicA (InputConfig c) {
	float metallic = INPUT_PROP(_MetallicA);
	metallic *= GetMaskA(c).r;
	return metallic;
}

float GetOcclusionR (InputConfig c) {
	float strength = INPUT_PROP(_OcclusionR);
	float occlusion = GetMaskR(c).g;
	occlusion = lerp(occlusion, 1.0, strength);
	return occlusion;
}
float GetOcclusionG (InputConfig c) {
	float strength = INPUT_PROP(_OcclusionG);
	float occlusion = GetMaskG(c).g;
	occlusion = lerp(occlusion, 1.0, strength);
	return occlusion;
}
float GetOcclusionB (InputConfig c) {
	float strength = INPUT_PROP(_OcclusionB);
	float occlusion = GetMaskB(c).g;
	occlusion = lerp(occlusion, 1.0, strength);
	return occlusion;
}
float GetOcclusionA (InputConfig c) {
	float strength = INPUT_PROP(_OcclusionA);
	float occlusion = GetMaskA(c).g;
	occlusion = lerp(occlusion, 1.0, strength);
	return occlusion;
}

float GetSmoothnessR (InputConfig c) {
	float smoothness = INPUT_PROP(_SmoothnessR);
	smoothness *= GetMaskR(c).a;

	if (c.useDetail) {
		float detail = GetDetail(c).b * INPUT_PROP(_DetailSmoothness);
		float mask = GetMaskR(c).a;
		smoothness =
			lerp(smoothness, detail < 0.0 ? 0.0 : 1.0, abs(detail) * mask);
	}
	
	return smoothness;
}
float GetSmoothnessG (InputConfig c) {
	float smoothness = INPUT_PROP(_SmoothnessG);
	smoothness *= GetMaskG(c).a;

	if (c.useDetail) {
		float detail = GetDetail(c).b * INPUT_PROP(_DetailSmoothness);
		float mask = GetMaskG(c).a;
		smoothness =
            lerp(smoothness, detail < 0.0 ? 0.0 : 1.0, abs(detail) * mask);
	}
	
	return smoothness;
}
float GetSmoothnessB (InputConfig c) {
	float smoothness = INPUT_PROP(_SmoothnessB);
	smoothness *= GetMaskB(c).a;

	if (c.useDetail) {
		float detail = GetDetail(c).b * INPUT_PROP(_DetailSmoothness);
		float mask = GetMaskB(c).a;
		smoothness =
            lerp(smoothness, detail < 0.0 ? 0.0 : 1.0, abs(detail) * mask);
	}
	
	return smoothness;
}
float GetSmoothnessA (InputConfig c) {
	float smoothness = INPUT_PROP(_SmoothnessA);
	smoothness *= GetMaskA(c).a;

	if (c.useDetail) {
		float detail = GetDetail(c).b * INPUT_PROP(_DetailSmoothness);
		float mask = GetMaskA(c).a;
		smoothness =
            lerp(smoothness, detail < 0.0 ? 0.0 : 1.0, abs(detail) * mask);
	}
	
	return smoothness;
}

float GetFresnelR (InputConfig c) {
	return INPUT_PROP(_FresnelR);
}
float GetFresnelG (InputConfig c) {
	return INPUT_PROP(_FresnelG);
}
float GetFresnelB (InputConfig c) {
	return INPUT_PROP(_FresnelB);
}
float GetFresnelA (InputConfig c) {
	return INPUT_PROP(_FresnelA);
}

#endif