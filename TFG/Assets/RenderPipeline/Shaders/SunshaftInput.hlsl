﻿#ifndef SUNSHAFT_INPUT_INCLUDED
#define SUNSHAFT_INPUT_INCLUDED


UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseColor)
	UNITY_DEFINE_INSTANCED_PROP(float4, _ShaftColor)
	UNITY_DEFINE_INSTANCED_PROP(float4, _NoiseScaleAndOffset)
	UNITY_DEFINE_INSTANCED_PROP(float4, _NoiseSpeed)
	UNITY_DEFINE_INSTANCED_PROP(float, _Cutoff)
	UNITY_DEFINE_INSTANCED_PROP(float, _ZWrite)
	UNITY_DEFINE_INSTANCED_PROP(float, _RayStrength)
	UNITY_DEFINE_INSTANCED_PROP(float, _Fresnel)
UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

#define INPUT_PROP(name) UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, name)

struct InputConfig {
	Fragment fragment;
	float2 baseUV;
	float2 detailUV;
	bool useMask;
	bool useDetail;
};

InputConfig GetInputConfig (float4 positionSS, float2 baseUV, float2 detailUV = 0.0) {
	InputConfig c;
	c.fragment = GetFragment(positionSS);
	c.baseUV = baseUV;
	c.detailUV = detailUV;
	c.useMask = false;
	c.useDetail = false;
	return c;
}

float2 TransformBaseUV (float2 baseUV) {
	return baseUV;
}

float2 TransformDetailUV (float2 detailUV) {
	return detailUV;
}

float4 GetMask (InputConfig c) {
	return 1.0;
}

float4 GetDetail (InputConfig c) {
	return 0.0;
}

float4 GetBase (InputConfig c) {
	return float4(1,1,1,0);
}

float4 GetNoiseScaleAndOffset (InputConfig c) {
	return INPUT_PROP(_NoiseScaleAndOffset);
}

float GetFinalAlpha (float alpha) {
	return INPUT_PROP(_ZWrite) ? 1.0 : alpha;
}

float3 GetNormalTS (InputConfig c) {
	
	return 0;
}

float2 SeamlessUV(float2 uv)
{
	return float2(sin(uv.x* PI ),sin(uv.y* PI));
}
float3 GetEmission(InputConfig c)
{
	return 0;
}

float2 GetNoiseSpeed (InputConfig c) {
	return INPUT_PROP(_NoiseSpeed);
}

float GetCutoff (InputConfig c) {
	return INPUT_PROP(_Cutoff);
}

float GetMetallic (InputConfig c) {
	return 0;
}

float GetOcclusion (InputConfig c) {
	return 0;
}

float GetSmoothness (InputConfig c) {
	
	return 0;
}

float GetFresnel (InputConfig c) {
	return INPUT_PROP(_Fresnel);
}

float4 GetBaseColor(InputConfig c)
{
	return INPUT_PROP(_BaseColor);
}

float4 GetShaftColor(InputConfig c)
{
	return INPUT_PROP(_ShaftColor);
}


float3 GetEmission (InputConfig c, float3 normal, float3 viewDir, float3 positionOS) {
	float4 noiseScaleAndOffset = GetNoiseScaleAndOffset(c);
	float2 uv = SeamlessUV(c.baseUV);
	float4 color =GetShaftColor(c) * GradientNoise(uv * noiseScaleAndOffset.xy + noiseScaleAndOffset.zw + (GetNoiseSpeed(c) * _Time), 1);
	color += GetBaseColor(c);
	color *= abs(1 - FresnelEffect(normal, viewDir, GetFresnel(c)));
	float depth = saturate(c.fragment.bufferDepth.x - c.fragment.depth);
	float mask = saturate(pow(depth,2)) * abs( 1- FresnelEffect( normalize(-positionOS), viewDir, INPUT_PROP(_RayStrength)));
	return color * mask;
}

#endif