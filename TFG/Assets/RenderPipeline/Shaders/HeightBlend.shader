﻿Shader "Custom RP/HeightBlend" {
	
	Properties {
		_Factor12 ("Factor 12", Range(0, 1)) = 0
		_Factor23 ("Factor 23", Range(0, 1)) = 0
		_Factor34 ("Factor 34", Range(0, 1)) = 0
		_BaseMapR("TextureR", 2D) = "white" {}
		_BaseMapG("TextureG", 2D) = "white" {}
		_BaseMapB("TextureB", 2D) = "white" {}
		_BaseMapA("TextureA", 2D) = "white" {}
		_BaseColorR("ColorR", Color) = (0.5, 0.5, 0.5, 1.0)
		_BaseColorG("ColorG", Color) = (0.5, 0.5, 0.5, 1.0)
		_BaseColorB("ColorB", Color) = (0.5, 0.5, 0.5, 1.0)
		_BaseColorA("ColorA", Color) = (0.5, 0.5, 0.5, 1.0)
		[NoScaleOffset] _HeightMapR("Height Map R", 2D) = "white" {}
		[NoScaleOffset] _HeightMapG("Height Map G", 2D) = "white" {}
		[NoScaleOffset] _HeightMapB("Height Map B", 2D) = "white" {}
		[NoScaleOffset] _HeightMapA("Height Map A", 2D) = "white" {}
		_Cutoff ("Alpha Cutoff", Range(0.0, 1.0)) = 0.5
		[Toggle(_VERTEX_COLORS)] _VertexColors ("Vertex Colors", Float) = 0
		[Toggle(_CLIPPING)] _Clipping ("Alpha Clipping", Float) = 0
		[Toggle(_RECEIVE_SHADOWS)] _ReceiveShadows ("Receive Shadows", Float) = 1
		[KeywordEnum(On, Clip, Dither, Off)] _Shadows ("Shadows", Float) = 0

		[Toggle(_MASK_MAP)] _MaskMapToggle ("Mask Map", Float) = 0
		[NoScaleOffset] _MaskMapR("Mask (MODS)R", 2D) = "white" {}
		[NoScaleOffset] _MaskMapG("Mask (MODS)G", 2D) = "white" {}
		[NoScaleOffset] _MaskMapB("Mask (MODS)B", 2D) = "white" {}
		[NoScaleOffset] _MaskMapA("Mask (MODS)A", 2D) = "white" {}
		_MetallicR ("MetallicR", Range(0, 1)) = 0
		_MetallicG ("MetallicG", Range(0, 1)) = 0
		_MetallicB ("MetallicB", Range(0, 1)) = 0
		_MetallicA ("MetallicA", Range(0, 1)) = 0
		_OcclusionR ("OcclusionR", Range(0, 1)) = 1
		_OcclusionG ("OcclusionG", Range(0, 1)) = 1
		_OcclusionB ("OcclusionB", Range(0, 1)) = 1
		_OcclusionA ("OcclusionA", Range(0, 1)) = 1
		_SmoothnessR ("SmoothnessR", Range(0, 1)) = 0.5
		_SmoothnessG ("SmoothnessG", Range(0, 1)) = 0.5
		_SmoothnessB ("SmoothnessB", Range(0, 1)) = 0.5
		_SmoothnessA ("SmoothnessA", Range(0, 1)) = 0.5
		_FresnelR ("FresnelR", Range(0, 1)) = 1
		_FresnelG ("FresnelG", Range(0, 1)) = 1
		_FresnelB ("FresnelB", Range(0, 1)) = 1
		_FresnelA ("FresnelA", Range(0, 1)) = 1

		[Toggle(_NORMAL_MAP)] _NormalMapToggle ("Normal Map", Float) = 0
		[NoScaleOffset] _NormalMapR("NormalsR", 2D) = "bump" {}
		[NoScaleOffset] _NormalMapG("NormalsG", 2D) = "bump" {}
		[NoScaleOffset] _NormalMapB("NormalsB", 2D) = "bump" {}
		[NoScaleOffset] _NormalMapA("NormalsA", 2D) = "bump" {}
		_NormalScaleR("Normal ScaleR", Range(0, 1)) = 1
		_NormalScaleG("Normal ScaleG", Range(0, 1)) = 1
		_NormalScaleB("Normal ScaleB", Range(0, 1)) = 1
		_NormalScaleA("Normal ScaleA", Range(0, 1)) = 1

		[NoScaleOffset] _EmissionMapR("EmissionR", 2D) = "white" {}
		[NoScaleOffset] _EmissionMapG("EmissionG", 2D) = "white" {}
		[NoScaleOffset] _EmissionMapB("EmissionB", 2D) = "white" {}
		[NoScaleOffset] _EmissionMapA("EmissionA", 2D) = "white" {}
		[HDR] _EmissionColorR("EmissionR", Color) = (0.0, 0.0, 0.0, 0.0)
		[HDR] _EmissionColorG("EmissionG", Color) = (0.0, 0.0, 0.0, 0.0)
		[HDR] _EmissionColorB("EmissionB", Color) = (0.0, 0.0, 0.0, 0.0)
		[HDR] _EmissionColorA("EmissionA", Color) = (0.0, 0.0, 0.0, 0.0)

		_DetailMapToggle ("Detail Maps", Float) = 0
		_DetailMap("Details", 2D) = "linearGrey" {}
		[NoScaleOffset] _DetailNormalMap("Detail Normals", 2D) = "bump" {}
		_DetailAlbedo("Detail Albedo", Range(0, 1)) = 1
		_DetailSmoothness("Detail Smoothness", Range(0, 1)) = 1
		_DetailNormalScale("Detail Normal Scale", Range(0, 1)) = 1
		
		[Toggle(_PREMULTIPLY_ALPHA)] _PremulAlpha ("Premultiply Alpha", Float) = 0

		[Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend ("Src Blend", Float) = 1
		[Enum(UnityEngine.Rendering.BlendMode)] _DstBlend ("Dst Blend", Float) = 0
		[Enum(Off, 0, On, 1)] _ZWrite ("Z Write", Float) = 1

		[HideInInspector] _MainTex("Texture for Lightmap", 2D) = "white" {}
		[HideInInspector] _Color("Color for Lightmap", Color) = (0.5, 0.5, 0.5, 1.0)
	}
	
	SubShader {
		HLSLINCLUDE
		#include "../ShaderLibrary/Common.hlsl"
		#include "HeightBlendInput.hlsl"
		ENDHLSL

		Pass {
			
			Tags {
				"LightMode" = "CustomLit"
			}

			Blend [_SrcBlend] [_DstBlend], One OneMinusSrcAlpha
			ZWrite [_ZWrite]

			HLSLPROGRAM
			#pragma target 3.5
			#pragma shader_feature _VERTEX_COLORS
			#pragma shader_feature _CLIPPING
			#pragma shader_feature _RECEIVE_SHADOWS
			#pragma shader_feature _PREMULTIPLY_ALPHA
			#pragma shader_feature _MASK_MAP
			#pragma shader_feature _NORMAL_MAP
			#pragma shader_feature _DETAIL_MAP
			#pragma multi_compile _ _DIRECTIONAL_PCF3 _DIRECTIONAL_PCF5 _DIRECTIONAL_PCF7
			#pragma multi_compile _ _OTHER_PCF3 _OTHER_PCF5 _OTHER_PCF7
			#pragma multi_compile _ _CASCADE_BLEND_SOFT _CASCADE_BLEND_DITHER
			#pragma multi_compile _ _SHADOW_MASK_ALWAYS _SHADOW_MASK_DISTANCE
			#pragma multi_compile _ _LIGHTS_PER_OBJECT
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_instancing
			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment
			
			#include "HeightBlendPass.hlsl"
			ENDHLSL
		}

		Pass {
			Tags {
				"LightMode" = "ShadowCaster"
			}

			ColorMask 0
			
			HLSLPROGRAM
			#pragma target 3.5
			#pragma shader_feature _ _SHADOWS_CLIP _SHADOWS_DITHER
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_instancing
			#pragma vertex ShadowCasterPassVertex
			#pragma fragment ShadowCasterPassFragment
			#include "HeightBlendShadowCasterPass.hlsl"
			ENDHLSL
		}

		Pass {
			Tags {
				"LightMode" = "Meta"
			}

			Cull Off

			HLSLPROGRAM
			#pragma target 3.5
			#pragma vertex MetaPassVertex
			#pragma fragment MetaPassFragment
			#include "HeightBlendMetaPass.hlsl"
			ENDHLSL
		}
	}

	CustomEditor "CustomShaderGUI"
}