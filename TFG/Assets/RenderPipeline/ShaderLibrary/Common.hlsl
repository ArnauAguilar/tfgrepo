﻿#ifndef CUSTOM_COMMON_INCLUDED
#define CUSTOM_COMMON_INCLUDED

#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/CommonMaterial.hlsl"
#include "UnityInput.hlsl"

#define UNITY_MATRIX_M unity_ObjectToWorld
#define UNITY_MATRIX_I_M unity_WorldToObject
#define UNITY_MATRIX_V unity_MatrixV
#define UNITY_MATRIX_VP unity_MatrixVP
#define UNITY_MATRIX_P glstate_matrix_projection

#if defined(_SHADOW_MASK_ALWAYS) || defined(_SHADOW_MASK_DISTANCE)
    #define SHADOWS_SHADOWMASK
#endif

#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/SpaceTransforms.hlsl"
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Packing.hlsl"

SAMPLER(sampler_linear_clamp);
SAMPLER(sampler_point_clamp);



float4 Heightblend(float4 input1, float height1, float4 input2, float height2, float factor)
{

	float height_start = max(height1, height2) - factor;
	float level1 = max(height1 - height_start, 0);
	float level2 = max(height2 - height_start, 0);
	return((input1 * level1) + (input2 * level2)) / (level1 + level2);
}

float4 Heightlerp(float4 input1, float height1, float4 input2, float height2, float factor, float t)
{
	t = clamp(t, 0, 1);
	return Heightblend(input1, height1 * (1 - t), input2, height2 * t, factor);
}

float4 HeightBlend4(float4 input1, float height1, float4 input2, float height2, float4 input3, float height3,float4 input4, float height4, float factor12, float factor23,float factor34)
{
	float4 color = Heightblend(input1, height1, input2, height2, factor12);
	float maxHeight = max(height1, height2);
	color = Heightblend(color, maxHeight, input3, height3, factor23);
	maxHeight = max(maxHeight, height3);
	color = Heightblend(color, maxHeight, input4, height4, factor34);
	return color;
}

inline float3 ObjSpaceViewDir (float3 v) {
	float3 objSpaceCameraPos =
        mul(unity_WorldToObject, float4(_WorldSpaceCameraPos.xyz, 1)).xyz;
	return objSpaceCameraPos - v.xyz;
}

inline float4 ComputeScreenPos (float4 pos) {
	float4 o = pos * 0.5f;
	#if defined(UNITY_HALF_TEXEL_OFFSET)
	o.xy = float2(o.x, o.y*_ProjectionParams.x) + o.w * _ScreenParams.zw;
	#else
	o.xy = float2(o.x, o.y*_ProjectionParams.x) + o.w;
	#endif
 
	o.zw = pos.zw;
	return o;
}

float FresnelEffect(float3 Normal, float3 ViewDir, float Power)
{
	return pow((1.0 - saturate(dot(normalize(Normal), normalize(ViewDir)))), Power);
}

float2 GradientNoise_dir(float2 p)
{
	p = p % 289;
	float x = (34 * p.x + 1) * p.x % 289 + p.y;
	x = (34 * x + 1) * x % 289;
	x = frac(x / 41) * 2 - 1;
	return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
}

float GradientNoise(float2 p)
{
	float2 ip = floor(p);
	float2 fp = frac(p);
	float d00 = dot(GradientNoise_dir(ip), fp);
	float d01 = dot(GradientNoise_dir(ip + float2(0, 1)), fp - float2(0, 1));
	float d10 = dot(GradientNoise_dir(ip + float2(1, 0)), fp - float2(1, 0));
	float d11 = dot(GradientNoise_dir(ip + float2(1, 1)), fp - float2(1, 1));
	fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
	return lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x);
}

float GradientNoise(float2 UV, float Scale)
{
	return GradientNoise(UV * Scale) + 0.5;
}

float2 RotateUV(float2 UV, float2 Center, float Rotation)
{
	Rotation = Rotation * (3.1415926f/180.0f);
	UV -= Center;
	float s = sin(Rotation);
	float c = cos(Rotation);
	float2x2 rMatrix = float2x2(c, -s, s, c);
	rMatrix *= 0.5;
	rMatrix += 0.5;
	rMatrix = rMatrix * 2 - 1;
	UV.xy = mul(UV.xy, rMatrix);
	UV += Center;
	return UV;
}


bool IsOrthographicCamera () {
	return unity_OrthoParams.w;
}

float OrthographicDepthBufferToLinear (float rawDepth) {
	#if UNITY_REVERSED_Z
	rawDepth = 1.0 - rawDepth;
	#endif
	return (_ProjectionParams.z - _ProjectionParams.y) * rawDepth + _ProjectionParams.y;
}

#include "Fragment.hlsl"

float remap(float value, float low1, float low2, float high1, float high2)
{
	return low2 + (value - low1) *  (high2 - low2) / (high1 - low1);

}

float Square (float x) {
	return x * x;
}

float DistanceSquared(float3 pA, float3 pB) {
	return dot(pA - pB, pA - pB);
}

void ClipLOD (Fragment fragment, float fade) {
	#if defined(LOD_FADE_CROSSFADE)
	float dither = InterleavedGradientNoise(fragment.positionSS, 0);
	clip(fade + (fade < 0.0 ? dither : -dither));
	#endif
}

float3 DecodeNormal (float4 sample, float scale) {
	#if defined(UNITY_NO_DXT5nm)
	return UnpackNormalRGB(sample, scale);
	#else
	return UnpackNormalmapRGorAG(sample, scale);
	#endif
}

float3 NormalTangentToWorld (float3 normalTS, float3 normalWS, float4 tangentWS) {
	float3x3 tangentToWorld =
        CreateTangentToWorld(normalWS, tangentWS.xyz, tangentWS.w);
	return TransformTangentToWorld(normalTS, tangentToWorld);
}

#endif